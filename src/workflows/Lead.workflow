<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CR_CP_Google_Apps_Welcome_Mail</fullName>
        <description>Control Panel for Google Apps: Welcome Mail</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>controlpanel.team@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_CP_Apps_Initial_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>Control_Panel_for_EC2_Welcome_Email</fullName>
        <description>Control Panel for EC2: Welcome Email</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>controlpanel.team@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Control_Panel_for_EC2_Initial_Welcome_Mail</template>
    </alerts>
    <alerts>
        <fullName>Control_Panel_for_Google_Apps_Mid_Trial_Feedback</fullName>
        <description>Control Panel for Google Apps: Mid-Trial Feedback</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>james.monico@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Control_Panel_Mid_trial_Feedback</template>
    </alerts>
    <alerts>
        <fullName>Notify_NL_users</fullName>
        <ccEmails>elke.nijhoff@cloudreach.co.uk</ccEmails>
        <description>Notify NL users</description>
        <protected>false</protected>
        <recipients>
            <recipient>NL_Event_Notification</recipient>
            <type>group</type>
        </recipients>
        <senderAddress>controlpanel.team@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/New_Lead_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lead_Update_Domain</fullName>
        <description>This field update updates the domain field if it is blank, using (in order of priority) 1) Domain from email address 2) Domain from website address</description>
        <field>Domain__c</field>
        <formula>IF ( Email =&quot;&quot; , 

IF(
FIND(&quot;www.&quot;, Website)=0,
IF ( FIND(&quot;http://&quot;, Website)=0,Website, RIGHT(Website, LEN(Website) - (FIND(&quot;http://&quot;, Website) +6))  ) ,
RIGHT(Website, LEN(Website) - (FIND(&quot;www.&quot;, Website) +3))
) ,

SUBSTITUTE(Email, LEFT(Email, FIND(&quot;@&quot;, Email)), NULL)     )</formula>
        <name>Lead: Update Domain</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Control Panel for EC2%3A Send welcome mail</fullName>
        <actions>
            <name>Control_Panel_for_EC2_Welcome_Email</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>GMP - GApp EC2CP</value>
        </criteriaItems>
        <description>Send a welcome mail to the user when they sign up for CP for EC2</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Create Followup Task</fullName>
        <actions>
            <name>Follow_up_for_this_lead</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Create_Task__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Notify Elke%21</fullName>
        <actions>
            <name>Notify_NL_users</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Event_name__c</field>
            <operation>equals</operation>
            <value>Amazon Web Services for the Media &amp; Publishing Industry (NL)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Event</value>
        </criteriaItems>
        <description>notify Elke that a new person registered to the &apos;Amazon Web Services for the Media &amp; Publishing Industry (NL)&apos; event</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead%3A Update Domain</fullName>
        <actions>
            <name>Lead_Update_Domain</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Domain__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Follow_up_for_this_lead</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Follow &apos;up for this lead</subject>
    </tasks>
</Workflow>
