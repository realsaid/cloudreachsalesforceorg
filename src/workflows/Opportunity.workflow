<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>CE_or_CP_Order_form_approved</fullName>
        <description>CE or CP Order form approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>contracts@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_Legal_and_CB</template>
    </alerts>
    <alerts>
        <fullName>CE_or_CP_Order_form_approved_by_Legal_Team</fullName>
        <description>CE or CP Order form approved by Legal Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>chris.bunch@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>morag.dowie@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_by_Legal</template>
    </alerts>
    <alerts>
        <fullName>CO_Order_form_approved</fullName>
        <description>CO Order form approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>contracts@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_Legal_and_JG</template>
    </alerts>
    <alerts>
        <fullName>CO_Order_form_approved_by_Legal_Team</fullName>
        <description>CO Order form approved by Legal Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.monico@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_by_Legal</template>
    </alerts>
    <alerts>
        <fullName>Echosign_Request_SLA_Breach</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <ccEmails>donal.fullerton@cloudreach.com</ccEmails>
        <description>Echosign Request - SLA Breach</description>
        <protected>false</protected>
        <recipients>
            <recipient>andre.azevedo@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SLA_Breach_send_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>Email_for_EchoSign_request</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <description>Email for EchoSign request</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Request_to_send_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>HTC_Email</fullName>
        <description>HTC Email</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FA_HTCs_VF</template>
    </alerts>
    <alerts>
        <fullName>Notify_opp_owners_to_enter_PO_number_after_7_days</fullName>
        <description>Notify opp owners to enter PO number after 7 days</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_PO_number_alert_email_to_opportunity_owner</template>
    </alerts>
    <alerts>
        <fullName>Opp_require_reapproval</fullName>
        <description>Opp: require reapproval</description>
        <protected>false</protected>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_require_reapproval</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Send_Email_to_Renewal_Owner</fullName>
        <description>Opportunity: Send Email to Renewal Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opportunity_Renewal_Created</template>
    </alerts>
    <alerts>
        <fullName>Oppty_1_hr_SLA_Breach_Urgent_Request_for_OF_to_be_EchoSigned</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <ccEmails>donal.fullerton@cloudreach.com</ccEmails>
        <description>Oppty: 1 hr SLA Breach Urgent Request for OF to be EchoSigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>andre.azevedo@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SLA_Breach_Urgent_request_to_send_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>Order_form_rejected</fullName>
        <description>Order form rejected</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_rejected</template>
    </alerts>
    <alerts>
        <fullName>Renewal_order_form_approved</fullName>
        <description>Renewal order form approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Renewal_Order_form_approved</template>
    </alerts>
    <alerts>
        <fullName>SFDC_Order_form_approved</fullName>
        <description>SFDC Order form approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>contracts@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_Legal_and_KC</template>
    </alerts>
    <alerts>
        <fullName>SFDC_Order_form_approved_by_Legal_Team</fullName>
        <description>SFDC Order form approved by Legal Team</description>
        <protected>false</protected>
        <recipients>
            <recipient>contracts@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>kevin.campbell@cloudreach.com.prod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_by_Legal</template>
    </alerts>
    <alerts>
        <fullName>US_Order_form_approved</fullName>
        <description>US Order form approved</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>contracts@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_Legal_and_TR</template>
    </alerts>
    <alerts>
        <fullName>US_Order_form_approved_by_Legal_Team</fullName>
        <description>US Order form approved by Legal Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>contracts@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tom.ray@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Opp_Order_form_approved_by_Legal</template>
    </alerts>
    <alerts>
        <fullName>Urgent_request_for_EchoSign</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <description>Urgent request for EchoSign</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Urgent_request_to_send_EchoSign</template>
    </alerts>
    <fieldUpdates>
        <fullName>Awaiting_BU_Lead_approval</fullName>
        <description>Awaiting BU Lead approval by Chris Bunch</description>
        <field>Order_Form_Approval_Status__c</field>
        <literalValue>Submitted for BU Lead approval</literalValue>
        <name>Awaiting BU Lead approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_approve_flag</fullName>
        <field>Apporved__c</field>
        <literalValue>0</literalValue>
        <name>Clear approve flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>FF_Due_Date_Update</fullName>
        <field>Due_Date__c</field>
        <formula>Invoice_Date__c  +  Payment_Terms__c</formula>
        <name>FF Due Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Clear_Credit_Note</fullName>
        <description>Clear credit note when an opportunity is first created.</description>
        <field>Credit_Notes__c</field>
        <name>Opportunity: Clear Credit Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Clear_Order_Form_link</fullName>
        <description>This clears the order form link for newly created opportunities. There is a chance that this deletes it from a new opportunity, but leaving the old order form on is very bad.</description>
        <field>Order_Form__c</field>
        <name>Opportunity: Clear Order Form link</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Name_Renewal_Update</fullName>
        <description>This update adds the renewal date and month to the opportunity name.</description>
        <field>Name</field>
        <formula>If (FIND(&quot;(Renewal&quot;, Name)=0, 

Name &amp; &quot; (Renewal &quot; &amp; 
IF (MONTH(CloseDate)&lt;10, &quot;0&quot; &amp; Text(Month(CloseDate)), Text(Month(CloseDate)))
&amp; &quot;/&quot; &amp; Text(Year(CloseDate)) &amp;&quot;)&quot;,

LEFT(Name, FIND(&quot;(Renewal&quot;, Name)-1) &amp; &quot;(Renewal &quot; &amp; 
IF (MONTH(CloseDate)&lt;10, &quot;0&quot; &amp; Text(Month(CloseDate)), Text(Month(CloseDate)))
&amp; &quot;/&quot; &amp; Text(Year(CloseDate)) &amp;&quot;)&quot;
)</formula>
        <name>Opportunity Name Renewal Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Form_Approved</fullName>
        <field>Order_Form_Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Order Form Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Form_Recalled</fullName>
        <field>Order_Form_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Order Form Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Order_Form_Rejected</fullName>
        <field>Order_Form_Approval_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Order Form Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Legal_approval</fullName>
        <description>Order form has been submitted for Legal Team approval</description>
        <field>Order_Form_Approval_Status__c</field>
        <literalValue>Submitted for Legal Approval</literalValue>
        <name>Submitted for Legal approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approve_field</fullName>
        <field>Apporved__c</field>
        <literalValue>1</literalValue>
        <name>Update Approve field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Start_Date</fullName>
        <description>Update service start date with the same date as the close date.</description>
        <field>Contract_Start_Month__c</field>
        <formula>CloseDate</formula>
        <name>Update Service Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Closed_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update Stage to Closed Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update Stage to Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Legal_Procurement</fullName>
        <field>StageName</field>
        <literalValue>Legal &amp; Procurement</literalValue>
        <name>Update Stage to Legal &amp; Procurement</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Project_Defined</fullName>
        <field>StageName</field>
        <literalValue>Project Defined</literalValue>
        <name>Update Stage to Project Defined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Proposal_Delivered</fullName>
        <field>StageName</field>
        <literalValue>Proposal Delivered</literalValue>
        <name>Update Stage to Proposal Delivered</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Prospect</fullName>
        <field>StageName</field>
        <literalValue>Prospect</literalValue>
        <name>Update Stage to Prospect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_to_Suspect</fullName>
        <field>StageName</field>
        <literalValue>Suspect</literalValue>
        <name>Update Stage to Suspect</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_stage_to_Legal</fullName>
        <field>StageName</field>
        <literalValue>Legal &amp; Procurement</literalValue>
        <name>Update stage to &apos;Legal&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Ts_Cs</fullName>
        <field>Contract_Type__c</field>
        <literalValue>Online Ts &amp; Cs</literalValue>
        <name>update Ts &amp; Cs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Opp%3A AM should create an FA</fullName>
        <actions>
            <name>NO_FA_for_this_account</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>IF opp probability is over 20% AND the account does NOT has an FA (contract lookup populated), the AM should create one ASAP</description>
        <formula>AND ( ISBLANK ( FA_Contract__c ) , Probability &gt; 0.2, ISPICKVAL( Contract_Type__c , &apos;FA&apos;)  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A Send HTC email</fullName>
        <actions>
            <name>HTC_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>IF opp probability is over 20% AND the account has an FA (contract lookup populated)  - email the user to warn him about FA HTCs</description>
        <formula>AND ( NOT ( ISBLANK ( FA_Contract__c ) ) , ISCHANGED( FA_Contract__c ) , Probability &gt; 0.2 )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opp%3A require reapproval</fullName>
        <actions>
            <name>Opp_require_reapproval</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Clear_approve_flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If Deal value changed (mre then 20% up or down), change either to or from a Multi-Year deal or Any territory change, switch off approval flag</description>
        <formula>AND(Apporved__c , OR (Amount &gt; 1.2* PRIORVALUE(Amount) , Amount &lt; 0.8* PRIORVALUE(Amount),    ISCHANGED( Shadow_territory_id__c ), ISCHANGED(Multi_year_deal__c)  ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity FF Rule</fullName>
        <actions>
            <name>FF_Due_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Invoice_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Payment_Terms__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Due Date based on Payment Terms</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Add renewal Month%2FYear to opportunity name</fullName>
        <actions>
            <name>Opportunity_Name_Renewal_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <description>This workflow adds the month and year of the renewal date to the opportunity name automatically</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Send Opportunity Owner notification of renewal</fullName>
        <actions>
            <name>Opportunity_Send_Email_to_Renewal_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Type</field>
            <operation>equals</operation>
            <value>Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.ForecastCategoryName</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Because of the way we implemented the renewal opportunity (using cloning) we need to ask the sales rep to update the forecast category.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PO number should not be blank</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Purchase_Order_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>If an opp is closed won, we need to enter PO number</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Notify_opp_owners_to_enter_PO_number_after_7_days</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Please_chase_the_opp_owner_to_enter_a_PO_number</name>
                <type>Task</type>
            </actions>
            <timeLength>11</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Please_get_the_PO_number_for_the_record_you_have_closed_won</name>
                <type>Task</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send request for EchoSign</fullName>
        <actions>
            <name>Email_for_EchoSign_request</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Request_to_send_for_EchoSign__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Urgent__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Order_Form_Echosigned__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Legal &amp; Procurement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Apporved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EchoSign_Cancelled_Declined__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EchoSign_out_for_signature__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Echosign_Request_SLA_Breach</name>
                <type>Alert</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Send urgent request for EchoSign</fullName>
        <actions>
            <name>Urgent_request_for_EchoSign</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Request_to_send_for_EchoSign__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Urgent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Order_Form_Echosigned__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Legal &amp; Procurement</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Apporved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EchoSign_Cancelled_Declined__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.EchoSign_out_for_signature__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow where the request is urgent</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Oppty_1_hr_SLA_Breach_Urgent_Request_for_OF_to_be_EchoSigned</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Service start date update</fullName>
        <actions>
            <name>Update_Service_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>Workflow to update this at &apos;open&apos; sales stages to match the close date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Contract Type</fullName>
        <actions>
            <name>update_Ts_Cs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Online_Terms__c</field>
            <operation>equals</operation>
            <value>http://www.cloudreach.com/gb-en/cloudreach-supply-of-service/</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Closed Lost</fullName>
        <actions>
            <name>Update_Stage_to_Closed_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Closed Lost&quot; when Reasons for Losing and Why We Have Lost are populated.</description>
        <formula>AND(  NOT(ISBLANK(TEXT(Reasons_for_loosing__c ))),  NOT(ISBLANK(TEXT(Why_have_we_lost__c))) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Closed Won</fullName>
        <actions>
            <name>Update_Stage_to_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Closed Won&quot; when relevant checkboxes criteria are all ticked.</description>
        <formula>AND(  Need_Problem_Pain__c = TRUE,  confirmed_it_s_OK_to_continue__c = TRUE,   Customer_looking_at_other_platforms__c = TRUE,   Call_Taken_Place__c  = TRUE,  Customer_understands_our_value_prop__c  = TRUE,  Customer_talking_to_other_integrators__c  = TRUE,     Timescale__c = TRUE,  Project_contacts_identified__c = TRUE,   Shared_non_priced_prospoal__c = TRUE,   Customer_s_Budget__c = TRUE,   Solution_fit_for_purpose__c = TRUE,   Customer_selection_criteria_known__c = TRUE,   Proposal_Sent__c = TRUE,   Order_Form_sent_to_customer__c = TRUE,  Proposal_discussed_with_customer__c = TRUE,   Competitor_review_completed__c = TRUE,    Cloudreach_is_the_selected_supplier__c = TRUE,   Timeline_for_signature_agreed__c = TRUE,   Procurement_process_understood_by_AM__c = TRUE,    Apporved__c  = TRUE,  Order_Form_Echosigned__c  = TRUE,  PO_number_received__c  = TRUE,   ISBLANK(TEXT(Reasons_for_loosing__c )),  ISBLANK(TEXT(Why_have_we_lost__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Legal %26 Procurement</fullName>
        <actions>
            <name>Update_Stage_to_Legal_Procurement</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Legal &amp; Procurement&quot; when relevant checkboxes criteria are all ticked.</description>
        <formula>AND(  Need_Problem_Pain__c = TRUE,  confirmed_it_s_OK_to_continue__c = TRUE,   Customer_looking_at_other_platforms__c = TRUE,   Call_Taken_Place__c  = TRUE,  Customer_understands_our_value_prop__c  = TRUE,  Customer_talking_to_other_integrators__c  = TRUE,     Timescale__c = TRUE,  Project_contacts_identified__c = TRUE,   Shared_non_priced_prospoal__c = TRUE,   Customer_s_Budget__c = TRUE,   Solution_fit_for_purpose__c = TRUE,   Customer_selection_criteria_known__c = TRUE,   Proposal_Sent__c = TRUE,   Apporved__c = TRUE,  Order_Form_sent_to_customer__c = TRUE,  Proposal_discussed_with_customer__c = TRUE,   Competitor_review_completed__c = TRUE,    Cloudreach_is_the_selected_supplier__c = TRUE,    Timeline_for_signature_agreed__c = TRUE,   Procurement_process_understood_by_AM__c = TRUE,    OR(Apporved__c  = FALSE,  Order_Form_Echosigned__c  = FALSE,  PO_number_received__c  = FALSE),   ISBLANK(TEXT(Reasons_for_loosing__c )),  ISBLANK(TEXT(Why_have_we_lost__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Project Defined</fullName>
        <actions>
            <name>Update_Stage_to_Project_Defined</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Project Defined&quot; when relevant checkboxes criteria are all ticked.</description>
        <formula>AND(  Need_Problem_Pain__c = TRUE,  confirmed_it_s_OK_to_continue__c = TRUE,   Customer_looking_at_other_platforms__c = TRUE,   Call_Taken_Place__c  = TRUE,  Customer_understands_our_value_prop__c  = TRUE,  Customer_talking_to_other_integrators__c  = TRUE,     Timescale__c = TRUE,  Project_contacts_identified__c = TRUE,   Shared_non_priced_prospoal__c = TRUE,   Customer_s_Budget__c = TRUE,   Solution_fit_for_purpose__c = TRUE,   OR(Customer_selection_criteria_known__c = FALSE,   Proposal_Sent__c = FALSE,   Proposal_discussed_with_customer__c = FALSE,   Competitor_review_completed__c = FALSE),    ISBLANK(TEXT(Reasons_for_loosing__c )),  ISBLANK(TEXT(Why_have_we_lost__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Proposal Delivered</fullName>
        <actions>
            <name>Update_Stage_to_Proposal_Delivered</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Proposal Delivered&quot; when relevant checkboxes criteria are all ticked.</description>
        <formula>AND(  Need_Problem_Pain__c = TRUE,  confirmed_it_s_OK_to_continue__c = TRUE,   Customer_looking_at_other_platforms__c = TRUE,   Call_Taken_Place__c  = TRUE,  Customer_understands_our_value_prop__c  = TRUE,  Customer_talking_to_other_integrators__c  = TRUE,     Timescale__c = TRUE,  Project_contacts_identified__c = TRUE,   Shared_non_priced_prospoal__c = TRUE,   Customer_s_Budget__c = TRUE,   Solution_fit_for_purpose__c = TRUE,   Customer_selection_criteria_known__c = TRUE,   Proposal_Sent__c = TRUE,  Order_Form_sent_to_customer__c = TRUE,   Proposal_discussed_with_customer__c = TRUE,   Competitor_review_completed__c = TRUE,    OR(Cloudreach_is_the_selected_supplier__c = FALSE,   Order_Form_sent_to_customer__c = FALSE,   Timeline_for_signature_agreed__c = FALSE,   Procurement_process_understood_by_AM__c = FALSE),    ISBLANK(TEXT(Reasons_for_loosing__c )),  ISBLANK(TEXT(Why_have_we_lost__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Prospect</fullName>
        <actions>
            <name>Update_Stage_to_Prospect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Prospect&quot; when checkboxes criteria are all ticked.</description>
        <formula>AND(  Need_Problem_Pain__c = TRUE,  confirmed_it_s_OK_to_continue__c = TRUE,   Customer_looking_at_other_platforms__c = TRUE,   Call_Taken_Place__c  = TRUE,  Customer_understands_our_value_prop__c  = TRUE,  Customer_talking_to_other_integrators__c  = TRUE,     OR(Timescale__c = FALSE,  Project_contacts_identified__c = FALSE,   Shared_non_priced_prospoal__c = FALSE,   Customer_s_Budget__c = FALSE,   Solution_fit_for_purpose__c = FALSE   ),    ISBLANK(TEXT(Reasons_for_loosing__c )),  ISBLANK(TEXT(Why_have_we_lost__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Stage to Suspect</fullName>
        <actions>
            <name>Update_Stage_to_Suspect</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Opportunity Stage to &quot;Suspect&quot; when no checkboxes criteria are ticked.</description>
        <formula>AND(  OR(Need_Problem_Pain__c = FALSE,  confirmed_it_s_OK_to_continue__c = FALSE,   Customer_looking_at_other_platforms__c = FALSE,   Call_Taken_Place__c  = FALSE,  Customer_understands_our_value_prop__c  = FALSE,  Customer_talking_to_other_integrators__c  = FALSE),   ISBLANK(TEXT(Reasons_for_loosing__c )),  ISBLANK(TEXT(Why_have_we_lost__c)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>NO_FA_for_this_account</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>A new opportunity has been created on your account, which doesn&apos;t have a Framework agreement contract. 

Please negotiate one with your customer, 
create/activate a contract on the account ASAP and get it reviewed ASAP to prevent delays or errors</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>NO FA for this account!</subject>
    </tasks>
    <tasks>
        <fullName>Please_chase_the_opp_owner_to_enter_a_PO_number</fullName>
        <assignedToType>role</assignedToType>
        <description>The opp owner has not entered a PO number for the closed won record and it has been more than 11 days</description>
        <dueDateOffset>11</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Please chase the opp owner to enter a PO number</subject>
    </tasks>
    <tasks>
        <fullName>Please_get_the_PO_number_for_the_record_you_have_closed_won</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>In Progress</status>
        <subject>Please get the PO number for the record you have closed won</subject>
    </tasks>
</Workflow>
