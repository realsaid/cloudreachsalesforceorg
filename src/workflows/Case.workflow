<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Closed_Due_to_Lack_of_Activity</fullName>
        <description>Cases: Case Closed Due to Lack of Activity</description>
        <protected>false</protected>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Closed_Solution_offered</template>
    </alerts>
    <alerts>
        <fullName>Cases_Alert_Evrard</fullName>
        <ccEmails>evrard.bibila@cloudreach.com</ccEmails>
        <description>Cases: Alert Evrard</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_New_Case_Alert_Evrard</template>
    </alerts>
    <alerts>
        <fullName>Cases_Email_Assigned_Engineer_Day_1</fullName>
        <description>Cases: Email Assigned Engineer (Day 1)</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Engineer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Alert_Peding_Cloudreach_Response</template>
    </alerts>
    <alerts>
        <fullName>Cases_Email_Assigned_Engineer_Day_2</fullName>
        <description>Cases: Email Assigned Engineer (Day 2)</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Engineer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Alert_Peding_Cloudreach_Response</template>
    </alerts>
    <alerts>
        <fullName>Cases_Email_Assigned_Engineer_Day_3</fullName>
        <description>Cases: Email Assigned Engineer (Day 3)</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Engineer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Alert_Peding_Cloudreach_Response</template>
    </alerts>
    <alerts>
        <fullName>Cases_Email_Assigned_Engineer_Day_4</fullName>
        <description>Cases: Email Assigned Engineer (Day 4)</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Engineer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Alert_Peding_Cloudreach_Response</template>
    </alerts>
    <alerts>
        <fullName>Cases_P1_Case_Alert_Sales</fullName>
        <description>Cases: P1 Case Alert - Sales</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <recipient>harald.viehweger@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tom.ray@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CR_P1_Case_Alert_Sales</template>
    </alerts>
    <alerts>
        <fullName>Cases_P1_P2_Alert_Tony</fullName>
        <ccEmails>tony.nghiem@cloudreach.co.uk</ccEmails>
        <description>Cases:P1/P2 Alert - Tony</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CR_P1_Case_Alert_Tony</template>
    </alerts>
    <alerts>
        <fullName>Cases_P1_P4</fullName>
        <ccEmails>christine.lutz@cloudreach.co.uk</ccEmails>
        <description>Cases: P1-P4 Alert - Christine</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_New_P1_to_P4_Case_Alert_Christine</template>
    </alerts>
    <alerts>
        <fullName>Cases_Send_email_to_Assigned_Engineer_on_case_assignment_or_change</fullName>
        <description>Cases: Send email to Assigned Engineer on case assignment or change</description>
        <protected>false</protected>
        <recipients>
            <field>Assigned_Engineer_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Case_Updated_Email_Assigned_Engineer</template>
    </alerts>
    <alerts>
        <fullName>Cases_Send_email_to_Escalation_Manager</fullName>
        <ccEmails>escalation.manager@cloudreach.co.uk</ccEmails>
        <description>Cases: Send email to Escalation Manager</description>
        <protected>false</protected>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Cases_Escalation_Mail</template>
    </alerts>
    <alerts>
        <fullName>Cases_Send_email_to_Support_Team_when_new_case_has_been_assigned</fullName>
        <ccEmails>support.team@cloudreach.co.uk</ccEmails>
        <description>Cases: Send email to Support Team when new case has been assigned</description>
        <protected>false</protected>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Cases_Send_email_to_Support_Team_when_new_case_received</fullName>
        <ccEmails>support.team@cloudreach.co.uk</ccEmails>
        <description>Cases: Send email to Support Team when new case received</description>
        <protected>false</protected>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_New_Case_Received</template>
    </alerts>
    <alerts>
        <fullName>Cases_Support_Team_Escalation</fullName>
        <ccEmails>support.team@cloudreach.co.uk</ccEmails>
        <description>Cases: Support Team Escalation</description>
        <protected>false</protected>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Cases_Support_Team_Escalation_Mail</template>
    </alerts>
    <alerts>
        <fullName>Email_alert_for_Aui_P1_P2</fullName>
        <ccEmails>christine.lutz@cloudreach.co.uk</ccEmails>
        <description>Email alert for Audi P1/P2</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CR_P1_Case_Alert_Christine</template>
    </alerts>
    <alerts>
        <fullName>Email_customer_if_status_is_pending</fullName>
        <description>Cases: Email customer if status is pending</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Pending_Reply_Notification</template>
    </alerts>
    <alerts>
        <fullName>Escalate_case_not_assigned</fullName>
        <ccEmails>escalation.manager@cloudreach.co.uk</ccEmails>
        <description>Escalate - case not assigned</description>
        <protected>false</protected>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/Cases_Escalate_if_unassigned_greater_than_2_hours</template>
    </alerts>
    <alerts>
        <fullName>Hearst_P1_alerts_Robert</fullName>
        <description>Hearst P1 alerts - Robert</description>
        <protected>false</protected>
        <recipients>
            <recipient>robert.wong@cloudreach.com.cr</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_New_P1_Case_Alert_Robert</template>
    </alerts>
    <alerts>
        <fullName>Send_email_survey_to_case_creator</fullName>
        <description>Cases: Send email survey to case creator</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SuppliedEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Send_email_survey_to_case_creator_second</fullName>
        <description>Cases: Send email survey to case creator</description>
        <protected>false</protected>
        <recipients>
            <field>ContactEmail</field>
            <type>email</type>
        </recipients>
        <senderAddress>servicedesk@cloudreach.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/CR_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>amber p1 - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>amber p1 - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>amber p1 - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>amber p1 - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>amber p1 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_resolution_warning_email_751</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>amber p1 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>amber p1 - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>amber_p1_response_warning_email_0</fullName>
        <ccEmails>priority@cloudreach.co.uk</ccEmails>
        <description>amber p1 - response warning email - 0%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p2 - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p2 - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p2 - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p2 - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p2 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p2 - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p2_response_warning_email_50</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p2 - response warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p3 - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p3 - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p3 - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p3 - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p3 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p3 - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_response_warning_email_75</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p3 - response warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p3_svc_req_response_warning_email_75</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p3 - svc req - response warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p4 - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p4 - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p4 - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p4 - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p4 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p4 - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_response_warning_email_75</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p4 - response warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p4_svc_req_response_warning_email_75</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p4 - svc req - response warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p5 - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p5 - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p5 - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p5 - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p5 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p5 - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_response_warning_email_75</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p5 - response warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_resolution_breached_email_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - resolution breached email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_resolution_breached_email_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - resolution breached email - 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_resolution_breached_email_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - resolution breached email - 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>p5_svc_req_response_warning_email_75</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>p5 - svc req - response warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_resolution_breached_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>red p1 resolution breached 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_resolution_breached_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>red p1 resolution breached 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_resolution_breached_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>red p1 resolution breached 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_resolution_warning_email_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>red p1 - resolution warning email - 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_resolution_warning_email_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>red p1 - resolution warning email - 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>red p1 - response failure email - 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p1_response_warning_email_0</fullName>
        <ccEmails>priority@cloudreach.co.uk</ccEmails>
        <description>red p1 - response warning email - 0%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_resolution_breach_100</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>red p2 resolution breach 100%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_resolution_breach_125</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>red p2 resolution breach 125%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_resolution_breach_150</fullName>
        <ccEmails>sla.breach@cloudreach.co.uk</ccEmails>
        <description>red p2 resolution breach 150%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Breached_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_resolution_warning_50</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>red p2 resolution warning 50%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_resolution_warning_75</fullName>
        <ccEmails>sla.approach@cloudreach.co.uk</ccEmails>
        <description>red p2 resolution warning 75%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Resolution_Warning_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_response_failure_email_100</fullName>
        <ccEmails>response.failure@cloudreach.co.uk</ccEmails>
        <description>red p2 - response failure email - 100%</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Failure_Email</template>
    </alerts>
    <alerts>
        <fullName>red_p2_response_warning_email_0</fullName>
        <ccEmails>priority@cloudreach.co.uk</ccEmails>
        <description>red p2 - response warning email - 0%</description>
        <protected>false</protected>
        <recipients>
            <recipient>support.team@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Cases_SLA_Response_Warning_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Cases_Escalate_to_Support_Team</fullName>
        <field>Escalation__c</field>
        <literalValue>Support Team</literalValue>
        <name>Cases: Escalate to Support Team</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cases_Set_Last_Updated_By_Apex_To_False</fullName>
        <field>Last_Updated_By_Apex__c</field>
        <literalValue>0</literalValue>
        <name>Cases: Set Last Updated By Apex To False</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cases_Update_escalation_field</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>Cases: Update escalation field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Assigning</fullName>
        <field>Status</field>
        <literalValue>Assigning</literalValue>
        <name>Change to Assigning</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Pending_Cloudreach</fullName>
        <description>Change to Pending Cloudreach</description>
        <field>Status</field>
        <literalValue>Pending Cloudreach Response</literalValue>
        <name>Change to Pending Cloudreach</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_case</fullName>
        <field>Status</field>
        <literalValue>Closed</literalValue>
        <name>Close case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>No_reply_from_customer</fullName>
        <field>Reason</field>
        <literalValue>No reply from Customer</literalValue>
        <name>No reply from customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Case_Clock</fullName>
        <description>Resets the Case Clock field.</description>
        <field>Clock__c</field>
        <formula>0.0</formula>
        <name>Reset Case Clock</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Assigned_Engineer_Email</fullName>
        <field>Assigned_Engineer_Email__c</field>
        <formula>SUBSTITUTE(LOWER(TEXT(Assigned_Engineer__c)), &quot; &quot;, &quot;.&quot;)  &amp; &quot;@cloudreach.co.uk&quot;</formula>
        <name>Update Assigned Engineer Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Community_Record_Type</fullName>
        <description>updates filed type from community user to standard user</description>
        <field>RecordTypeId</field>
        <lookupValue>Standard_User_Picklist_Value</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Community Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>amber_p1_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>amber p1 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>amber_p1_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>amber p1 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p2_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p2 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p2_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p2 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p3_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p3 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p3_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p3 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p3_svc_req_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p3 - svc req - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p3_svc_req_set_escalated_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p3 - svc req - set escalated manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p4_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p4 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p4_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p4 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p4_svc_req_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p4 - svc req - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p4_svc_req_set_escalated_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p4 - svc req - set escalated manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p5_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p5 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p5_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p5 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p5_svc_req_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>p5 - svc req - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>p5_svc_req_set_escalated_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>p5 - svc req - set escalated manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>red_p1_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>red p1 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>red_p1_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>red p1 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>red_p2_set_as_escalated</fullName>
        <field>IsEscalated</field>
        <literalValue>1</literalValue>
        <name>red p2 - set as escalated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>red_p2_set_escalation_manager</fullName>
        <field>Escalation__c</field>
        <literalValue>Escalation Manager</literalValue>
        <name>red p2 - set escalation manager</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Cases - Community user ownerships</fullName>
        <actions>
            <name>Update_Community_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>contains</operation>
            <value>Customer Portal Manager,Customer Portal User,High Volume Portal</value>
        </criteriaItems>
        <description>workflow rule that changes the new communityl case record type to your standard record type when a new case is created by a Community user</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Auto Close after 5 days</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Solution offered</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.AutoClose__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Close a case after 5 days of Solution offered</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Case_Closed_Due_to_Lack_of_Activity</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Close_case</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>No_reply_from_customer</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>5</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cases%3A Change status for new cases to Assigning</fullName>
        <actions>
            <name>Change_to_Assigning</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Changes the case status, for new Cases to Assigning</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Change to Pending Cloudreach on Assigned Eng</fullName>
        <actions>
            <name>Change_to_Pending_Cloudreach</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Assigned_Engineer__c</field>
            <operation>notEqual</operation>
            <value>Not Assigned</value>
        </criteriaItems>
        <description>Chnage to Pending Cloudreach Response when an Engeener is assigned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Escalate to Assigned Engineer</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Cloudreach Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident,Service Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cases_Email_Assigned_Engineer_Day_3</name>
                <type>Alert</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cases_Email_Assigned_Engineer_Day_2</name>
                <type>Alert</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cases_Email_Assigned_Engineer_Day_4</name>
                <type>Alert</type>
            </actions>
            <timeLength>4</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Cases_Email_Assigned_Engineer_Day_1</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cases%3A Escalate to Escalation Manager</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Cloudreach Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident,Service Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cases_Send_email_to_Escalation_Manager</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Cases_Update_escalation_field</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cases%3A Escalate to Support Team</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Cloudreach Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Type</field>
            <operation>equals</operation>
            <value>Incident,Service Request</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cases_Support_Team_Escalation</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Cases_Escalate_to_Support_Team</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Cases%3A New case received</fullName>
        <actions>
            <name>Cases_Send_email_to_Support_Team_when_new_case_received</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Assigned_Engineer__c</field>
            <operation>equals</operation>
            <value>Not Assigned</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Reset Last Updated By Apex Field</fullName>
        <actions>
            <name>Cases_Set_Last_Updated_By_Apex_To_False</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Last_Updated_By_Apex__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send email to Christine for Audi</fullName>
        <actions>
            <name>Email_alert_for_Aui_P1_P2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3</booleanFilter>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1,P2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Audi</value>
        </criteriaItems>
        <description>This will email Christine whenever a P1/P2 case for Audi is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send email to Christine on new Audi P1-P4 cases</fullName>
        <actions>
            <name>Cases_P1_P4</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1,P2,P3,P4</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Audi</value>
        </criteriaItems>
        <description>this will email Christine whenever a P1 to P4 case for Audi is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send email to Evrard for new Anovo and KeepCool</fullName>
        <actions>
            <name>Cases_Alert_Evrard</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1,P2,P3,P4,P5</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Keep Cool,ANOVO</value>
        </criteriaItems>
        <description>Send notfication to Evrard each time a case is logged for Anovo and KeepCool - all priorities.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send email to Robert for any Hearst P1 cases raised</fullName>
        <actions>
            <name>Hearst_P1_alerts_Robert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR 11</booleanFilter>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Magazines Netherlands B.V</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst HTV Support Desk</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>[Hearst UK] The National Magazine Company Limited</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Corporate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Digital Media</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Business Media</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Communications</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Magazines</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Hearst Technology</value>
        </criteriaItems>
        <description>this will email Robert whenever a P1 case for Hearst is created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send email to Tony for BP CAB RI</fullName>
        <actions>
            <name>Cases_P1_P2_Alert_Tony</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>(1 AND 2 AND 3) OR 4 OR 5</booleanFilter>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1,P2</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>BP</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>Citizens Advice</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>equals</operation>
            <value>River Island</value>
        </criteriaItems>
        <description>This will email Tony whenever a P1/P2 case for BP, CAB or RI is closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send email to sales if P1 case closed</fullName>
        <actions>
            <name>Cases_P1_Case_Alert_Sales</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Priority</field>
            <operation>equals</operation>
            <value>P1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.AccountName</field>
            <operation>notEqual</operation>
            <value>Cloudreach</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send notification to Support Team that the case has been assigned</fullName>
        <actions>
            <name>Cases_Send_email_to_Support_Team_when_new_case_has_been_assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Assigned_Engineer__c</field>
            <operation>notEqual</operation>
            <value>Not Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>Assigning,New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send survey to case creator on case close</fullName>
        <actions>
            <name>Send_email_survey_to_case_creator_second</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7 AND 8 AND 9 AND 10 AND 11 AND 12 AND 13 AND 14 AND 15 AND 16 AND 17</booleanFilter>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notContain</operation>
            <value>Spam</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Reason</field>
            <operation>notContain</operation>
            <value>Duplicate</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>Pon</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>PON</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notContain</operation>
            <value>pon.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.SuppliedEmail</field>
            <operation>notEqual</operation>
            <value>team@audiondemand.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notEqual</operation>
            <value>Audi</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Send_Feedback_Survey__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>Eircom</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
            <value>servicedesk@eircom.ie</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Name</field>
            <operation>notContain</operation>
            <value>OIA Global</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Email</field>
            <operation>notContain</operation>
            <value>oiaglobal.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_Engineer__c</field>
            <operation>notEqual</operation>
            <value>To Be Deleted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
            <value>devops@hearstcorp.pagerduty.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
            <value>email@hearst-magazines-digital-media.pagerduty.com</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.ContactEmail</field>
            <operation>notEqual</operation>
            <value>hearstet@service-now.com</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Send update email to Assigned Engineer</fullName>
        <actions>
            <name>Cases_Send_email_to_Assigned_Engineer_on_case_assignment_or_change</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Assigned_Engineer_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Vendor Response,Pending Cloudreach Response,Assigning,Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Assigned_Engineer__c</field>
            <operation>notEqual</operation>
            <value>Not Assigned</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Last_Updated_By_Apex__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Update Assigned Engineer Email</fullName>
        <actions>
            <name>Update_Assigned_Engineer_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Assigned_Engineer__c</field>
            <operation>notEqual</operation>
            <value>Not Assigned</value>
        </criteriaItems>
        <description>Updates the Assigned Engineer Email field when the Assigned Engineer is assigned or changed.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Update Case Clock After Insert</fullName>
        <actions>
            <name>Reset_Case_Clock</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Case Clock field after Case insertion so that the Case update trigger is fired, given that the Case status is NOT one for which the Case clock should be stopped.</description>
        <formula>!ISPICKVAL(Status, &apos;Pending Vendor&apos;) &amp;&amp; !ISPICKVAL(Status, &apos;Pending Customer&apos;) &amp;&amp; !ISPICKVAL(Status, &apos;Solution Offered&apos;) &amp;&amp; !ISPICKVAL(Status, &apos;Closed&apos;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>SLA check assigned within 2 hours</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigning</value>
        </criteriaItems>
        <description>Check that once a ticket has been received it is assigned within 2 hours - i.e. within SLA</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Escalate_case_not_assigned</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.CreatedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
