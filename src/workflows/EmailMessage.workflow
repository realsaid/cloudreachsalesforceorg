<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_to_Pending_Cloudreach2</fullName>
        <field>Status</field>
        <literalValue>Pending Cloudreach Response</literalValue>
        <name>Change to Pending Cloudreach2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Pending_Customer_Response</fullName>
        <description>Change Status field to Pending Customer response</description>
        <field>Status</field>
        <literalValue>Pending Customer Response</literalValue>
        <name>Change to Pending Customer Response</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Cases%3A Pending Cloudreach  customer email</fullName>
        <actions>
            <name>Change_to_Pending_Cloudreach2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Response</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Change to Pending Cloudreach when a customer replies via email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Pending Cloudreach after email</fullName>
        <actions>
            <name>Change_to_Pending_Cloudreach2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Customer Response,Pending Vendor Response,Solution offered,Internal - Pending Implementation,RFC - Pending Implementation,On Hold</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Change to Pending CR when an email is received &amp; ticket was pending Customer/Developer/Vendor/solution offered
This is deliberately vague as if the customer or vendor/developer emails a ticket it will require an action irrespective of the previos satate</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Pending Customer after Cloudreach email</fullName>
        <actions>
            <name>Change_to_Pending_Customer_Response</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Cloudreach Response</value>
        </criteriaItems>
        <description>Change to Pending Customer when Cloudreach replies via email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Cases%3A Re-Open closed case on customer email</fullName>
        <actions>
            <name>Change_to_Pending_Cloudreach2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Change to Pending Cloudreach when a customer replies via email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
