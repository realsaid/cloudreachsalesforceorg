<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Acct_1_hr_SLA_Breach_Urgent_Request_for_FA_or_NDA_to_be_EchoSigned</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <ccEmails>donal.fullerton@cloudreach.com</ccEmails>
        <description>Acct: 1 hr SLA Breach Urgent Request for FA or NDA to be EchoSigned</description>
        <protected>false</protected>
        <recipients>
            <recipient>andre.azevedo@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_1_hr_SLA_breach_Urgent_Request_to_send_FA_or_NDA_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>Acct_8_hour_SLA_breach</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <ccEmails>donal.fullerton@cloudreach.com</ccEmails>
        <description>Acct: 8 hour SLA breach</description>
        <protected>false</protected>
        <recipients>
            <recipient>andre.azevedo@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_8_hr_SLA_Breach_Request_to_send_FA_or_NDA_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>Acct_Request_for_FA_or_NDA_to_be_EchoSigned</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <description>Acct: Request for FA or NDA to be EchoSigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Request_to_send_FA_or_NDA_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>Acct_Urgent_Request_for_FA_or_NDA_to_be_EchoSigned</fullName>
        <ccEmails>commercial.support@cloudreach.com</ccEmails>
        <description>Acct: Urgent Request for FA or NDA to be EchoSigned</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>william.taylor@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Urgent_Request_to_send_FA_or_NDA_EchoSign</template>
    </alerts>
    <alerts>
        <fullName>BU_Lead_approval_of_Customer_PO_Terms</fullName>
        <description>BU Lead approval of Customer PO Terms</description>
        <protected>false</protected>
        <recipients>
            <recipient>andre.azevedo@cloudreach.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Non_negotiated_Customer_terms_approved_by_Legal</template>
    </alerts>
    <alerts>
        <fullName>Customer_PO_Terms_approved</fullName>
        <description>Customer PO Terms approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Customer_PO_Terms_Approved</template>
    </alerts>
    <alerts>
        <fullName>Customer_PO_Terms_rejected</fullName>
        <description>Customer PO Terms rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Customer_PO_Terms_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Founder_approval_of_Customer_PO_Terms</fullName>
        <description>Founder approval of Customer PO Terms</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.monico@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pontus.noren@cloudreach.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Customer_PO_terms_approved_by_Legal</template>
    </alerts>
    <alerts>
        <fullName>Framework_agreement_approved</fullName>
        <description>Framework agreement approved</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Framework_Agreement_Approved</template>
    </alerts>
    <alerts>
        <fullName>Framework_agreement_rejected</fullName>
        <description>Framework agreement rejected</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_Framework_Agreement_Rejected</template>
    </alerts>
    <alerts>
        <fullName>NDA_Approved_by_Legal_Team</fullName>
        <description>NDA Approved by Legal Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_NDA_Approved</template>
    </alerts>
    <alerts>
        <fullName>NDA_Not_Approved_by_Legal_Team</fullName>
        <description>NDA Not Approved by Legal Team</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Acct_NDA_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_profile_update_A2</fullName>
        <field>Account_Profile__c</field>
        <literalValue>A2</literalValue>
        <name>Account profile update A2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_profile_update_B</fullName>
        <field>Account_Profile__c</field>
        <literalValue>B</literalValue>
        <name>Account profile update B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_profile_update_C</fullName>
        <field>Account_Profile__c</field>
        <literalValue>C</literalValue>
        <name>Account profile update C</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_profile_update_PS</fullName>
        <field>Account_Profile__c</field>
        <literalValue>Public Sector</literalValue>
        <name>Account profile update PS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Agreement_Terms_Recalled</fullName>
        <description>Agreement has been recalled from the approval process</description>
        <field>Agreement_terms_status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Agreement Terms Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_PO_Terms_Approved</fullName>
        <field>Agreement_terms_status__c</field>
        <literalValue>Customer PO Terms Approved</literalValue>
        <name>Customer PO Terms Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Customer_PO_Terms_Rejected</fullName>
        <field>Agreement_terms_status__c</field>
        <literalValue>Customer PO Terms Rejected</literalValue>
        <name>Customer PO Terms Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Framework_Agreement_Approved</fullName>
        <description>Framework Agreement approved</description>
        <field>Agreement_terms_status__c</field>
        <literalValue>Framework Agreement Approved</literalValue>
        <name>Framework Agreement Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Framework_Agreement_Rejected</fullName>
        <description>Framework Agreement has been rejected</description>
        <field>Agreement_terms_status__c</field>
        <literalValue>Framework Agreement Rejected</literalValue>
        <name>Framework Agreement Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_as_PO_Terms_used_once</fullName>
        <description>Updates account to show that PO Terms have been used once so cannot be used again.</description>
        <field>Customer_used_PO_terms_previously__c</field>
        <literalValue>1</literalValue>
        <name>Mark as PO Terms used once</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_approval</fullName>
        <description>Framework agreement submitted for approval</description>
        <field>Agreement_terms_status__c</field>
        <literalValue>Submitted for approval</literalValue>
        <name>Submitted for approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account profile update A2</fullName>
        <actions>
            <name>Account_profile_update_A2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates account profile according to Global Turnover value selected</description>
        <formula>ISPICKVAL( Global_turnover_value__c , &quot;$1B - $10B&quot;)  ||  ISPICKVAL( Global_turnover_value__c , &quot;&gt;$10B&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account profile update B</fullName>
        <actions>
            <name>Account_profile_update_B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates account profile according to Global Turnover value selected</description>
        <formula>ISPICKVAL( Global_turnover_value__c , &quot;$500M - $1B&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account profile update C</fullName>
        <actions>
            <name>Account_profile_update_C</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates account profile according to Global Turnover value selected</description>
        <formula>ISPICKVAL( Global_turnover_value__c , &quot;&lt;$500M&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account profile update PS</fullName>
        <actions>
            <name>Account_profile_update_PS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates account profile according to Global Turnover value selected</description>
        <formula>ISPICKVAL( Global_turnover_value__c , &quot;Public Sector&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Acct%3A Request to send FA or NDA EchoSign</fullName>
        <actions>
            <name>Acct_Request_for_FA_or_NDA_to_be_EchoSigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Send_request_for_EchoSign__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Agreement_EchoSigned__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Urgent__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EchoSign_Cancelled_Declined__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EchoSign_out_for_signature__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to request that an FA or NDA is sent for EchoSigning</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Acct_8_hour_SLA_breach</name>
                <type>Alert</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Acct%3A Urgent Request to send FA or NDA EchoSign</fullName>
        <actions>
            <name>Acct_Urgent_Request_for_FA_or_NDA_to_be_EchoSigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Send_request_for_EchoSign__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Agreement_EchoSigned__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Urgent__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EchoSign_Cancelled_Declined__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.EchoSign_out_for_signature__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <description>Workflow to request that an FA or NDA is sent for EchoSigning urgently</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Acct_1_hr_SLA_Breach_Urgent_Request_for_FA_or_NDA_to_be_EchoSigned</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
