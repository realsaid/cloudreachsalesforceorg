<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateGermany_Company</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach GmbH</literalValue>
        <name>Update Germany Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Canada_Company</fullName>
        <field>CompanyName</field>
        <formula>IF(
 Country = &quot;Canada&quot;, &quot;Cloudreach Canada Inc&quot;, null
)</formula>
        <name>Update Canada Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Canada_Company2</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach Canada Inc</literalValue>
        <name>Update Canada Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_FranceCompany</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach SAS</literalValue>
        <name>Update France Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_France_Company</fullName>
        <field>CompanyName</field>
        <formula>IF( 
Country = &quot;France&quot;, &quot;Cloudreach SAS&quot;, null 
)</formula>
        <name>Update France Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Germany_Company</fullName>
        <field>CompanyName</field>
        <formula>IF(
Country = &quot;Germany&quot;, &quot;Cloudreach GmbH&quot;, null
)</formula>
        <name>Update Germany Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Netherlands_Company</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach B.V.</literalValue>
        <name>Update Netherlands Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Switzerland_Company</fullName>
        <field>CompanyName</field>
        <formula>IF (
Country = &quot;Switzerland&quot;, &quot;Cloudreach Swiss GmbH&quot;, null
)</formula>
        <name>Update Switzerland Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Switzerlandd_Company</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach Swiss GmbH</literalValue>
        <name>Update Switzerland Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UK_Company</fullName>
        <field>CompanyName</field>
        <formula>IF(
$User.Country = &quot;USA&quot;, &quot;Cloudreach Inc&quot;,
null
)</formula>
        <name>Update USA Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UK_Company_User</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach Europe Limited</literalValue>
        <name>Update_UK_Company_User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UK_Edinburgh_Company</fullName>
        <field>CompanyName</field>
        <formula>IF (
Country = &quot;United Kingdom&quot; &amp;&amp; City = &quot;Edinburgh&quot;, &quot;Cloudreach Holdings Limited&quot;, null
)</formula>
        <name>Update UK Edinburgh Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_UK_London_Company</fullName>
        <field>CompanyName</field>
        <formula>IF (
 Country = &quot;United Kingdom&quot;  &amp;&amp; City = &quot;London&quot;, 	
&quot;Cloudreach Europe Limited&quot;, null
)</formula>
        <name>Update UK London Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_USA_Company</fullName>
        <field>FF_Company__c</field>
        <literalValue>Cloudreach Inc.</literalValue>
        <name>Update USA Company</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>User%3A Update Canada Company%27s User</fullName>
        <actions>
            <name>Update_Canada_Company2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>Canada</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Update France Company%27s User</fullName>
        <actions>
            <name>Update_FranceCompany</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>France</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Update Germany Company%27s User</fullName>
        <actions>
            <name>UpdateGermany_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>Germany</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Update Netherlands Company%27s User</fullName>
        <actions>
            <name>Update_Netherlands_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>Netherlands</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Update Switzerland Company%27s User</fullName>
        <actions>
            <name>Update_Switzerlandd_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>Switzerland</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Update UK Company%27s User</fullName>
        <actions>
            <name>Update_UK_Company_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>United Kingdom</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>User%3A Update USA Company%27s User</fullName>
        <actions>
            <name>Update_USA_Company</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Country</field>
            <operation>equals</operation>
            <value>USA</value>
        </criteriaItems>
        <description>Update company&apos;s users according the country field</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
