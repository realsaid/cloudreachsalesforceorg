<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>AccountId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ActivationDate</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>ApprovalStatus</fullName>
        <picklist>
            <picklistValues>
                <fullName>Draft</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Activated</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>BillingAddress</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>ContactId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CurrencyIsoCode</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Description</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Discount</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>EndDate</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>GrandTotal</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>LineItemCount</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Name</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>This is the opportunity that paid for this periods service contract</inlineHelpText>
        <label>Original Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Service Contracts</relationshipLabel>
        <relationshipName>Service_Contracts</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>OwnerId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ParentServiceContractId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Renewal_Opportunity__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <inlineHelpText>This is the opportunity that we hope will renew the contract.</inlineHelpText>
        <label>Renewal Opportunity</label>
        <referenceTo>Opportunity</referenceTo>
        <relationshipLabel>Service Contracts (Renewal Opportunity)</relationshipLabel>
        <relationshipName>Service_Contracts1</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Renewal_Type__c</fullName>
        <description>Determines whether we automatically generate a new service contract.</description>
        <externalId>false</externalId>
        <inlineHelpText>Determines wether the service contract is likely to renew or not. All contracts, except for those that we know for sure (like pilots), should be set to Renewal Expected.</inlineHelpText>
        <label>Renewal Type</label>
        <picklist>
            <picklistValues>
                <fullName>Renewal Expected</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No Renewal</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Auto Renewal</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>RootServiceContractId</fullName>
        <trackHistory>false</trackHistory>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Service_Contract_Name__c</fullName>
        <externalId>false</externalId>
        <label>Service Contract Name</label>
        <picklist>
            <picklistValues>
                <fullName>Elastic Consulting T&amp;M</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AWS Managed Services 24/7 P1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AWS Managed Services 24/7 P1 and P2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AWS Managed Services 9/6 GMT/BST</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>AWS Managed Services 9/6 EST/EDT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Google Apps Managed Services 24/7 P1</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Google Apps Managed Services 24/7 P1 and P2</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Google Apps Managed Services GMT/BST</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Google Apps Managed Services EST/EDT</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Application Development Managed Services 9/6 GMT/BST</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Alumina Managed Services 9/6</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SFDC Managed Service 9/6 GMT/BST</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Service_Contract_Number__c</fullName>
        <displayFormat>SC{0000}</displayFormat>
        <externalId>false</externalId>
        <label>Service Contract Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </fields>
    <fields>
        <fullName>Service_Review_Date__c</fullName>
        <description>Date of SR</description>
        <externalId>false</externalId>
        <label>Service Review Date</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Service_Review_Frequency__c</fullName>
        <externalId>false</externalId>
        <label>Service Review Frequency</label>
        <picklist>
            <picklistValues>
                <fullName>Bi-Weekly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Monthly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quarterly</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Not Required</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>MultiselectPicklist</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Service_Review_Owner__c</fullName>
        <externalId>false</externalId>
        <label>Service Review Owner</label>
        <picklist>
            <picklistValues>
                <fullName>Alberto Alvarez</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Andrew Philp</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Antoine Noel</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Anton Chen</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Asser Hassan</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Ben Ezard</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Christopher Williamson</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Col Birkett</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Colin Bruce</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Daniel Shelley</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>David Elliott</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Euan Reid</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>George Svachulay</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Gergo Szollosi</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Gordon Thomson</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Jared Chia</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Jon Gasparini</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Jon Hall</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Kieran Doonan</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Liam Caproni</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Liam Dow</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Lloyd Wyngard</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Mark Kohler</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Martyn Langlands</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Neil Stewart</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Peter Cridland</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Peter Waitland</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Petr Hecko</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Rashid Niazi</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Robbie Clews</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sean Bramhall</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Sebastian Kasprzak</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Shona Mckenzie</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Service_Review_Type__c</fullName>
        <description>Is this Base or Enterprise review as per OF</description>
        <externalId>false</externalId>
        <inlineHelpText>Base includes incident / service request metrics only, it does not include capacity data
Enterprise includes everything including capacity graphs</inlineHelpText>
        <label>Service Review Type</label>
        <picklist>
            <picklistValues>
                <fullName>Base</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CSS</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Enterprise</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>ShippingAddress</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>ShippingHandling</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>SpecialTerms</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>StartDate</fullName>
        <inlineHelpText>This date represents the Service Start Date in our contracts</inlineHelpText>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>Status</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>StatusIndicator</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Subtotal</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Tax</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>Term</fullName>
        <trackHistory>true</trackHistory>
    </fields>
    <fields>
        <fullName>TotalPrice</fullName>
        <trackHistory>false</trackHistory>
    </fields>
    <fields>
        <fullName>True_up_Type__c</fullName>
        <description>This defines if the service contract involves a true up</description>
        <externalId>false</externalId>
        <label>True-Up Type</label>
        <picklist>
            <picklistValues>
                <fullName>Custom True-Up</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Monthly True-Up</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No True-Up</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Quarterly True-Up</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <type>Picklist</type>
    </fields>
    <listViews>
        <fullName>All_ServiceContracts</fullName>
        <columns>CONTRACT.NAME</columns>
        <columns>ACCOUNT.NAME</columns>
        <columns>Service_Contract_Name__c</columns>
        <columns>CONTRACT.DESCRIPTION</columns>
        <columns>CONTRACT.CONTRACT_NUMBER</columns>
        <columns>CONTRACT.APPROVALSTATUS</columns>
        <columns>CONTRACT.STARTDATE</columns>
        <columns>CONTRACT.ENDDATE</columns>
        <columns>CONTRACT.STATUS</columns>
        <columns>CONTRACT.CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CONTRACT.STATUS</field>
            <operation>equals</operation>
            <value>0,1,2</value>
        </filters>
        <label>All Service Contracts</label>
    </listViews>
    <listViews>
        <fullName>Op_s_with_true_up</fullName>
        <columns>CONTRACT.NAME</columns>
        <columns>CONTRACT.STARTDATE</columns>
        <columns>CONTRACT.ENDDATE</columns>
        <columns>CONTRACT.APPROVALSTATUS</columns>
        <columns>True_up_Type__c</columns>
        <columns>CONTRACT.STATUS</columns>
        <columns>CONTRACT.STATUSINDICATOR</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>True_up_Type__c</field>
            <operation>equals</operation>
            <value>Custom True-Up,Monthly True-Up,Quarterly True-Up</value>
        </filters>
        <filters>
            <field>CONTRACT.STATUS</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Op&apos;s with true-up</label>
    </listViews>
    <searchLayouts>
        <customTabListAdditionalFields>CONTRACT.NAME</customTabListAdditionalFields>
        <customTabListAdditionalFields>CONTRACT.STARTDATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>CONTRACT.ENDDATE</customTabListAdditionalFields>
        <customTabListAdditionalFields>CONTRACT.APPROVALSTATUS</customTabListAdditionalFields>
        <customTabListAdditionalFields>Opportunity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CONTRACT.STATUS</customTabListAdditionalFields>
        <searchResultsAdditionalFields>CONTRACT.NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CONTRACT.STARTDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CONTRACT.ENDDATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CONTRACT.APPROVALSTATUS</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CONTRACT.STATUS</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CONTRACT.STATUSINDICATOR</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <validationRules>
        <fullName>End_Date_Required</fullName>
        <active>true</active>
        <description>An end date is required if</description>
        <errorConditionFormula>AND (ISBLANK( EndDate ),  ISPICKVAL( Renewal_Type__c , &quot;Renewal Expected&quot;) )</errorConditionFormula>
        <errorDisplayField>EndDate</errorDisplayField>
        <errorMessage>Please enter an End Date when the renewal is expected</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Renewal_Type_Required</fullName>
        <active>true</active>
        <errorConditionFormula>AND( !(ISBLANK(EndDate)), TEXT(Renewal_Type__c )=&quot;&quot; )</errorConditionFormula>
        <errorDisplayField>Renewal_Type__c</errorDisplayField>
        <errorMessage>Please choose a Renewal Type</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Trueup_Type_Required</fullName>
        <active>true</active>
        <errorConditionFormula>AND( !(ISBLANK(EndDate)), TEXT(True_up_Type__c )=&quot;&quot; )</errorConditionFormula>
        <errorDisplayField>True_up_Type__c</errorDisplayField>
        <errorMessage>Please choose a True-Up Type</errorMessage>
    </validationRules>
</CustomObject>
