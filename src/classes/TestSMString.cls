@isTest
public with sharing class TestSMString {
	static testMethod void shouldIndicateNullStringIsNull() {
		SMString theString = new SMString(null);
		
		Boolean result = theString.isNull();
		
		System.assertEquals(true, result);
	}
	
	static testMethod void shouldIndicateEmptyStringIsNull() {
        SMString theString = new SMString('         ');
		
		Boolean result = theString.isNull();
		
		System.assertEquals(true, result);
    }
    
    static testMethod void shouldNotIndicateNonEmptyStringIsNull() {
        SMString theString = new SMString('a non-null string');
		
		Boolean result = theString.isNull();
		
		System.assertEquals(false, result);
    }
    
	static testMethod void shouldNotIndicateWordIsInListIfListIsNull() {
		SMString theString = new SMString('a non-null string');
		
    	Boolean result = theString.isInList(null);
    	
    	System.assertEquals(false, result);
    }
    
    static testMethod void shouldNotIndicateWordIsInListIfWordIsNotInList() {
    	SMString theString = new SMString('word0');
		
    	Boolean result = theString.isInList(new String[]{'word1', 'word2'});
    	
    	System.assertEquals(false, result);
    }
    
    static testMethod void shouldIndicateWordIsInListIfWordIsInList() {
        SMString theString = new SMString('word1');
		
    	Boolean result = theString.isInList(new String[]{'word1', 'word2'});
    	
    	System.assertEquals(true, result);
    }
    
    static testMethod void shouldReturnCorrectStringGivenInstantiationWasFromStringArray() {
    	String[] stringArray = new String[]{'string1', 'string2', 'string3'};
    	
    	SMString smString = new SMString(stringArray, ',');
    	
    	System.assertEquals('string1,string2,string3', smString.toSFString());
    }
}