@isTest
private class testAttachmentTriggerInsert{
    private static testMethod void unitTestInsert()
    {
        system.runas(TestDataSet.getTestUser()){ 
            Opportunity opp = new OpportunityBuilder().build();
            insert opp;
            Attachment attachment = new Attachment(Name='Order Form',body=blob.valueof('b'),parentId=opp.Id);
            try{
                insert attachment;
            }
            catch(exception e)
                {}
            
            Opportunity testopp = [Select Proposed_Order_Form__c  from Opportunity where Id = :opp.Id LIMIT 1];
            String orderForm = testopp.Proposed_Order_Form__c ;
            System.assertEquals(System.URL.getSalesforceBaseURL().getHost()+'/servlet/servlet.FileDownload?file='+attachment.Id,orderForm);
        }
    }
    private static testMethod void unitTestInsert2()
    {
        system.runas(TestDataSet.getTestUser()){ 
            Opportunity opp = new OpportunityBuilder().build();
            insert opp;
            Attachment attachment = new Attachment(Name='OrderForm',body=blob.valueof('b'),parentId=opp.Id);
            try{
                insert attachment;
            }
            catch(exception e)
                {}
            
            Opportunity testopp = [Select Proposed_Order_Form__c  from Opportunity where Id = :opp.Id LIMIT 1];
            String orderForm = testopp.Proposed_Order_Form__c ;
            System.assertEquals(System.URL.getSalesforceBaseURL().getHost()+'/servlet/servlet.FileDownload?file='+attachment.Id,orderForm);
        }
    }
}