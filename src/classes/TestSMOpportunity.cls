@isTest(seeAllData=true)
public with sharing class TestSMOpportunity {
	private static final Integer N_LINE_ITEMS = 10;
	
	static testMethod void shouldInsertCloneOfItselfWithLineItems() {
        TestDataSet.insertOpportunityCloneSettings();
        Opportunity opp;
        Opportunity clonedOpp;
        SMOpportunity smOpp;
        system.runas(TestDataSet.getTestUser()){
            opp = new OpportunityBuilder().build();
            insert opp;
            OpportunityLineItem[] items1 = generateLineItemsFor(opp, N_LINE_ITEMS);
            insert items1;
            smOpp = new SMOpportunity(opp);
            
            Test.startTest();
            clonedOpp = smOpp.insertClone();
            Test.stopTest();
        }
		String query = new SelectAll('Opportunity').getSOQL() + ' WHERE ID = ' + new SMString(clonedOpp.ID).inQuotes() + ' limit 1';
		clonedOpp = Database.Query(query);
		query = new SelectAll('Opportunity').getSOQL() + ' WHERE ID = ' + new SMString(opp.ID).inQuotes() + ' limit 1';
		smOpp = new SMOpportunity(Database.Query(query));
		System.assertEquals(smOpp.getLineItems().size(), new SMOpportunity(clonedOpp).getLineItems().size());
	}
	
	static testMethod void shouldHaveProductsInFamilyGivenOpportunityHasProductsInManagedServicesFamily() {
		Opportunity opp;
        system.runas(TestDataSet.getTestUser()){
            opp = new OpportunityBuilder().build();
            insert opp;
            insert generateLineItemsFor(opp, N_LINE_ITEMS, SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES);
            
            Test.startTest();
            Boolean hasManagedServicesProducts = new SMOpportunity(opp).hasProductsInFamily(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES);
            Test.stopTest();
            
            System.assertEquals(true, hasManagedServicesProducts);
        }
	}
	
	static testMethod void shouldNotHaveProductsInFamilyGivenOpportunityHasProductsThatAreNotInManagedServicesFamily() {
		Opportunity opp;
        system.runas(TestDataSet.getTestUser()){
            opp = new OpportunityBuilder().build();
            insert opp;
            insert generateLineItemsFor(opp, N_LINE_ITEMS, 'A Random Family');
            
            Test.startTest();
            Boolean hasManagedServicesProducts = new SMOpportunity(opp).hasProductsInFamily(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES);
            Test.stopTest();
            
            System.assertEquals(false, hasManagedServicesProducts);
        }
	}
	
	static testMethod void shouldGetNullPrimaryServiceContractGivenOpportunityDoesNotHaveAPrimaryServiceContract() {
		Opportunity opp;
        system.runas(TestDataSet.getTestUser()){
            opp = new OpportunityBuilder().build();
		
			ServiceContract gotPrimaryServiceContract = new SMOpportunity(opp).getPrimaryServiceContract();
		
			System.assertEquals(null, gotPrimaryServiceContract);
        }
	}
	
	static testMethod void shouldGetPrimaryServiceContractGivenOpportunityHasPrimaryServiceContract() {
        system.runas(TestDataSet.getTestUser()){
            Opportunity opp = new OpportunityBuilder().build();
            ServiceContract con = new ServiceContractBuilder().build();
            insert con;
            opp.Primary_Service_Contract__c = con.ID;
            
            ServiceContract gotPrimaryServiceContract = new SMOpportunity(opp).getPrimaryServiceContract();
            
            System.assertEquals(opp.Primary_Service_Contract__c, gotPrimaryServiceContract.ID);
        }
	}
    
    static testMethod void shouldCloseWinOpportunity() {
        system.runas(TestDataSet.getTestUser()){
            Opportunity opp = new OpportunityBuilder()
            .withType('Upsell')
            .withStageName('Suspect')
            .withOrderForm('https://docs.google.com/a/cloudreach.co.uk/blahblahblah')
            .build();
            insert opp;
            
            TestDataSet.insertOpportunitySignedAgreement(opp.Id);		
            opp.Apporved__c = true;
            opp.StageName = 'Closed Won';
            string errorMessage = '';
            try {
                update opp; 
            }
            catch (Exception e) {
            	errorMessage = e.getMessage();
           	 System.Debug('Exception caught in shouldCloseWinOpportunity: ' + errorMessage);       
        	}
		
			System.assertEquals(errorMessage, '');
        }
	}

    static testMethod void shouldConvertToString() {
        Opportunity opp = TestDataSet.getOpportunity();
        opp.Name = 'Opportunity Name';
        SMOpportunity smOpp = new SMOpportunity(opp);

        String str = smOpp.toString();

        System.assert(str.contains('Opportunity Name'));
    }

    static testMethod void shouldGetOpportunityRecord() {
        Opportunity opp = TestDataSet.getOpportunity();
        opp.Name = 'Opportunity Name';
        SMOpportunity smOpp = new SMOpportunity(opp);

        Opportunity record = smOpp.getRecord();

        System.assertEquals(opp.Name, record.Name);
    }

    static testMethod void shouldSetOpportunityStageName() {
        Opportunity opp = TestDataSet.getOpportunity();
        SMOpportunity smOpp = new SMOpportunity(opp);
        String stageName = 'Stage Name';

        smOpp.setStageName(stageName);

        System.assertEquals(stageName, smOpp.getRecord().StageName);
    }
	
	private static OpportunityLineItem[] generateLineItemsFor(Opportunity opportunity, Integer nItems) {
		return generateLineItemsFor(opportunity, nItems, SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES);
	}
	
	private static OpportunityLineItem[] generateLineItemsFor(Opportunity opportunity, Integer nItems, String productFamily) {
		OpportunityLineItem[] lineItems	= new OpportunityLineItem[]{};
		OpportunityLineItem lineItem = null;
		for (Integer i = 0; i < nItems; ++i) {
			lineItems.add(TestDataSet.getOpportunityLineItem(productFamily, opportunity.ID));
		}
		return lineItems;
	} 
	
	private static OpportunityLineItem[] assignLineItemsTo(OpportunityLineItem[] items, Opportunity opportunity) {
		for (OpportunityLineItem item : items) {
			item.OpportunityID = opportunity.ID;
		}
		return items;
	}
}