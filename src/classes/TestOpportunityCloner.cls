@isTest
public with sharing class TestOpportunityCloner {
	private static final String[] REQUIRED_FIELDS = new String[]{'Name', 'CloseDate', 'StageName'};
	
	private static void insertRequiredFieldset() {
		Opportunity_Clone_Fieldset__c[] fieldset = new Opportunity_Clone_Fieldset__c[]{};
		for (String field : REQUIRED_FIELDS) {
			String fieldLabel;
			fieldLabel = field.length() > 40 ? field.substring(0, 39) : field;
			fieldset.add(new Opportunity_Clone_Fieldset__c(Field_Name__c=field, Name=fieldLabel));	
		}
		insert fieldset;
	}
	
	static testMethod void shouldCloneOpportunityWithDescriptionGivenDescriptionHasBeenDeclaredForCloning() {
		insertRequiredFieldset();
		insert new Opportunity_Clone_Fieldset__c(Field_Name__c='Description', Name='Description');
		String description = 'description';
		Opportunity opp = new OpportunityBuilder().withDescription(description).build();
		insert opp;
		OpportunityCloner cloner = new OpportunityCloner();
		
		Opportunity clone = cloner.insertClone(opp.ID);
		
		System.assertEquals(opp.Description, clone.Description);
	}
	
	static testMethod void shouldNotCloneOpportunityWithDescriptionGivenDescriptionHasNotBeenDeclaredForCloning() {
		insertRequiredFieldset();
        String description = 'description';
        Opportunity opp = new OpportunityBuilder().withDescription(description).build();
		insert opp;
        OpportunityCloner cloner = new OpportunityCloner();
        
        Opportunity clone = cloner.insertClone(opp.ID);
        
        System.assertNotEquals(opp.Description, clone.Description);
    }
}