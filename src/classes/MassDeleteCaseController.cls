public with sharing class MassDeleteCaseController extends MassDeleteSObjectController {
	public MassDeleteCaseController(ApexPages.StandardSetController controller) {
        super(controller);
    }
    
    protected override PageReference showListView() {
        return new PageReference('/500');
    }
}