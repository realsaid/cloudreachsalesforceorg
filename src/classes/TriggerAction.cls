public with sharing virtual class TriggerAction {
	public virtual void onBeforeInsert(){
		throw new TriggerException(TriggerException.METHOD_NOT_IMPLEMENTED);
	}
	
	public virtual void onAfterInsert(){
		throw new TriggerException(TriggerException.METHOD_NOT_IMPLEMENTED);
	}
	
	public virtual void onBeforeUpdate(){
		throw new TriggerException(TriggerException.METHOD_NOT_IMPLEMENTED);
	}
	
	public virtual void onAfterUpdate(){
		throw new TriggerException(TriggerException.METHOD_NOT_IMPLEMENTED);
	}
}