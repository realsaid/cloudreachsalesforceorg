@isTest
public with sharing class TestSMDate {
	static testMethod void shouldConvertToSFDateObject() {
		Date originalDate = Date.today();
		SMDate smDate = new SMDate(originalDate);	
		
		Date convertedDate = smDate.toSFObject();
		
		System.assertEquals(originalDate, convertedDate);
	}
	
	static testMethod void shouldReturnSundayForDate01012012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 1), DayOfWeek.SUNDAY);
	}
	
	static testMethod void shouldReturnMondayForDate01022012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 2), DayOfWeek.MONDAY);
	}
	
	static testMethod void shouldReturnTuesdayForDate01032012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 3), DayOfWeek.TUESDAY);
	}
	
	static testMethod void shouldReturnWednesdayForDate01042012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 4), DayOfWeek.WEDNESDAY);
	}
	
	static testMethod void shouldReturnThursdayForDate01052012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 5), DayOfWeek.THURSDAY);
	}
	
	static testMethod void shouldReturnFridayForDate01062012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 6), DayOfWeek.FRIDAY);
	}
	
	static testMethod void shouldReturnSaturdayForDate07012012() {
		shouldReturnDayForDate(Date.newInstance(2012, 1, 7), DayOfWeek.SATURDAY);
	}
	
	private static void shouldReturnDayForDate(Date theDate, DayOfWeek day) {
		SMDate smDate = new SMDate(theDate);
		System.assertEquals(day, smDate.getDayOfWeek());
	}
}