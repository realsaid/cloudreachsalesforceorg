public with sharing class SalesInvoiceExtension {
  public c2g__codaInvoice__c invoice {get;set;}
  public list <c2g__codaInvoice__c> bankAccountList {get;set;}
  public list <c2g__codaBankAccount__c> CREBankAccounts {get;set;}
  public OpportunityContactRole decisionMaker {get;set;}
  public list<OpportunityContactRole> ocrList {get;set;}

    public SalesInvoiceExtension(ApexPages.StandardController controller) {
      this.invoice = (c2g__codaInvoice__c)controller.getRecord();

      list<c2g__codaInvoice__c> bankAccount = [select c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__AccountName__c
                          , c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__AccountNumber__c
                          , c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__BankName__c
                          , c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__SortCode__c
                          , c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__SWIFTNumber__c
                          , c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__IBANNumber__c
                          , c2g__OwnerCompany__r.c2g__BankAccount__r.c2g__Email__c
                          from c2g__codaInvoice__c
                          where id =: invoice.id ];

      system.debug('Jupiter bankAccount: ' + bankAccount[0]);
      
      list<c2g__codaBankAccount__c> CEBankAccount = [Select c2g__AccountName__c,c2g__AccountNumber__c,c2g__BankName__c,
                                      c2g__SortCode__c,c2g__SWIFTNumber__c,c2g__IBANNumber__c,c2g__Email__c,c2g__BankAccountCurrency__r.Name
                                      from c2g__codaBankAccount__c Where c2g__OwnerCompany__c =:invoice.c2g__OwnerCompany__c];
                              
system.debug('My New BankAccount: ' + CEBankAccount[0]);

      list<OpportunityContactRole> ocrList = [select id
                                                        , contact.name
                                                        , role 
                                                        from OpportunityContactRole
                                                        where opportunityId = :invoice.c2g__Opportunity__r.Id
                                                        and role = 'Decision Maker'
                                                        limit 1];

      for(OpportunityContactRole ocr : ocrList){
        decisionMaker = ocr;
      }
    }

}