global with sharing class CaseUpdateBatch implements Schedulable, Database.Batchable<SObject> {
	/* Scheduled Job */
	
	private String triggerId;
	
	public CaseUpdateBatch() {}
	
	global void execute(Schedulablecontext sc) {
		Database.executeBatch(new CaseUpdateBatch(sc.getTriggerId()));
	}
	
	/* Batch Job */
	
	public CaseUpdateBatch(String triggerId) {
		this.triggerId = triggerId;
	}
	
	global Iterable<SObject> start(Database.BatchableContext bc) {
		Case[] cases = [SELECT Id FROM Case WHERE Case.Status != :SMCase.STATUS_CLOSED];
		return cases;
	}
	
	global void execute(Database.BatchableContext bc, Case[] scope) {
		for (Case caseObj : scope) {
			caseObj.Last_Updated_By_Apex__c = true;
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext bc) {
		System.abortJob(triggerId);
		try {
			System.abortJob(triggerId);
		}
		catch (Exception e) {}
		System.schedule(UpdateCasesController.CASE_UPDATE_NAME,
						UpdateCasesController.CASE_UPDATE_CRON,
						new CaseUpdateBatch());
	}
}