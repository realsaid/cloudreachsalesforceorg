public with sharing class ServiceContractBuilder {
	private ServiceContract con;
	
	public ServiceContractBuilder() {
		con = getMinimalServiceContract();	
	}
	
	private ServiceContract getMinimalServiceContract() {
		ServiceContract con = new ServiceContract();
		con.Name = 'name';
		con.True_Up_Type__c = 'blah';
		return con;
	} 
	
	public ServiceContractBuilder withStartDate(Date startDate) {
		con.StartDate = startDate;
		return this;
	}
	
	public ServiceContractBuilder withEndDate(Date endDate) {
		con.EndDate = endDate;
		return this;
	}
	
	public ServiceContractBuilder withRenewalType(String renewalType) {
		con.Renewal_Type__c = renewalType;
		return this;
	}
	
	public ServiceContractBuilder withTrueUpType(String trueUpType) {
		con.True_Up_Type__c = trueUpType;
		return this;
	}
	
	public ServiceContract build() {
		return con;
	}
}