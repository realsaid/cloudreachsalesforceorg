public with sharing class SMTask {
    private final String FROM_ADDRESS = 'From';
    private final String TO_ADDRESS = 'To';
    private final String CC_ADDRESS = 'CC';
    private final String BCC_ADDRESS = 'BCC';

    public Task record {get; set;}

    public String fromAddress {get; set;}
    public String toAddress {get; set;}
    public String ccAddress {get; set;}
    public String bccAddress {get; set;}

    public String equalityField {get; set;}

    public SMTask(Task record) {
        this.record = record;
        fromAddress = extractDetail(record.Description, FROM_ADDRESS);
        toAddress = extractDetail(record.Description, TO_ADDRESS);
        ccAddress = extractDetail(record.Description, CC_ADDRESS);
        bccAddress = extractDetail(record.Description, BCC_ADDRESS);

        equalityField = record.Subject +
                record.Description +
                record.WhoID +
                record.Priority +
                record.Status +
                fromAddress +
                toAddress +
                ccAddress +
                bccAddress;
    }

    private String extractDetail(String text, String detail) {
        String value = '';
        String searchText = detail + ': ';
        if (text.contains(searchText)) {
            String restOfText = text.split(searchText)[1];
            if (text.contains('\n')) {
                value = restOftext.split('\n')[0];
            }
            else {
                value = restOftext;
            }
        }
        return value;
    }

    public Boolean equals(Object otherObject) {
        return equalityField == ((SMTask) otherObject).equalityField;
    }

    public Integer hashCode() {
        return equalityField.hashCode();
    }
}