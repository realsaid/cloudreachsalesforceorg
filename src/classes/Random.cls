public with sharing class Random {
    private static final Integer MAX_YEAR_NUMBER = 3000;
    private static final Integer MAX_MONTH_NUMBER = 12;
    private static final Integer MAX_DAY_NUMBER = 28;
    
    private static final Integer MAX_HOUR_NUMBER = 23;
    private static final Integer MAX_MINUTE_NUMBER = 59;
    private static final Integer MAX_SECOND_NUMBER = 59;
    private static final Integer MAX_MILLISECOND_NUMBER = 999;
    
    private static final Integer MULTIPLIER = 10000;
    
    public Random() {}
    
    public Boolean getBoolean() {
        return Math.mod(getInteger(), 2) == 0;
    }
    
    public Integer getInteger() {
        return (Integer) (Math.random() * MULTIPLIER);
    }
    
    public Double getDouble() {
        return (Double) (Math.random() * MULTIPLIER);
    }
    
    public Decimal getDecimal() {
        return Math.random() * MULTIPLIER;
    }
    
    public Decimal getPercent() {
        return getDecimal(100.0);
    }
    
    public Decimal getDecimal(Decimal maxSize) {
        return Math.random() * maxSize;
    }
    
    public String getString() {
        String randomString = '';
        Character[] values = Character.values();
        String newChar = null;
        for (Integer i = 0; i < 10; ++i) {
            newChar = '' + values[Math.mod(getInteger(), 25)];
            randomString += getBoolean() ? newChar.toUpperCase() : newChar.toLowerCase();
        }
        return randomString;
    }
    
    public String getPhone() {
        return getString();
    }
    
    public String getEmail() {
        return getString() + '@' + getString();
    }
    
    public Date getDate() {
        return getDate(Date.today());
    }
    
    public Date getDate(Date minDate) {
        Integer year = getIntegerInRange(minDate.year(), MAX_YEAR_NUMBER);
        Integer month = getIntegerInRange(minDate.month(), MAX_MONTH_NUMBER);
        Integer day = getIntegerInRange(minDate.day(), MAX_DAY_NUMBER);
        return Date.newInstance(year, month, day);
    }
    
    private Integer getIntegerInRange(Integer min, Integer max) {
        Integer modulus = max - min + 1;
        if (max < min) {
            modulus = 1;
        }
        return Math.mod((Integer) (Math.random() * MULTIPLIER), modulus) + min;
    }
    
    public Time getTime() {
        Integer hour = getIntegerInRange(0, MAX_HOUR_NUMBER);
        Integer minute = getIntegerInRange(0, MAX_MINUTE_NUMBER);
        Integer second = getIntegerInRange(0, MAX_SECOND_NUMBER);
        Integer milliSecond = getIntegerInRange(0, MAX_MILLISECOND_NUMBER);
        return Time.newInstance(hour, minute, second, millisecond);
    }
    
    public DateTime getDateTime() {
        return DateTime.newInstance(getDate(), getTime());
    }
    
    public String getString(List<String> strings){
        List<Double> ranks = new List<Double>();
        Map<Double,String> rankMap = new Map<Double,String>();
        
        for(String s : strings){
            Boolean isDup = true;
            Double rank;
            While(isDup){
                Double x = getInteger();
                if(!rankMap.containsKey(x)){
                    rank = x;
                    isDup = false;
                }
            }
            ranks.add(rank);
            rankMap.put(rank,s);
        }
        ranks.sort();
        return rankMap.get(ranks.get(0));
    }
     
    public String getPickListValue(Sobject anSObject, String fieldName, Boolean allowBlank){
        List<String> Strings = new List<String>();
        if(allowBlank){
        String b = '';
        Strings.add(b);
        }
          Schema.sObjectType sObjectType = anSObject.getSObjectType();
          Schema.DescribeSObjectResult sObjectDescribe = sObjectType.getDescribe();
          Map<String, Schema.SObjectField> fieldMap = sObjectDescribe.fields.getMap();
          List<Schema.PicklistEntry> picklistValues = fieldMap.get(fieldName).getDescribe().getPickListValues();
          for (Schema.PicklistEntry a : picklistValues) {
             Strings.add(a.getValue());
          }
          
          return getString(Strings);
    }
    
    private Map<String,List<String>> getPicVals(sObject anSObject){
        Map<String,List<String>> valueMap = new Map<String,List<String>>();
        
        Schema.sObjectType sObjectType = anSObject.getSObjectType();
        Schema.DescribeSObjectResult r = sObjectType.getDescribe();
        Map<String, Schema.SObjectField> fieldMap = r.fields.getMap();
        
        for(String s : fieldMap.keyset()){
            List<String> strings = new List<String>();
            Schema.DescribeFieldResult F = fieldMap.get(s).getDescribe();
            if(f.GetType() == Schema.DisplayType.Picklist || f.GetType() == Schema.DisplayType.MultiPicklist){
                List<Schema.PicklistEntry> picklistValues = fieldMap.get(s).getDescribe().getPickListValues();
                for (Schema.PicklistEntry a : picklistValues) {
                    strings.add(a.getValue());
                }
                valueMap.put(String.valueOf(fieldMap.get(s)),strings);
            }
        }
        return valueMap;
    }
    
    public Opportunity getOpportunity() {
        Opportunity opp = new Opportunity();
        Map<String, Schema.SObjectfield> fieldMap = Opportunity.getSObjectType().getDescribe().fields.getMap();
        Account acc = getAccount();
        insert acc;
        opp.AccountID = acc.ID;
        Campaign cam = getCampaign();
        insert cam;
        opp.CampaignID = cam.ID;
        opp = (Opportunity) fillFields(opp, fieldMap);
        // This is done to avoid a validation rule error when inserting the opportunity for testing
        opp.StageName='Prospecting';
        return opp;
    }
    
    public Account getAccount() {
        Map<String, Schema.SObjectfield> fieldMap = Account.getSObjectType().getDescribe().fields.getMap();
        return (Account) fillFields(new Account(), fieldMap);
    }
    
    public Contact getContact() {
        Map<String, Schema.SObjectfield> fieldMap = Contact.getSObjectType().getDescribe().fields.getMap();
        return (Contact) fillFields(new Contact(), fieldMap);
    }
    
    public Campaign getCampaign() {
        Map<String, Schema.SObjectfield> fieldMap = Campaign.getSObjectType().getDescribe().fields.getMap();
        Campaign cam = new Campaign();
        cam.StartDate = Date.today();
        cam = (Campaign) fillFields(cam, fieldMap);
        cam.EndDate = getDate(cam.StartDate);
        return cam;
    }
    
    public ServiceContract getServiceContract() {
        Map<String, Schema.SObjectfield> fieldMap = ServiceContract.getSObjectType().getDescribe().fields.getMap();
        ServiceContract contract = (ServiceContract) fillFields(new ServiceContract(), fieldMap);
        contract.EndDate = getDate(contract.StartDate);
        return contract;
    }
    
    public OpportunityLineItem getOpportunityLineItem() {
        Map<String, Schema.SObjectfield> fieldMap = OpportunityLineItem.getSObjectType().getDescribe().fields.getMap(); 
        OpportunityLineItem item = (OpportunityLineItem) fillFields(new OpportunityLineItem(), fieldMap);
        // Must only specify one of: UnitPrice or TotalPrice
        item.TotalPrice = null;
        return item;
    }
    
    public ContractLineItem getContractLineItem() {
        Map<String, Schema.SObjectfield> fieldMap = ContractLineItem.getSObjectType().getDescribe().fields.getMap(); 
        ContractLineItem item = (ContractLineItem) fillFields(new ContractLineItem(), fieldMap);
        item.EndDate = getDate(item.StartDate);
        return item;
    }
    
    private SObject fillFields(SObject sObj, Map<String, Schema.SObjectfield> fieldMap) {
        for (String fieldName : fieldMap.keySet()) {
            Schema.DescribeFieldResult fieldDescribe = fieldMap.get(fieldName).getDescribe();
            if ((fieldDescribe.isCreateable() || fieldDescribe.isNameField()) && !fieldIsManaged(fieldName)) {
                Schema.DisplayType fieldType = fieldDescribe.getType(); 
                if (fieldType == Schema.DisplayType.Boolean) {
                    sObj.put(fieldName, getBoolean());
                }
                else if (fieldType == Schema.DisplayType.Integer) {
                    sObj.put(fieldName, getInteger());
                }
                else if (fieldType == Schema.DisplayType.Double) {
                    sObj.put(fieldName, getDouble());
                }
                else if (fieldType == Schema.DisplayType.Currency) {
                    sObj.put(fieldName, getDecimal());
                }
                else if (fieldType == Schema.DisplayType.Percent) {
                    sObj.put(fieldName, getDecimal(100.0));
                }
                else if (fieldType == Schema.DisplayType.String) {
                    String value = getString();
                    try {
                        sObj.put(fieldName, value);
                    }
                    catch (Exception e) {
                        System.Debug('Exception caught writing field "' + fieldName + '"' + ' with value: ' + value);
                    }
                }
                else if (fieldType == Schema.DisplayType.Email) {
                    sObj.put(fieldName, getEmail());
                }
                else if (fieldType == Schema.DisplayType.Date) {
                    sObj.put(fieldName, getDate());
                }
                else if (fieldType == Schema.DisplayType.Time) {
                    sObj.put(fieldName, getTime());
                }
                else if (fieldType == Schema.DisplayType.DateTime) {
                    sObj.put(fieldName, getDateTime());
                }
                else if (fieldType == Schema.DisplayType.Picklist ||
                         fieldType == Schema.DisplayType.MultiPicklist) {
                    sObj.put(fieldName, getPickListValue(sObj, fieldName, false));
                }
            }
        }
        return sObj;
    }
    
    private Boolean fieldIsManaged(String fieldName) {
        Boolean fieldIsManaged = false;
        String[] parts = fieldName.split('__');
        if (parts.size() > 2) {
            fieldIsManaged = true;
        }
        return fieldIsManaged;
    }
}