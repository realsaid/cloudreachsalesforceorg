public with sharing class EngineerEmailCaseTriggerAction extends TriggerAction {
	public static Boolean DID_SEND_EMAIL = false;
	
	private static final String ENGINEER_EMAIL_TEMPLATE = 'Cases: Updated Email Assigned Engineer';
	private static final String SERVICE_DESK_EMAIL_ADDRESS = 'servicedesk@cloudreach.co.uk';
	private static final String[] UPDATE_EMAIL_STATUSES = new String[]{SMCase.STATUS_PENDING_VENDOR,
																	   SMCase.STATUS_PENDING_CLOUDREACH,
																	   SMCase.STATUS_ASSIGNING,
																	   SMCase.STATUS_CLOSED};
	
	private static Boolean hasExecuted = false;
	
	
	private Case[] cases;
	
	public EngineerEmailCaseTriggerAction(Case[] cases) {
		this.cases = cases;
	}
	
	public override void onBeforeInsert() {
		if (shouldEmailEngineer()) {
			emailEngineerAssignedTo(cases[0]);
		}
		hasExecuted = true;
	}
	
	public override void onBeforeUpdate() {
		if (shouldEmailEngineer()) {
			emailEngineerAssignedTo(cases[0]);
		}
		hasExecuted = true;
	}
	
	private Boolean shouldEmailEngineer() {
		return cases != null
			   && cases.size() == 1
			   && new SMString(cases[0].Status).isInList(UPDATE_EMAIL_STATUSES)
			   && cases[0].Assigned_Engineer__c != SMCase.ENGINEER_NOT_ASSIGNED
			   && cases[0].Assigned_Engineer_Email__c != null
			   && !hasExecuted;
	}
	
	private void emailEngineerAssignedTo(Case theCase) {
		EmailTemplate template = getEmailTemplate();
		Contact engineer = getEngineer(theCase.Assigned_Engineer_Email__c);
		if (template != null && engineer != null) {
			Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();
			ID emailAddressID = getServiceDeskEmailID();
			if (emailAddressID != null) {
				msg.setOrgWideEmailAddressID(emailAddressID);
			}
			msg.setTemplateId(template.Id);
			msg.setTargetObjectId(engineer.Id);
			msg.setToAddresses(new String[]{engineer.Email});
			msg.setWhatId(theCase.Id);
			msg.setSaveAsActivity(false);
			Messaging.sendEmail(new Messaging.SingleEmailMessage[]{msg});
			DID_SEND_EMAIL = true;
		}
	}
	
	private EmailTemplate getEmailTemplate() {
		EmailTemplate template = null;
		try {
			template = [SELECT Id
    		 			FROM EmailTemplate
		     			WHERE Name = :ENGINEER_EMAIL_TEMPLATE
	   	     			limit 1];
		}
		catch (Exception e) {
			log('Exception caught in EngineerEmailCaseTrigger.sendEmail: ' + e);
		}
		return template;
	}
	
	private ID getServiceDeskEmailID() {
		ID emailID = null;
		try {
			emailID = [SELECT ID
					   FROM OrgWideEmailAddress
					   WHERE Address = :SERVICE_DESK_EMAIL_ADDRESS
					   limit 1].ID;
		}
		catch (Exception e) {
			System.Debug('Exception caught in EngineerEmailCaseTriggerAction.getServiceDeskEmailID: ' + e.getMessage());
		}
		return emailID;
	}
	
	private Contact getEngineer(String engineerEmail) {
		Contact contact = null;
		try {
			contact = [SELECT Id, Email
					   FROM Contact
					   WHERE Email = :engineerEmail
					   limit 1];
		}
		catch(Exception e) {
			log('Exception caught in EngineerEmailCaseTrigger.getEngineer: ' + e.getMessage());
			contact = new Contact();
			contact.Email = engineerEmail;
			String localPart = engineerEmail.split('@')[0];
			String firstName = localPart;
			String lastName = localPart;
			String[] names = localPart.split('.');
			if (names != null && names.size() == 2) {
				firstName = new SMString(names[0]).capitalise(0);
				lastName = new SMString(names[1]).capitalise(0);
			}
			contact.FirstName = firstName;
			contact.LastName = lastName;
			insert contact;
		}
		return contact;
	}
	
	private void log(String log) {
		System.Debug(log);
	}
}