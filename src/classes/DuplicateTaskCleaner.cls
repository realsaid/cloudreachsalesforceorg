public with sharing class DuplicateTaskCleaner {
	private static final Integer N_TASKS = 1000;
	
	public Integer clean(Task[] tasks) {
		Set<SMTask> uniqueTasks = new Set<SMTask>();
        Task[] tasksToDelete = new Task[]{};
        
        for (Task task : tasks) {
            SMTask smTask = new SMTask(task);
            
            if (!uniqueTasks.contains(smTask)) {
                uniqueTasks.add(smTask);
            }
            else {
                tasksToDelete.add(task);
            }
        }
        
        delete tasksToDelete;
        return tasksToDelete.size();
	}
}