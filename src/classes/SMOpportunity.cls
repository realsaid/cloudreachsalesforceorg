public with sharing class SMOpportunity {
    private static final String ID = 'ID';
    private static final String LAST_MODIFIED_DATE = 'LastModifiedDate';
    private static final String SYSTEM_MOD_STAMP = 'SystemModStamp';
    private static final String CREATED_DATE = 'CreatedDate';
    private static final String AMOUNT = 'Amount';
    private static final String ORDER_NUMBER = 'Order_Number__c';
    private static final String EXPECTED_REVENUE = 'ExpectedRevenue';
    
    private static final String[] MATCH_FIELD_EXCEPTIONS = new String[]{ID,
                                                                        LAST_MODIFIED_DATE,
                                                                        SYSTEM_MOD_STAMP,
                                                                        CREATED_DATE,
                                                                        AMOUNT,
                                                                        ORDER_NUMBER,
                                                                        EXPECTED_REVENUE};
                                                                        
    public static final String PRODUCT_FAMILY_MANAGED_SERVICES = 'Operational Services';
    public static final String STAGE_NAME_PROPOSAL = 'Proposal Delivered';
    public static final String STAGE_NAME_LEGAL_AND_PROCUREMENT = 'Legal & Procurement';
    public static final String STAGE_NAME_CLOSED_WON = 'Closed Won';
    public static final String TYPE_UPSELL = 'Upsell';
    public static final String TYPE_RENEWAL = 'Renewal';
    
    private Opportunity opp;
    
    public SMOpportunity(Opportunity opp) {
        this.opp = opp;
    }
    
    public Opportunity getRecord() {
        return opp;
    }
    
    public OpportunityLineItem[] getLineItems() {
        return getLineItems(' WHERE OpportunityID = ' + stringQuotes(opp.ID)); 
    }
    
    public OpportunityLineItem[] getLineItems(String whereClause) {
        String query = new SelectAll('OpportunityLineItem').getSOQL() + whereClause; 
        return Database.query(query);
    }
    
    public Opportunity insertClone() {
        return new OpportunityCloner().insertClone(opp.ID);
    }
    
    public override String toString() {
        String toString = '';
        Map<String, Schema.SObjectfield> fieldMap = Opportunity.getSObjectType().getDescribe().fields.getMap(); 
        for (String field : fieldMap.keySet()) {
            toString += '' + field + ': ' + opp.get(field) + '\n';
        }
        return toString;
    }
    
    public Boolean hasProductsInFamily(String productFamily) {
        return getLineItems(' WHERE OpportunityID = ' + stringQuotes(opp.ID) +
                            ' AND PricebookEntry.Product2.Family = ' + stringQuotes(productFamily)).size() > 0;
    }
    
    public ServiceContract getPrimaryServiceContract() {
        ServiceContract con = null;
        if (opp.Primary_Service_Contract__c != null) {
            con = Database.query(new SelectAll('ServiceContract').getSOQL() +
                                                           ' WHERE ID = ' + stringQuotes(opp.Primary_Service_Contract__c));
        }
        return con;
    }
    
    public void setStageName(String stageName) {
        opp.StageName = stageName;
    }
    
    private String stringQuotes(String theString) {
        return '\'' + theString + '\''; 
    }
}