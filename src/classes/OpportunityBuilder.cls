public with sharing class OpportunityBuilder {
	private Opportunity opp;
	
	public OpportunityBuilder() {
		opp = TestDataSet.getOpportunity();
	}
	
	public OpportunityBuilder withDescription(String description) {
        opp.Description = description;
        return this;
    }
	
	public OpportunityBuilder withStageName(String stageName) {
		opp.StageName = stageName;
		return this;
	}
	
	public OpportunityBuilder withLineItem(String productFamily) {
		insert opp;
		insert TestDataSet.getOpportunityLineItem(productFamily, opp.ID);
		return this;
	}
	
	public OpportunityBuilder withPrimaryServiceContract(ServiceContract primaryServiceContract) {
		opp.Primary_Service_Contract__c = primaryServiceContract.ID;
		return this;
	}
	
	public OpportunityBuilder withType(String oppType) {
		opp.Type = oppType;
		return this;
	}
	
	public OpportunityBuilder withOrderForm(String orderFormURL) {
		opp.Order_Form__c = orderformURL;
		return this;
	}
	
	public Opportunity build() {
		return opp;
	}
}