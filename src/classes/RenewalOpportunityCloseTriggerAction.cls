public with sharing class RenewalOpportunityCloseTriggerAction extends TriggerAction {
	private static final String[] WIN_STAGE_NAMES = new String[] { SMOpportunity.STAGE_NAME_CLOSED_WON };
	
	private final Opportunity[] oldOpportunities;
	private final Opportunity[] newOpportunities;
	private ServiceContract primaryContract;
	
	public RenewalOpportunityCloseTriggerAction(final Opportunity[] oldOpportunities, final Opportunity[] newOpportunities) {
		this.oldOpportunities = oldOpportunities;
		this.newOpportunities = newOpportunities;
	}
	
	public override void onBeforeUpdate() {
		if (shouldPerformAction()) {
			performAction(newOpportunities[0]);
		}
	}
	
	private Boolean shouldPerformAction() {
		Boolean shouldPerformAction = false;
		if (newOpportunities.size() == 1 &&
			oldOpportunities.size() == 1 &&
			isRenewalOpportunity(newOpportunities[0]) &&
			hasRenewalPrimaryServiceContract(newOpportunities[0]) &&
			isNewlyWon(oldOpportunities[0], newOpportunities[0]) &&
			isManagedServicesOpportunity(newOpportunities[0])) {
			shouldPerformAction = true;
		}
		return shouldPerformAction;
	}
	
	private Boolean isRenewalOpportunity(Opportunity opp) {
		return opp.Type == SMOpportunity.TYPE_RENEWAL;
	}
	
	private Boolean hasRenewalPrimaryServiceContract(Opportunity opp) {
		return opp.Primary_Service_Contract__c != null &&
		       new SMOpportunity(opp).getPrimaryServiceContract().Renewal_Type__c == SMServiceContract.RENEWAL_TYPE_RENEWAL_EXPECTED;
	}
	
	private Boolean isNewlyWon(Opportunity oldOpportunity, Opportunity newOpportunity) {
		return !(new SMString(oldOpportunity.StageName).isInList(WIN_STAGE_NAMES))
			   && (new SMString(newOpportunity.StageName).isInList(WIN_STAGE_NAMES));
	}
	
	private Boolean isManagedServicesOpportunity(Opportunity opportunity) {
		return new SMOpportunity(opportunity).hasProductsInFamily(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES);
	}
	
	private void performAction(Opportunity opp) {
		primaryContract = new SMOpportunity(opp).getPrimaryServiceContract();
		primaryContract.Renewal_Type__c = '';
		primaryContract.StartDate = primaryContract.EndDate;
		primaryContract.EndDate = null;
		update primaryContract;
	}
}