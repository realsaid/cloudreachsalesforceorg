global with sharing class JSWebservices {

    //@RemoteAction
    webservice static string sendInvoiceEmail(Id invoiceId){
        system.debug('About to send invoice by email: ' + invoiceId);

        //get invoice details
        c2g__codaInvoice__c invoice = [select Id
                                        , name
                                        , c2g__Account__r.c2g__CODAInvoiceEmail__c
                                        , Additional_invoice_emails__c
                                        , c2g__InvoiceStatus__c
                                        , c2g__OwnerCompany__r.name
                                        , Invoice_sent__c
                                        , c2g__Account__r.name
                                        from c2g__codaInvoice__c
                                        where id =: invoiceId];

        system.debug('invoice: ' + invoice);

 
        // Reference the attachment page and pass in the account ID
        
        Boolean isComplete = false;
        if(invoice.c2g__InvoiceStatus__c == 'Complete') isComplete = true;
        
        //add email addresses to recipient list
        list<String> toAddresses = new list<string>{};
        if(invoice.additional_invoice_emails__c != null){
            for(string s :invoice.Additional_invoice_emails__c.split(',', 0)){
                toAddresses.add(s);
            }
        }
        if(invoice.c2g__Account__r.c2g__CODAInvoiceEmail__c != null ){
            toAddresses.add(invoice.c2g__Account__r.c2g__CODAInvoiceEmail__c);
        }
      system.debug('toAddresses: ' + toAddresses);
        try{
            if(toAddresses.isempty()){
            
                return 'Email not sent - no email addresses found';
            } else if(invoice.Invoice_sent__c == true){
                return 'Email not sent - invoice has already been sent.';
            
            
            } else if(isComplete) {

                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

                PageReference pdf =  Page.CRSalesInvoice;
                pdf.getParameters().put('id',invoiceId);
                pdf.getParameters().put('complete','true');
                pdf.getParameters().put('p','1');
                
                pdf.setRedirect(true);
         
                // Take the PDF content
                Blob b = pdf.getContent();
         
                // Create the email attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(invoice.c2g__Account__r.name + ' Invoice: ' + invoice.Name + '.pdf');
                efa.setBody(b);
                
    
                
                // Sets the paramaters of the email
                email.setSubject( invoice.c2g__OwnerCompany__r.name + ' Invoice: ' + invoice.Name );
                email.setToAddresses( toAddresses );
                email.setHTMLBody( 'Hello, <br/><br/>Please find attached invoice number ' + invoice.name + '<br/>Thank you for choosing Cloudreach.<br/><br/>Thanks & Regards,<br/>Accounts Department.' );
         
                email.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
         
                // Sends the email
                Messaging.SendEmailResult [] r =
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email}); 
                
                invoice.invoice_sent__c = true;
                update invoice;
                
                return 'Email sent successfully';
            }else{
                    return 'You Cannot Email In Progress Invoices. Please Post the Invoice and try again';
                    }

        } catch (exception e){            
            
            return e.getMessage();
        }
    }

    



}