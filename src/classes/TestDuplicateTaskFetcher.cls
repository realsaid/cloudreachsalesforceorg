public with sharing class TestDuplicateTaskFetcher {
	public static final String TEST_TAG = 'Auto-Generated Test ';
	
	static testMethod void shouldReturn1TaskWithinDateRange() {
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(1);
        
        Task[] tasks = new DuplicateTaskFetcher().fetch(1, Date.today().addDays(-1), Date.today().addDays(1));
        
        System.assertEquals(1, tasks.size());
    }
    
    static testMethod void shouldReturn1TaskWithinDateRangeGiven2TasksExist() {
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(2);
        
        Task[] tasks = new DuplicateTaskFetcher().fetch(1, Date.today().addDays(-1), Date.today().addDays(1));
        
        System.assertEquals(1, tasks.size());
    }
    
    static testMethod void shouldReturn0TasksWithinDateRange() {
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(1);
        
        Task[] tasks = new DuplicateTaskFetcher().fetch(1, Date.today().addDays(-1), Date.today());
        
        System.assertEquals(0, tasks.size());
    }
}