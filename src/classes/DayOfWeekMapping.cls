public with sharing class DayOfWeekMapping {
	public static DayOfWeek get(Integer ordinal) {
		DayOfWeek day;
		if (ordinal == 0) {
			day = DayOfWeek.SUNDAY;
		}
		else if (ordinal == 1) {
			day = DayOfWeek.MONDAY;
		}
		else if (ordinal == 2) {
			day = DayOfWeek.TUESDAY;
		}
		else if (ordinal == 3) {
			day = DayOfWeek.WEDNESDAY;
		}
		else if (ordinal == 4) {
			day = DayOfWeek.THURSDAY;
		}
		else if (ordinal == 5) {
			day = DayOfWeek.FRIDAY;
		}
		else if (ordinal == 6) {
			day = DayOfWeek.SATURDAY;
		}
		return day;
	}
}