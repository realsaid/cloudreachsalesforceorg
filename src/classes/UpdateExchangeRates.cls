/*

IMPORTANT: Make sure that whoever is the running user of this class has a User Company record set up for every company for which ex



*/



global class UpdateExchangeRates {
	public List<c2g__codaExchangeRate__c> xrList;
	public string companyId {get;set;}


	public UpdateExchangeRates(string companyId) {
		
		this.companyId = companyId;

		//get company first, ensure that 
		list<c2g__codaCompany__c> companyList = [select id
													, Name
													, ownerId
													, (select id
														, name
														, c2g__Home__c
														, c2g__OwnerCompany__c
														from c2g__AccountingCurrencies__r)
													from c2g__codaCompany__c
													where id = :companyid];

		Http h = new Http();

		// REST query generated using Yahoo's YQl generator: 
		// https://developer.yahoo.com/yql/console/?q=show%20tables&env=store://datatables.org/alltableswithkeys#h=select+*+from+yahoo.finance.xchange+where+pair+in+(%22USDGBP%22%2C+%0A%22EURGBP%22%2C+%0A%22CHFGBP%22%2C+%0A%22CADGBP%22%2C+%0A%22GBPUSD%22%2C+%0A%22EURUSD%22%2C+%0A%22CHFUSD%22%2C+%0A%22CADUSD%22%2C+%0A%22GBPEUR%22%2C+%0A%22USDEUR%22%2C+%0A%22CHFEUR%22%2C+%0A%22CADEUR%22%2C+%0A%22GBPCHF%22%2C+%0A%22USDCHF%22%2C+%0A%22EURCHF%22%2C+%0A%22CADCHF%22%2C+%0A%22GBPCAD%22%2C+%0A%22USDCAD%22%2C+%0A%22EURCAD%22%2C+%0A%22CHFCAD%22)

		HttpRequest req = new HttpRequest();
		req.setEndPoint('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDGBP%22%2C%20%0A%22EURGBP%22%2C%20%0A%22CHFGBP%22%2C%20%0A%22CADGBP%22%2C%20%0A%22GBPUSD%22%2C%20%0A%22EURUSD%22%2C%20%0A%22CHFUSD%22%2C%20%0A%22CADUSD%22%2C%20%0A%22GBPEUR%22%2C%20%0A%22USDEUR%22%2C%20%0A%22CHFEUR%22%2C%20%0A%22CADEUR%22%2C%20%0A%22GBPCHF%22%2C%20%0A%22USDCHF%22%2C%20%0A%22EURCHF%22%2C%20%0A%22CADCHF%22%2C%20%0A%22GBPCAD%22%2C%20%0A%22USDCAD%22%2C%20%0A%22EURCAD%22%2C%20%0A%22CHFCAD%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=');
		req.setMethod('GET');

		String jsonText;
		if(!test.isrunningtest()){
			HttpResponse resp = h.send(req);
			jsonText = resp.getBody();
		} else {
			//hardcode the response if running a unit test. Blurg.
			jsonText = '{"query": {"count": 20,"created": "2015-01-31T13:28:16Z","lang": "en-US", "results": {"rate": [{ "id": "USDGBP", "Name": "USD/GBP", "Rate": "0.7650", "Date": "1/31/2016", "Time": "2:09pm", "Ask": "0.7650", "Bid": "0.7650"},{ "id": "EURGBP", "Name": "EUR/GBP", "Rate": "0.8597", "Date": "1/31/2016", "Time": "2:22pm", "Ask": "0.8597", "Bid": "0.8597"},{ "id": "CHFGBP", "Name": "CHF/GBP", "Rate": "0.7903", "Date": "1/31/2016", "Time": "2:04pm", "Ask": "0.7913", "Bid": "0.7903"},{ "id": "CADGBP", "Name": "CAD/GBP", "Rate": "0.5852", "Date": "1/31/2016", "Time": "4:50am", "Ask": "0.5862", "Bid": "0.5852"},{ "id": "GBPUSD", "Name": "GBP/USD", "Rate": "1.3035", "Date": "1/31/2016", "Time": "11:25pm", "Ask": "1.3037", "Bid": "1.3035"},{ "id": "EURUSD", "Name": "EUR/USD", "Rate": "1.1148", "Date": "1/31/2016", "Time": "3:43pm", "Ask": "1.1148", "Bid": "1.1148"},{ "id": "CHFUSD", "Name": "CHF/USD", "Rate": "1.0304", "Date": "1/31/2016", "Time": "10:30am", "Ask": "1.0314", "Bid": "1.0304"},{ "id": "CADUSD", "Name": "CAD/USD", "Rate": "0.7577", "Date": "1/31/2016", "Time": "2:34pm", "Ask": "0.7581", "Bid": "0.7577"},{ "id": "GBPEUR", "Name": "GBP/EUR", "Rate": "1.1655", "Date": "1/31/2016", "Time": "4:03am", "Ask": "1.1659", "Bid": "1.1655"},{ "id": "USDEUR", "Name": "USD/EUR", "Rate": "0.8933", "Date": "1/31/2016", "Time": "10:04pm", "Ask": "0.8938", "Bid": "0.8933"},{ "id": "CHFEUR", "Name": "CHF/EUR", "Rate": "0.9174", "Date": "1/31/2016", "Time": "8:32pm", "Ask": "0.9184", "Bid": "0.9174"},{ "id": "CADEUR", "Name": "CAD/EUR", "Rate": "0.6831", "Date": "1/31/2016", "Time": "3:35am", "Ask": "0.6836", "Bid": "0.6831"},{ "id": "GBPCHF", "Name": "GBP/CHF", "Rate": "1.2671", "Date": "1/31/2016", "Time": "6:08pm", "Ask": "1.2673", "Bid": "1.2671"},{ "id": "USDCHF", "Name": "USD/CHF", "Rate": "0.9732", "Date": "1/31/2016", "Time": "0:39am", "Ask": "0.9733", "Bid": "0.9732"},{ "id": "EURCHF", "Name": "EUR/CHF", "Rate": "1.0906", "Date": "1/31/2016", "Time": "6:50pm", "Ask": "1.0908", "Bid": "1.0906"},{ "id": "CADCHF", "Name": "CAD/CHF", "Rate": "0.7447", "Date": "1/31/2016", "Time": "1:18am", "Ask": "0.7449", "Bid": "0.7447"},{ "id": "GBPCAD", "Name": "GBP/CAD", "Rate": "1.7064", "Date": "1/31/2016", "Time": "11:10pm", "Ask": "1.7069", "Bid": "1.7064"},{ "id": "USDCAD", "Name": "USD/CAD", "Rate": "1.3193", "Date": "1/31/2016", "Time": "2:34pm", "Ask": "1.3194", "Bid": "1.3193"},{ "id": "EURCAD", "Name": "EUR/CAD", "Rate": "1.4677", "Date": "1/31/2016", "Time": "8:39pm", "Ask": "1.4679", "Bid": "1.4677"},{ "id": "CHFCAD", "Name": "CHF/CAD", "Rate": "1.3443", "Date": "1/31/2016", "Time": "9:00pm", "Ask": "1.3463","Bid": "1.3443"}]}}}';		
		}
		 
		jsonText = jsonText.substringAfter('rate\":');

		integer jsonLength = jsonText.length();
		jsonText = jsonText.substring(0, (jsonLength - 2));

		jsonText = jsonText.replace('Date', 'XRDate');
		jsonText = jsonText.replace('Time', 'XRTime');


		//sObject RetunedResult =  updateExchangeRateJSONParser.updateExchangeRateJSONParser(jsonText);

		list<Rate> rates = (list<Rate>) JSON.deserialize(jsonText, list<Rate>.class);


		//put rates into a map
		map<string, Rate> ratesMap = new map<string,Rate>{};
		for(Rate r : rates){
			ratesMap.put(r.id, r);
		}

		system.debug(ratesMap);

		//if this is the first time this has run today, update SF Standard dated exchange rates


		//create list of company sObject records
		list<Company> companyObjectList = new list<Company>{};


		//cycle through every ownerCompany to set home currency and collect accounting currencies list for that company
		for(c2g__codaCompany__c c : companyList){
			string homeCurrency;
			//list of currencies not equal to home currency

			list<c2g__codaAccountingCurrency__c> relevantCurrencies = new list<c2g__codaAccountingCurrency__c>{};
			for(c2g__codaAccountingCurrency__c ac : c.c2g__AccountingCurrencies__r){
				//if the currency is the home currency, set this value
				system.debug('Accounting Currency' + ac);
				system.debug('is home currency?' + ac.c2g__Home__c);
				if(ac.c2g__Home__c == true){
					homeCurrency = ac.Name;
				//if it's not the home currency, we want to create an exhange rate for it, so send it to the company subclass
				} else{
					relevantCurrencies.add(ac);
					system.debug('adding to relevantCurrencies list' + ac);
				}
			}

			//create company sObject
			companyObjectList.add(new Company(c.id, c.name, homeCurrency, relevantCurrencies, c.ownerId));

		}

		//get current user Id
		id userId = userInfo.getUserId();

		//cycle through company sObject list, create a new exchange rate record for accounting currency record
		List<c2g__codaExchangeRate__c> xrList;

		for(Company c : companyObjectList){
		
		xrList = new list<c2g__codaExchangeRate__c>{};

			for(c2g__codaAccountingCurrency__c curr : c.currencylist){
					
					string currencyCombo = c.homeCurrency + curr.name;
					system.debug('currencyCombo: ' + currencyCombo);
					//get relevant rate from rate map
					Rate thisRate = ratesMap.get(currencyCombo);
					//decimal thisRate = ratesMap.get(currencyCombo).Rate;

					string thisDate = parseDate(thisRate.xrDate);

					c2g__codaExchangeRate__c xr = new c2g__codaExchangeRate__c(
								c2g__OwnerCompany__c = c.CompanyId
								, c2g__StartDate__c = date.valueof(thisDate)
								, c2g__ExchangeRateCurrency__c = curr.id
								, c2g__Rate__c = decimal.valueof(thisRate.Rate));

					system.debug('Exchange Rate record ' + currencyCombo + ': ' + xr);

					//insert xr;
					xrList.add(xr);

			}

		}
		if(!test.isrunningtest()){
			insert xrList;
		}

	}

	public class Results {
		public List<Rate> rate {get;set;} 

	}
	
	public class Rate {
		public String id {get;set;} 
		public String Name {get;set;} 
		public String Rate {get;set;} 
		public String XRDate {get;set;} 
		public String XRTime {get;set;} 
		public String Ask {get;set;} 
		public String Bid {get;set;} 

	}

	public class Company {
		public Id CompanyId {get;set;}
		public string CompanyName {get;set;}
		public string homeCurrency {get;set;}
		public list<c2g__codaAccountingCurrency__c> currencylist {get;set;}
		public id ownerId {get;set;}

		public  company(Id i, string cName, string hCurr, list<c2g__codaAccountingCurrency__c> curList, id o){
			companyId = i;
			companyName = cName;
			homeCurrency = hCurr;
			currencyList = curList;
			ownerId = o;

		}
	}

	public string parseDate(string d){

		string fullDate = d;

		string monthPart = fullDate.substringBefore('/');
		string dayPart = fullDate.substringBetween('/');
		string yearPart= fullDate.substringAfterLast('/');

		string newDate = yearPart + '-' + monthPart + '-' + dayPart;

		system.debug('newDate: ' + newDate);

		return newDate;

	}
}