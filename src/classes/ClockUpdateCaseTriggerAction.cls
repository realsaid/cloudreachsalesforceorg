public without sharing class ClockUpdateCaseTriggerAction extends TriggerAction {
	private final Case[] cases;
	private final Map<Id, Case> caseMap;
	private final Boolean isTest;
	
	public static Integer nBeforeUpdates = 0;
	
	private static final String[] STOP_STATUSES = new String[]{SMCase.STATUS_PENDING_VENDOR,
														  	   SMCase.STATUS_PENDING_CUSTOMER,
														  	   SMCase.STATUS_SOLUTION_OFFERED,
														  	   SMCase.STATUS_CLOSED};
	public ClockUpdateCaseTriggerAction(final Case[] cases) {
		this.cases = cases;
		isTest = Test.isRunningTest();
		if (!isTest) {
			caseMap = mapCasesToCasesWithBusinessHours();
		}
	}
	
	private Map<Id, Case> mapCasesToCasesWithBusinessHours() {
		Map<Id, Case> caseMap = new Map<Id, Case>([SELECT     IsStopped,
															  Status,
															  Clock__c,
															  Clock_Start_Date__c,
															  BusinessHours.SundayStartTime,
															  BusinessHours.SundayEndTime,
															  BusinessHours.MondayStartTime,
															  BusinessHours.MondayEndTime,
															  BusinessHours.TuesdayStartTime,
															  BusinessHours.TuesdayEndTime,
															  BusinessHours.WednesdayStartTime,
															  BusinessHours.WednesdayEndTime,
															  BusinessHours.ThursdayStartTime,
															  BusinessHours.ThursdayEndTime,
															  BusinessHours.FridayStartTime,
															  BusinessHours.FridayEndTime,
															  BusinessHours.SaturdayStartTime,
															  BusinessHours.SaturdayEndTime
													  FROM Case
													  WHERE Id in :cases]);
		return caseMap;
	}
	
	public override void onBeforeUpdate() {
		if (shouldPerformAction()) {
			performAction();
		}
		++nBeforeUpdates;
	}

	private Boolean shouldPerformAction() {
		return !(Test.isRunningTest() && Trigger.isExecuting)
			   && (nBeforeUpdates < 1)
			   && (cases.size() == 1);

	}
	
	private void performAction() {
		for (Case caseObj : cases) {
			SMCase smCase = new SMCase(caseObj);
			if (isTest) {
				performTestAction(smCase);
			}
			else {
				performAction(smCase);
			}
		}
	}
	
	private void performTestAction(SMCase smCase) {
		SMCase updatedCase = smCase.clone();
		copyCaseDetails(smCase, updatedCase);
		startOrStopClock(updatedCase);
		copyCaseDetails(updatedCase, smCase);
	}
	
	private void performAction(SMCase smCase) {
		SMCase updatedCase = new SMCase(caseMap.get(smCase.getId()));
		copyCaseDetails(smCase, updatedCase);
		startOrStopClock(updatedCase);
		copyCaseDetails(updatedCase, smCase);
	}
	
	private void copyCaseDetails(SMCase copiedFrom, SMCase copiedTo) {
		copiedTo.setStatus(copiedFrom.getStatus());
		copiedTo.setClock(copiedFrom.getClock());
		copiedTo.setClockStartDate(copiedFrom.getClockStartDate());
	}
	
	private void startOrStopClock(SMCase smCase) {
		System.Debug('smCase: ' + 'status: ' + smCase.getStatus());
		if (shouldStopCase(smCase)) {
			System.Debug('should stop');
			smCase.stopClock();
		}
		else {
			System.Debug('starting clock');
			smCase.startClock();
		}
	}
	
	private Boolean shouldStopCase(SMCase smCase) {
		Boolean shouldStopCase = false;
		if (isStopStatus(smCase) || !smCase.isEntitled()) {
			shouldStopCase = true;
		}
		return shouldStopCase;
	}
	
	private Boolean isStopStatus(SMCase smCase) {
		return new SMString(smCase.getStatus()).isInList(STOP_STATUSES);
	}
}