@isTest
public with sharing class TestCRON {
	static testMethod void shouldReturnCorrectCRONExpression() {
		DateTime dt = DateTime.newInstance(Date.newInstance(2000, 1, 1), Time.newInstance(0, 0, 0, 0));
		
		String cron = new CRON(dt).getCRON();
		
		System.assertEquals('0 0 0 1 1 ? 2000', cron);
	}
}