global class GoogleInternalRegHandler implements Auth.RegistrationHandler{
    global User createUser(Id portalId, Auth.UserData data){
        // use the following debug statements if Oauth fails, to see the returned data from google apis
        // system.debug('in createUser, data object is: ' + data);
        // system.debug('user email is: ' + data.email);
        User u = [SELECT Id FROM user WHERE email =: data.email and IsActive = true];
        //system.debug('GUY found user ' + u.Id);
        //set the profile pic
        setImage(u.Id,data.attributeMap.get('picture'));
        return u;
    }
    
    global void updateUser(Id userId, Id portalId, Auth.UserData data){
        //system.debug('GUY in updateUser, data object is: ' + data);
        setImage(userId,data.attributeMap.get('picture'));
    }
    
    public void setImage(Id userId, String url) {
        
        try {
            //check user's existing photo
            ConnectApi.Photo p = ConnectApi.ChatterUsers.getPhoto(null, userId);
            
            // if they don't have a photo and we get a link to one
            if((p == null || p.photoVersionId == null) &&  url != null ) {
            
                //retrieve the photo from google
                HttpRequest req = new HttpRequest();
                req.setEndpoint(url);
                req.setMethod('GET');
                req.setHeader('Authorization', 'Bearer ' + Auth.AuthToken.getAccessToken('0SOb0000000PAsw', 'open id connect'));
                HttpResponse res = new Http().send(req);
                Blob b = res.getBodyAsBlob();
                
                //set the photo on the user's profile
                ConnectApi.ChatterUsers.setPhoto(null, userId, new ConnectApi.BinaryInput(b,'image/jpg','user.jpg'));
            }
        }
        catch(Exception e){
            //don't block the user login by throwing an exception
        }
    }
}