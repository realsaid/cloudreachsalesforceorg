public with sharing class Logger {

	public static void log(String component, String msg) {
		new Log().withComponent(component).withMessage(msg).save();
	}

	public static void logException(String component, String msg, Exception e) {
		new Log().withComponent(component)
			.withMessage(msg + ': ' + e.getMessage())
			.withStackTrace(e.getStackTraceString())
			.save();
	}
}