public with sharing class CRON {
	private final DateTime dt;
	
	public CRON(DateTime dt) {
		this.dt = dt;
	}
	
	public String getCRON() {
		String result = '' + dt.second()  + ' '    + dt.minute()   + ' '
						   + dt.hour()    + ' '    + dt.day()      + ' '
						   + dt.month()   + ' ? '  + dt.year();
        return result;
	}
}