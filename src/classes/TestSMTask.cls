@isTest
public with sharing class TestSMTask {
	private static Contact insertContact(String email) {
		Contact con = new Contact();
		con.FirstName = 'Bring';
		con.LastName = 'Your Laptop!';
        con.Email = email;
        insert con;
        return con;
	}
	
	static testMethod void shouldBeEqualToOtherTask() {
		Contact who = insertContact('robin.smith@cloudreach.co.uk');		
		Task task1 = getAluminaEmailTask(who.Id);
		Task task2 = getAluminaEmailTask(who.Id);
		SMTask smTask1 = new SMTask(task1);
		SMTask smTask2 = new SMTask(task2);
		
		Boolean areEqual = smTask1.equals(smTask2);
		
		System.assertEquals(true, areEqual);
		System.assertEquals(smTask1.hashCode(), smTask2.hashCode());
	}
	
	static testMethod void shouldNotBeEqualToOtherTaskGivenSubjectIsDifferent() {
		Contact who = insertContact('robin.smith@cloudreach.co.uk');      
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        task2.Subject += 'a';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
	
	static testMethod void shouldNotBeEqualToOtherTaskGivenDescriptionIsDifferent() {
		Contact who = insertContact('robin.smith@cloudreach.co.uk');      
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        task2.Description += 'a';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenWhoIdIsDifferent() {
    	Contact robin = insertContact('robin.smith@cloudreach.co.uk');
    	Contact juan = insertContact('juan.canham@cloudreach.co.uk');
    	      
        Task task1 = getAluminaEmailTask(robin.Id);
        Task task2 = getAluminaEmailTask(juan.Id);
    	
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenPriorityIsDifferent() {
    	Contact who = insertContact('robin.smith@cloudreach.co.uk');
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        task2.Priority += 'a';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenStatusIsDifferent() {
    	Contact who = insertContact('robin.smith@cloudreach.co.uk');
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        task2.Status += 'a';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenFromAddressIsDifferent() {
    	Contact who = insertContact('robin.smith@cloudreach.co.uk');
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        Task2.Description = 'From: Robinator\nTo: Hello World!\nCC: Barry m8\nBCC: Sure-Shot Sean\n\nHello this is a synced Alumina Email.';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenToAddressIsDifferent() {
    	Contact who = insertContact('robin.smith@cloudreach.co.uk');
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        Task2.Description = 'From: Robin Smith\nTo: Robinator\nCC: Barry m8\nBCC: Sure-Shot Sean\n\nHello this is a synced Alumina Email.';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenCCAddressIsDifferent() {
    	Contact who = insertContact('robin.smith@cloudreach.co.uk');
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        Task2.Description = 'From: Robin Smith\nTo: Hello World!\nCC: Robinator\nBCC: Sure-Shot Sean\n\nHello this is a synced Alumina Email.';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }
    
    static testMethod void shouldNotBeEqualToOtherTaskGivenBCCAddressIsDifferent() {
    	Contact who = insertContact('robin.smith@cloudreach.co.uk');
        Task task1 = getAluminaEmailTask(who.Id);
        Task task2 = getAluminaEmailTask(who.Id);
        Task2.Description = 'From: Robin Smith\nTo: Hello World!\nCC: Barry m8\nBCC: Robinator\n\nHello this is a synced Alumina Email.';
        SMTask smTask1 = new SMTask(task1);
        SMTask smTask2 = new SMTask(task2);
        
        Boolean areEqual = smTask1.equals(smTask2);
        
        System.assertEquals(false, areEqual);
        System.assertNotEquals(smTask1.hashCode(), smTask2.hashCode());
    }

    public static Task getAluminaEmailTask(String whoID) {
        String subject = 'Synced Alumina Email';
        String description = 'From: Robin Smith\nTo: Hello World!\nCC: Barry m8\nBCC: Sure-Shot Sean\n\nHello this is a synced Alumina Email.';
        
        Task task = new Task();
        task.Subject = subject;
        task.Description = description;
        task.WhoId = whoId;
        task.Type = 'Email';
        //task.Alumina__Is_Alumina_Email__c = true;
        task.Priority = 'Normal';
        task.Status = 'Urgent!';
        
        return task;
    }
}