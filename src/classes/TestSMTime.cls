@isTest
public with sharing class TestSMTime {
	private static final Integer DECIMAL_PLACES = SMTime.DECIMAL_PLACES;
	private static final TimeConverter converter = new TimeConverter(DECIMAL_PLACES);
	
	private static Decimal scale(Decimal dec) {
		return dec.setScale(converter.getDecimalPlaces());
	}
	
	static testMethod void shouldConvertToCorrectSFTimeObject() {
		Time sfTime = DateTime.now().time();
		SMTime smTime = new SMTIme(sfTime);
		
		Time convertedSFTime = smTime.toSFObject();
		
		System.assertEquals(sfTime, convertedSFTime);
	}
	
	static testMethod void shouldReturnTimeInDecimalHours() {
		Integer hours = 1;
		Integer minutes = 2;
		Integer seconds = 3;
		Integer milliseconds = 4;
		Time theTime = Time.newInstance(hours, minutes, seconds, milliseconds);
		SMTime smTime = new SMTime(theTime);
		
		Decimal timeInHours = smTime.getHours();
		
		Decimal expectedHours = hours
							  + converter.minutesToHours(minutes)
							  + converter.secondsToHours(seconds)
							  + converter.millisecondsToHours(milliseconds);
		System.assertEquals(scale(expectedHours), scale(timeInHours));
	}
	
	static testMethod void shouldAddSecondsToTime() {
		SMTime originalTime = new SMTime(Time.newInstance(1, 2, 3, 4));
		Integer addedTime = 10;
		
		SMTime increasedTime = originalTime.addSeconds(addedTime);
		
		Decimal expectedNewTimeInHours = originalTime.getHours()
									   + converter.secondsToHours(addedTime);
		System.assertEquals(scale(expectedNewTimeInHours), increasedTime.getHours());
	}
	
	static testMethod void shouldAddHoursToTime() {
		SMTime originalTime = new SMTime(Time.newInstance(1, 2, 3, 4));
		Integer addedTime = 10;
		
		SMTime increasedTime = originalTime.addHours(addedTime);
		
		Decimal expectedNewTimeInHours = originalTime.getHours()
									   + addedTime;
		System.assertEquals(scale(expectedNewTimeInHours), increasedTime.getHours());
	}
	
	static testMethod void shouldReturnDecimalHoursBetweenTimes() {
		Integer hours = 1;
		Integer minutes = 2;
		Integer seconds = 3;
		Integer milliseconds = 4;
		Time time1 = Time.newInstance(0, 0, 0, 0);
		SMTime smTime1 = new SMTime(time1);
		Time time2 = Time.newInstance(hours, minutes, seconds, milliseconds);
		SMTime smTime2 = new SMTime(time2);
		Decimal expectedHoursBetween = scale(smTime2.getHours() - smTime1.getHours());
		
		Decimal hoursBetween = smTime1.hoursBetween(smTime2);
		
		System.assertEquals(scale(expectedHoursBetween), scale(hoursBetween));
	}
	
	static testMethod void shouldBeEqualToOtherTimeToNearestMillisecond() {
		SMTime time1 = new SMTime(Time.newInstance(1, 2, 3, 4));
		SMTime time2 = new SMTime(Time.newInstance(1, 2, 3, 4));
		
		Boolean isEqual = time1.equals(time2);
		
		System.assertEquals(true, isEqual);
	}
	
	static testMethod void shouldNotBeEqualToOtherTimeThatIsOneMillisecondDifferent() {
		SMTime time1 = new SMTime(Time.newInstance(1, 2, 3, 4));
		SMTime time2 = new SMTime(Time.newInstance(1, 2, 3, 5));
		
		Boolean isEqual = time1.equals(time2);
		
		System.assertEquals(false, isEqual);
	}
}