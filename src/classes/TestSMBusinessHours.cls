@isTest
public with sharing class TestSMBusinessHours {
	private static final SMTime SUNDAY_START_TIME = new SMTime(Time.newInstance(9, 0, 0, 0));
	private static final SMTime SUNDAY_END_TIME = new SMTime(Time.newInstance(17, 0, 0, 0));
	private static final TimePeriod SUNDAY_HOURS = new TimePeriod(SUNDAY_START_TIME, SUNDAY_END_TIME);
	private static final SMTime MONDAY_START_TIME = new SMTime(Time.newInstance(10, 0, 0, 0));
	private static final SMTime MONDAY_END_TIME = new SMTime(Time.newInstance(18, 0, 0, 0));
	private static final TimePeriod MONDAY_HOURS = new TimePeriod(MONDAY_START_TIME, MONDAY_END_TIME);
	private static final SMTime TUESDAY_START_TIME = new SMTime(Time.newInstance(11, 0, 0, 0));
	private static final SMTime TUESDAY_END_TIME = new SMTime(Time.newInstance(19, 0, 0, 0));
	private static final TimePeriod TUESDAY_HOURS = new TimePeriod(TUESDAY_START_TIME, TUESDAY_END_TIME);
	private static final SMTime WEDNESDAY_START_TIME = new SMTime(Time.newInstance(12, 0, 0, 0));
	private static final SMTime WEDNESDAY_END_TIME = new SMTime(Time.newInstance(20, 0, 0, 0));
	private static final TimePeriod WEDNESDAY_HOURS = new TimePeriod(WEDNESDAY_START_TIME, WEDNESDAY_END_TIME);
	private static final SMTime THURSDAY_START_TIME = new SMTime(Time.newInstance(13, 0, 0, 0));
	private static final SMTime THURSDAY_END_TIME = new SMTime(Time.newInstance(21, 0, 0, 0));
	private static final TimePeriod THURSDAY_HOURS = new TimePeriod(THURSDAY_START_TIME, THURSDAY_END_TIME);
	private static final SMTime FRIDAY_START_TIME = new SMTime(Time.newInstance(14, 0, 0, 0));
	private static final SMTime FRIDAY_END_TIME = new SMTime(Time.newInstance(22, 0, 0, 0));
	private static final TimePeriod FRIDAY_HOURS = new TimePeriod(FRIDAY_START_TIME, FRIDAY_END_TIME);
	private static final SMTime SATURDAY_START_TIME = new SMTime(Time.newInstance(15, 0, 0, 0));
	private static final SMTime SATURDAY_END_TIME = new SMTime(Time.newInstance(23, 0, 0, 0));
	private static final TimePeriod SATURDAY_HOURS = new TimePeriod(SATURDAY_START_TIME, SATURDAY_END_TIME);
	
	private static final SMTime TODAY_START_TIME = new SMTime(Time.newInstance(9, 0, 0, 0));
	private static final SMTime TODAY_END_TIME = new SMTime(Time.newInstance(17, 0, 0, 0));
	private static final TimePeriod TODAY_HOURS = new TimePeriod(TODAY_START_TIME, TODAY_END_TIME);
	
	private static final Integer DECIMAL_PLACES = 4;
	
	static testMethod void shouldReturnBusinessHoursForSunday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.SundayStartTime = SUNDAY_START_TIME.toSFObject();
		businessHours.SundayEndTime = SUNDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.SUNDAY);
		
		System.assertEquals(true, SUNDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, SUNDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForMonday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.MondayStartTime = MONDAY_START_TIME.toSFObject();
		businessHours.MondayEndTime = MONDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.MONDAY);
		
		System.assertEquals(true, MONDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, MONDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForTuesday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.TuesdayStartTime = TUESDAY_START_TIME.toSFObject();
		businessHours.TuesdayEndTime = TUESDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.TUESDAY);
		
		System.assertEquals(true, TUESDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, TUESDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForWednesday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.WednesdayStartTime = WEDNESDAY_START_TIME.toSFObject();
		businessHours.WednesdayEndTime = WEDNESDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.WEDNESDAY);
		
		System.assertEquals(true, WEDNESDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, WEDNESDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForThursday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.ThursdayStartTime = THURSDAY_START_TIME.toSFObject();
		businessHours.ThursdayEndTime = THURSDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.THURSDAY);
		
		System.assertEquals(true, THURSDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, THURSDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForFriday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.FridayStartTime = FRIDAY_START_TIME.toSFObject();
		businessHours.FridayEndTime = FRIDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.FRIDAY);
		
		System.assertEquals(true, FRIDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, FRIDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForSaturday() {
		BusinessHours businessHours = new BusinessHours();
		businessHours.SaturdayStartTime = SATURDAY_START_TIME.toSFObject();
		businessHours.SaturdayEndTime = SATURDAY_END_TIME.toSFObject();
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(DayOfWeek.SATURDAY);
		
		System.assertEquals(true, SATURDAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, SATURDAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnBusinessHoursForToday() {
		BusinessHours businessHours = TestDataSet.getBusinessHours(TODAY_START_TIME, TODAY_END_TIME);
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		
		TimePeriod dayHours = hours.getHoursForDay(new SMDate(Date.today()).getDayOfWeek());
		
		System.assertEquals(true, TODAY_START_TIME.equals(dayHours.getStart()));
		System.assertEquals(true, TODAY_END_TIME.equals(dayHours.getEnd()));
	}
	
	static testMethod void shouldReturnTotalBusinessHoursWhenHoursContainRange() {
		BusinessHours businessHours = TestDataSet.getBusinessHours(TODAY_START_TIME, TODAY_END_TIME);
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		DateTimePeriod period = getTodayPeriod(+2, -2);
		
		Decimal totalHours = hours.getTotalBusinessHoursForPeriod(period);
		
		System.assertEquals(4, totalHours);
	}
	
	static testMethod void shouldReturnTotalBusinessHoursWhenRangeContainsHours() {
		BusinessHours businessHours = TestDataSet.getBusinessHours(TODAY_START_TIME, TODAY_END_TIME);
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		DateTimePeriod period = getTodayPeriod(-2, +2);
		
		Decimal totalHours = hours.getTotalBusinessHoursForPeriod(period);
		
		System.assertEquals(8, totalHours);
	}
	
	static testMethod void shouldReturnTotalBusinessHoursWhenStartOccursBeforeHours() {
		BusinessHours businessHours = TestDataSet.getBusinessHours(TODAY_START_TIME, TODAY_END_TIME);
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		DateTimePeriod period = getTodayPeriod(-2, -2);
		
		Decimal totalHours = hours.getTotalBusinessHoursForPeriod(period);
		
		System.assertEquals(6, totalHours);
	}
	
	static testMethod void shouldReturnTotalBusinessHoursWhenEndOccursAfterHours() {
		BusinessHours businessHours = TestDataSet.getBusinessHours(TODAY_START_TIME, TODAY_END_TIME);
		SMBusinessHours hours = new SMBusinessHours(businessHours);
		DateTimePeriod period = getTodayPeriod(+2, +2);
		
		Decimal totalHours = hours.getTotalBusinessHoursForPeriod(period);
		
		System.assertEquals(6, totalHours);
	}
	
	private static DateTimePeriod getTodayPeriod(Integer startOffset, Integer endOffset) {
		return new DateTimePeriod(getTodayStartTime(startOffset), getTodayEndTime(endOffset));
	}
	
	private static SMDateTime getTodayStartTime(Integer offsetInHours) {
		return getTodayDateTime(TODAY_START_TIME, offsetInHours);
	}
	
	private static SMDateTime getTodayEndTime(Integer offsetInHours) {
		return getTodayDateTime(TODAY_END_TIME, offsetInHours);
	}
	
	private static SMDateTime getTodayDateTime(SMTime theTime, Integer offsetInHours) {
		return new SMDateTime(new SMDate(Date.today()),
							  		 theTime.addHours(offsetInHours));
	}
}