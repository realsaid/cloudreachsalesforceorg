@isTest
public without sharing class TestCaseTrigger {
	private static final Boolean IS_TRIGGER_TEST = true;
	private static final SMTime START_TIME = new SMTime(Time.newInstance(9, 0, 0, 0));
	private static final SMTime END_TIME = new SMTime(Time.newInstance(17, 0, 0, 0));
	private static final SMTime NOW = new SMTime(DateTime.now().time());
	private static final Integer CLOCK_DECIMAL_PLACES = 4;
	
	private static ClockUpdateCaseTriggerAction action;
	private static SMCase theCase;
	
	private static void setup(String status, SMTime startTime, SMTime endTime) {
		theCase = getCase(startTime, endTime);
		theCase.setStatus(status);
		action = new ClockUpdateCaseTriggerAction(new Case[]{theCase.getRecord()});
	}
	
	private static SMCase getCase(SMTime startTime, SMTime endTime) {
		return new SMCase(TestDataSet.getCase());
	}
	
	static testMethod void shouldExecuteBeforeUpdateTriggerActionOnUpdate() {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			setup(SMCase.STATUS_ASSIGNING, NOW.addHours(-1), NOW.addHours(+1));
			insert theCase.getRecord();
			ClockUpdateCaseTriggerAction.nBeforeUpdates = 0;
			
			update theCase.getRecord();
			
			System.assertEquals(1, ClockUpdateCaseTriggerAction.nBeforeUpdates);
		}
		else {
			System.assert(true);
		}
	}
}