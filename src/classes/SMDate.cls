public with sharing class SMDate {
	private final Date theDate;
	
	public SMDate(final Date theDate) {
		this.theDate = theDate;	
	}
	
	public Date toSFObject() {
		return theDate;
	}
	
	public DayOfWeek getDayOfWeek() {
		Integer dayOfWeekNumber = Math.mod(Date.newInstance(1970, 1, 1).daysBetween(theDate) - 3, 7);
		return DayOfWeekMapping.get(dayOfWeekNumber);
	}
}