public with sharing class DuplicateTaskFetcher {
	public Task[] fetch(final Integer nTasks, final Date startDate, final Date endDate) {
		final Task[] tasks = [SELECT Id,
                                  Subject,
                                  Description,
                                  WhoId,
                                  Priority,
                                  Status,
                                  CreatedDate
                        FROM Task
                        WHERE Type = 'Email'
                        //AND Alumina__Is_Alumina_Email__c = true 
                        AND CreatedDate >= :startDate
                        AND CreatedDate < :endDate
                        ORDER BY CreatedDate ASC
                        LIMIT :nTasks];
                        
        return tasks;
	}
}