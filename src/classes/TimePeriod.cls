public with sharing class TimePeriod {
	public static final Integer DECIMAL_PLACES = 4;
	
	private SMTime startTime;
	private SMTime endTime;
	
	public TimePeriod(SMTime startTime, SMTime endTime) {
		this.startTime = startTime;
		this.endTime = endTime;                     
	}
	
	public SMTime getStart() {
		return startTime;
	}
	
	public SMTime getEnd() {
		return endTime;
	}
	
	public Decimal getHours() {
		return startTime.hoursBetween(endTime).setScale(DECIMAL_PLACES);
	}
	
	public Boolean doesInclude(SMTime theTime) {
		Boolean doesInclude = false;
		Decimal zero = 0.0000;
		if (theTime != null) {
			if ((startTime.hoursBetween(theTime).setScale(DECIMAL_PLACES) >= zero)
				 && (theTime.hoursBetween(endTime).setScale(DECIMAL_PLACES) >= zero)) {
				doesInclude = true;
			}
		}
		return doesInclude;
	}
	
	public Boolean doesIncludeNow() {
		return doesInclude(new SMTime(DateTime.now().time()));
	}
	
	public Decimal getHoursAfter(SMTime theTime) {
		Decimal hours = 0.0000;
		if (startTime.hoursBetween(theTime).setScale(DECIMAL_PLACES) <= 0.0000) {
			hours += this.getHours();
		}
		else if (this.doesInclude(theTime)) {
			hours += theTime.hoursBetween(endTime);
		}
		else {
			// the time occurs after this period - do not increment hours
		}
		return hours.setScale(DECIMAL_PLACES);
	}
	
	public Decimal getHoursBefore(SMTime theTime) {
		Decimal hours = 0.0000;
		if (endTime.hoursBetween(theTime).setScale(DECIMAL_PLACES) >= 0.0000) {
			hours += this.getHours();
		}
		else if (this.doesInclude(theTime)) {
			hours += startTime.hoursBetween(theTime);
		}
		else {
			// the time occurs before this period - do not increment hours
		}
		return hours.setScale(DECIMAL_PLACES);
	}
	
	public Decimal getOverlappingHours(TimePeriod otherPeriod) {
		Decimal hours = 0.0000;
		if (otherPeriod.doesInclude(startTime) && otherPeriod.doesInclude(endTime)) {
			// otherPeriod contains this period
			hours += this.getHours();
		}
		else if (this.doesInclude(otherPeriod.getStart()) && this.doesInclude(otherPeriod.getEnd())) {
			// this period contains otherPeriod
			hours += otherPeriod.getHours();
		}
		else if (this.doesInclude(otherPeriod.getStart()) && !this.doesInclude(otherPeriod.getEnd())) {
			// otherPeriod overlaps the end of this period
			hours += this.getHoursAfter(otherPeriod.getStart());
		}
		else if (!this.doesInclude(otherPeriod.getStart()) && this.doesInclude(otherPeriod.getEnd())) {
			// otherPeriod overlaps the start of this period
			hours += this.getHoursBefore(otherPeriod.getEnd());
		}
		return hours;
	}
}