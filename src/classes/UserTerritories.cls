public class UserTerritories {
    public Map<Id, Territory> getUserTerritories() {
        // get the current user's territories
        String userId = UserInfo.getUserId(); // get User ID
        // retrieve user territories from UserTerritory table
        Map<Id,UserTerritory> UserTerritoryCurrentUserMap = new Map<Id,UserTerritory>([Select u.UserId, u.TerritoryId, u.IsActive, u.Id  From UserTerritory u 
                                                                                        Where u.isActive=true and u.userId =: userId]);    
        // retieve names of User Territories
        set<Id> TerritoryIdSet = new set<Id>();
        for(UserTerritory ut:UserTerritoryCurrentUserMap.values()){
              TerritoryIdSet.add(ut.TerritoryId);
        }       
        Map<Id, Territory> idToTerritoryMap = new Map<Id, Territory>([SELECT Id, Name FROM Territory WHERE Id IN : TerritoryIdSet]);
        return idToTerritoryMap;
    }  
}