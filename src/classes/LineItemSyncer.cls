public with sharing class LineItemSyncer {
	private static final String QT = '\'';
	
	public void syncLineItemsFromOppToServiceContract(ID oppID, ID conID) {
		Opportunity opp = Database.query(new SelectAll('Opportunity').getSOQL() +
		                                               ' WHERE ID = ' + quotes(oppID) +
		                                               'limit 1');
        ServiceContract con = Database.query(new SelectAll('ServiceContract').getSOQL() +
                                                           ' WHERE ID = ' + quotes(conID) +
                                                           'limit 1');
        
        if (con != null) {
            OpportunityLineItem[] oppLineItems = new SMOpportunity(opp).getLineItems();
            
            ContractLineItem[] conLineItems = new LineItemConverter().convert(oppLineItems, con.ID);
            
            delete [SELECT ID FROM ContractLineItem WHERE ServiceContractID =: opp.Primary_Service_Contract__c];    
            new SMServiceContract(con).addLineItems(conLineItems);
        }
	}
	
	private static String quotes(String str) {
        return QT + str + QT;
    }
}