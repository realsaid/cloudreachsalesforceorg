public with sharing class CRPurchaseOrderComponentExtension {

	public SCMC__Purchase_Order__c purchaseOrder {get;set;}
    public string pId {get;set;}


    public CRPurchaseOrderComponentExtension(SCMC.PurchaseOrderController cont) {
       pid = ApexPages.currentPage().getParameters().get('id');
       purchaseOrder = [select id
                        , name 
                        , SCMFFA__Company__r.AddressMultiLine__c
                        , SCMFFA__Company__r.Name
                        , SCMC__Total_Tax__c
                        from SCMC__Purchase_Order__c 
                        where id =:pid];
    }

}