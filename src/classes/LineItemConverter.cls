public with sharing class LineItemConverter {
	// Map of fields from OpportunityLineItem to ContractLineItem
	public static final Map<String, String> FIELD_MAP = generateFieldMap();
	
	private static Map<String, String> generateFieldMap() {
        Map<String, String> fieldMap = new Map<String, String>();
        fieldMap.put('Description', 'Description');
        fieldMap.put('Discount', 'Discount');
        fieldMap.put('PricebookEntryId', 'PricebookEntryId');
        fieldMap.put('Quantity', 'Quantity');
        fieldMap.put('UnitPrice', 'UnitPrice');
        return fieldMap;
    }
	
	public ContractLineItem[] convert(OpportunityLineItem[] oppLineItems) {
		ContractLineItem[] conLineItems = new ContractLineItem[]{};
		
		ContractLineItem item = null;
		for (OpportunityLineItem oppLineItem : oppLineItems) {
			item = new ContractLineItem();
			for (String key : FIELD_MAP.keySet()) {
				item.put(FIELD_MAP.get(key), oppLineItem.get(key));
			}
			conLineItems.add(item);
	 	}
	 	
	 	return conLineItems;
	}
	
	public ContractLineItem[] convert(OpportunityLineItem[] oppLineItems, ID serviceContractID) {
        ContractLineItem[] conLineItems = new ContractLineItem[]{};
        
        ContractLineItem item = null;
        for (OpportunityLineItem oppLineItem : oppLineItems) {
            item = new ContractLineItem();
            for (String key : FIELD_MAP.keySet()) {
                item.put(FIELD_MAP.get(key), oppLineItem.get(key));
            }
            item.put('ServiceContractID', serviceContractID);
            conLineItems.add(item);
        }
        
        return conLineItems;
    }
}