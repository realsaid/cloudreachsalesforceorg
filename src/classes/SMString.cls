public with sharing class SMString {
	private String sfString;
	
	public SMString(String sfString) {
		this.sfString = sfString;	
	}
	
	public SMString(String[] strings, String delimiter) {
		this.sfString = concatenate(strings, delimiter);
	}
	
	public Boolean isNull() {
        return sfString == null || sfString.trim() == '' ? true : false;
    }
	
	public Boolean isInList(String[] strings) {
        Boolean isPresent = false;
        if (strings != null) {
            for (Integer i = 0; i < strings.size() && !isPresent; ++i) {
                if (sfString == strings[i]) {
                    isPresent = true;
                }
            }
        }
        return isPresent;
    }
    
    private String concatenate(String[] strings, String delimiter) {
    	String concatenation = '';
        if ((strings != null) && (strings.size() > 0) && !(new SMString(delimiter).isNull())) {
        	for (Integer i = 0; i < strings.size() - 1; ++i) {
                concatenation += formatLine(strings[i], delimiter);
            }
            concatenation += strings[strings.size() - 1];
        }
        return concatenation;
    }
    
    private String formatLine(String line, String delimiter) {
    	String formattedLine = '';
    	if (!(new SMString(line)).isNull() && !(new SMString(delimiter).isNull())) {
    		formattedLine = line + delimiter;
    	}
    	return formattedLine;
    }
    
    public String[] toCharArray() {
    	String[] chars = new String[]{};
    	for (Integer i = 0; i < sfString.length(); ++i) {
    		chars.add(sfString.subString(i, i + 1));
    	}
    	return chars;
    }
    
    public String capitalise(Integer character) {
    	if (character < sfString.length()) {
    		String[] chars = toCharArray();
    		chars[character].toUpperCase();
    		sfString = concatenate(chars, '');
    	}
    	return sfString;
    }
    
  	public String inQuotes() {
  		return '\'' + sfString + '\''; 	
  	}
  	
    public String toSFString() {
    	return sfString;
    }
}