@isTest
public class PonCaseEmailServiceTest {

    static testMethod void testEmailIn() {
    
       // Create a new email, envelope object and Attachment
       Messaging.InboundEmail email = new Messaging.InboundEmail();
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
       email.subject = 'test';
       email.htmlBody = bodyTemplate();
       env.fromAddress = 'user@acme.com';

       // call the class and test it with the data in the testMethod
       PonCaseEmailService emailServiceObj = new PonCaseEmailService();
       Messaging.InboundEmailResult result = emailServiceObj.handleInboundEmail(email, env );   

       System.debug('Looking at: '  + emailServiceObj.emailBodyInfoMap); 
       /*// Get the cases and emails
       Case caseCreated = [SELECT Id FROM Case];
       Contact contactCreated = [SELECT Id FROM Contact];
       
       System.assert(caseCreated != null);
       System.assert(contactCreated != null); */
    }
       
    static testMethod void sendEmailOut() {   
 
              // Create a new email, envelope object and Attachment
       Messaging.InboundEmail email = new Messaging.InboundEmail();
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
    
       email.subject = 'test';
       email.htmlBody = bodyTemplate();
       env.fromAddress = 'user@acme.com';
       
       // call the class and test it with the data in the testMethod
       PonCaseEmailService emailServiceObj = new PonCaseEmailService();
       Messaging.InboundEmailResult result = emailServiceObj.handleInboundEmail(email, env );   
       
       Account thisAccount = new Account(Name='Test Account', sales_territory__c = 'UK & Ireland');
       Database.SaveResult srAcc = Database.insert(thisAccount, true);
       System.debug('Account created: ' + srAcc.isSuccess());   
          
       Contact thisContact = new Contact(FirstName = 'Mike',
                                         LastName = 'Borodzin',
                                         Account=[SELECT Id FROM Account WHERE Id=:srAcc.getId()]); 
       Database.SaveResult srContact = Database.insert(thisContact, true);                                  
       System.debug('Contact created: ' + srContact.isSuccess());
       
       Case thisCase = new Case(Type='Incident', ContactId=srContact.getId(), Priority='P4',
                                Origin='Email', Status='New', Subject ='Test Subject', 
                                Description='Test Email', Incident_Component__c='GA - Sites',
                                BusinessHoursId='01m20000000DKdR');  
       Database.SaveResult srCase = Database.insert(thisCase, true);        
       System.debug('Case created: ' + srCase.isSuccess());
       
       emailServiceObj.sendAutoReply(email, result, [SELECT Id FROM Case WHERE Id=:srCase.getId()], [SELECT Id FROM Contact WHERE Id=:srContact.getId()]);
                         
    }    

    private static String bodyTemplate() {
        //String bodyStr = '<div dir="ltr"><h2 style="font-family:Arial">Hi Mark Lehmann,</h2><p style="font-size:13.3333px;';
        
        String bodyStr = '<div dir="ltr"><h2 style="font-family:Arial">Hi Mark Lehmann,</h2><p style="font-size:13.3333px;font-family:Arial">We received your request with the following details:</p><table style="font-size:13.3333px;font-family:Arial">'
        +'<tbody><tr><th colspan="1" rowspan="1">User</th><td colspan="1" rowspan="1">Mark Lehmann</td></tr><tr><th colspan="1" rowspan="1">Email</th><td colspan="1" rowspan="1"><a href="mailto:admin.mlehmann@pon.com" target="_blank">f</a><a href="mailto:abien.flight@cloudreach.com" target="_blank">abien.flight@cloudreach.com</a></td>'
        +'</tr><tr><th colspan="1" rowspan="1">Organization</th><td colspan="1" rowspan="1">Googleadmins</td></tr><tr><th colspan="1" rowspan="1">What is your question about?</th><td colspan="1" rowspan="1">Groups</td></tr><tr><th colspan="1" rowspan="1">'
        +'I would like to</th><td colspan="1" rowspan="1">Ask a question about Google Groups</td></tr><tr><th colspan="1" rowspan="1">Subject</th><td colspan="1" rowspan="1">pass on to fabian</td></tr><tr><th colspan="1" rowspan="1">'
        +'Description</th><td colspan="1" rowspan="1">Please send this onto fabien as test<br><br></td></tr></tbody></table><div><br></div>-- <br><br><b>Fabien Flight</b> <br> Systems Developer, Cloudreach Europe<br> [t] <a value="00442071833893">+44 20 7183 3893</a> (ext. 410)<br>'
         +'[m] <a value="0044 7510 139 430">+44 7510 139 430</a><br> <br><a href="http://goo.gl/YjdhZv" target="_blank">Not if. When.</a><br> <br></div>';
        
        return bodyStr;
    }
}