@isTest(seeAllData=true)
public with sharing class TestSMOpportunityLineItem {
	static testMethod void shouldBeEqualToOpportunityLineItemGivenRelevantFieldsAreEqual() {
		OpportunityLineItem item1 = TestDataSet.getOpportunityLineItem();
		OpportunityLineItem item2 = item1.clone();
		
		Boolean areEqual = new SMOpportunityLineItem(item1).equals(item2);
		
		System.assertEquals(true, areEqual);
	}
}