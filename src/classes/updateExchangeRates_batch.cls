global class updateExchangeRates_batch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    
    String query;
    global string CompanyId {get;set;}
    global list<groupMember> originalGroupMembership;
    global string currentUserId {get;set;}
    
    global updateExchangeRates_batch(c2g__codaCompany__c company) {
        
        this.companyId = company.id;
        currentUserId = userInfo.getUserId();

        originalGroupMembership = [select id, groupId, userOrGroupId from groupMember where userOrGroupId = :userInfo.getUserId()];
        if(!test.isRunningTest()) delete originalGroupMembership;




        groupMember newGroupMembership = new groupMember(groupId = company.ownerId, userOrGroupId = currentUserId);
        if(!test.isRunningTest()) insert newGroupMembership;
        
	    
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        //set new group membership


        query = 'select id'
                    + ', Name '
                    + ', ownerId '
                    + ', (select id '
                    + ' , name '
                    + ' , c2g__Home__c '
                    + ' , c2g__OwnerCompany__c '
                    + ' from c2g__AccountingCurrencies__r) '
                    + 'from c2g__codaCompany__c '
                    + 'where id = :companyId'; 

        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {

        UpdateExchangeRates con = new UpdateExchangeRates(companyId);
    
    }
    
    global void finish(Database.BatchableContext BC) {
        
    	list<groupMember> currGroupMembership = [select id, groupId, userOrGroupId from groupMember where userOrGroupId = :userInfo.getUserId()];
        if(!test.isRunningTest()) delete currGroupMembership;

        list<groupMember> reinstatedGroupMembership = new list<groupMember>{};
            
            for(groupMember g : originalGroupMembership){
                reinstatedGroupmembership.add(new groupMember(groupId = g.groupId, userOrGroupId = g.userOrGroupId));
                
                
            }
        if(!test.isRunningTest()) insert reinstatedGroupMembership;
	    
    }
    
}