@isTest
private class UpdateExchangeRates_test{

/*	@testSetup
	static void createData(){



	}*/

	@istest (seeAllData = true)
	public static void  testCron() {

		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 

		user u = new user(Alias = 'standt'
						, Email='standarduser@testorg.com'
						, EmailEncodingKey='UTF-8'
						, LastName='Testing'
						, LanguageLocaleKey='en_US'
						, LocaleSidKey='en_US'
						, ProfileId = p.Id
						, TimeZoneSidKey='America/Los_Angeles'
						, UserName='standarduserzzz@testorg.com');
		insert u;

		list<c2g__codaCompany__c> fullCompanyList = [select id
												, name
												, ownerId
												, (select id from c2g__AccountingCurrencies__r)
												 from c2g__codaCompany__c order by name limit 1];
		c2g__codaCompany__c company = fullCompanyList.get(0);

        groupMember newGroupMembership = new groupMember(groupId = company.ownerId, userOrGroupId = u.id);
        insert newGroupMembership;



		Test.StartTest();
			system.runas(u){

				updateExchangeRates_cron sh1 = new updateExchangeRates_cron();      
			 	String sch = '0 0 23 * * ?';
				system.schedule('Test check', sch, sh1);
			}
		Test.stopTest();
	}
}