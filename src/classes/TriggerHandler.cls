public with sharing class TriggerHandler {
	public static void onAfterInsert(final List<TriggerAction> actions) {
		for (TriggerAction action : actions) {
			action.onAfterInsert();
		}
	}
	
	public static void onBeforeUpdate(final List<TriggerAction> actions) {
		for (TriggerAction action : actions) {
			action.onBeforeUpdate();
		}
	}
	
	public static void onBeforeInsert(final List<TriggerAction> actions) {
		for (TriggerAction action : actions) {
			action.onBeforeInsert();
		}
	}
}