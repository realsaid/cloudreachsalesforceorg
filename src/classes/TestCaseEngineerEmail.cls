@isTest
public with sharing class TestCaseEngineerEmail {
	public static Boolean IS_TRIGGER_TEST = true;
	
	private static EngineerEmailCaseTriggerAction action;
	private static Case theCase;
	
	private static void setup(String status) {
		theCase = TestDataSet.getCase();
		theCase.Status = status;
		theCase.Assigned_Engineer__c = 'Engineer';
		theCase.Assigned_Engineer_Email__c = 'engineer@cloudreach.co.uk';
		action = new EngineerEmailCaseTriggerAction(new Case[]{theCase});
	}
	
	private static void performInsertTest() {
		Test.startTest();
		if (IS_TRIGGER_TEST) {
			insert theCase;
		}
		else {
			action.onBeforeInsert();
		}
		Test.stopTest();
	}
	
	private static void performUpdateTest() {
		Test.startTest();
		if (IS_TRIGGER_TEST) {
			insert theCase;
			update theCase;
		}
		else {
			action.onBeforeUpdate();
		}
		Test.stopTest();
	}
	
	public static void shouldNotEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsNotAnEmailStatus() {
		shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIs('Not An Email Status', false);
	}
	
	public static void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsPendingVendorResponse() {
		shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_PENDING_VENDOR, true);
	}
	
	public static void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsPendingCloudreachResponse() {
		shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_PENDING_CLOUDREACH, true);
	}
	
	public static void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsAssigning() {
		shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_ASSIGNING, true);
	}
	
	public static void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsClosed() {
		shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_CLOSED, true);
	}
	
	private static void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIs(String status, Boolean shouldEmail) {
		setup(status);
		
		performInsertTest();
		
		System.assertEquals(shouldEmail, EngineerEmailCaseTriggerAction.DID_SEND_EMAIL);
	}
	
	public static void shouldNotEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsNotAnEmailStatus() {
		shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIs('Not An Email Status', false);
	}
	
	public static void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsPendingVendorResponse() {
		shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_PENDING_VENDOR, true);
	}
	
	public static void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsPendingCloudreachResponse() {
		shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_PENDING_CLOUDREACH, true);
	}
	
	public static void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsAssigning() {
		shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_ASSIGNING, true);
	}
	
	public static void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsClosed() {
		shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIs(SMCase.STATUS_CLOSED, true);
	}
	
	private static void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIs(String status, Boolean shouldEmail) {
		setup(status);
		
		performUpdateTest();
		
		System.assertEquals(shouldEmail, EngineerEmailCaseTriggerAction.DID_SEND_EMAIL);
	}
}