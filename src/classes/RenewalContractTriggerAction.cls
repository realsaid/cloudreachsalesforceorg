public with sharing class RenewalContractTriggerAction extends TriggerAction {
    private ServiceContract[] oldContracts;
    private ServiceContract[] newContracts;
    
    public RenewalContractTriggerAction(final ServiceContract[] oldContracts, final ServiceContract[] newContracts) {
        this.oldContracts = oldContracts;
        this.newContracts = newContracts;
    }
    
    public override void onBeforeUpdate() {
        if (shouldPerformAction()) {
            performAction();
        }
    }
    
    private Boolean shouldPerformAction() {
        return newContracts.size() == 1 &&
               newContracts[0].Renewal_Type__c == SMServiceContract.RENEWAL_TYPE_RENEWAL_EXPECTED &&
               endDateIsNewlySet(oldContracts[0], newContracts[0]);
    }
    
    private Boolean endDateIsNewlySet(ServiceContract oldContract, ServiceContract newContract) {
        return (oldContract.EndDate == null) && (newContract.EndDate != null);
    }
    
    private void performAction() {
        try {
            ServiceContract renewalContract = newContracts[0];
            Opportunity originalOpp = new SMServiceContract(renewalContract).getMostRecentlyClosedRelatedOpportunity(); 
            if (originalOpp != null) {
                Opportunity renewalOpp = new SMOpportunity(originalOpp).insertClone();
                // The new opportunity will be type Renewal
                renewalOpp.Type = SMOpportunity.TYPE_RENEWAL;
                // We explicitly want the new opportunity to have this stage name
                renewalOpp.StageName = SMOpportunity.STAGE_NAME_LEGAL_AND_PROCUREMENT;
                // Satisfy validation rule
                //renewalOpp.Reasons_For_Loosing__c = null;
                // We want the end date to reflect the contract's end date
                renewalOpp.CloseDate = renewalContract.EndDate;
                // We don't want to rely on underlying cloning code for this field as it will usually be disabled
                renewalOpp.Primary_Service_Contract__c = originalOpp.Primary_Service_Contract__c;
                update renewalOpp;
            }
        }
        catch (Exception e) {
            System.Debug('Exception caught in RenewalContractTriggerAction.performAction: ' + e.getMessage());
        }
    }
}