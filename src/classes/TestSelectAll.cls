@isTest
public with sharing class TestSelectAll {
	static testMethod void shouldSelectAllFieldsFromOpportunity() {
		Opportunity opp = TestDataSet.getOpportunity();
		insert opp;
		String baseQuery = new SelectAll('Opportunity').getSOQL();
		String query = baseQuery + ' WHERE Name=\'' + opp.Name + '\' limit 1';
				
		opp = Database.query(query);
		
		Boolean canReadFields = true;
		try {
			String Type = opp.Type;
		}
		catch (SObjectException e) {
			canReadFields = false;
		}
		System.assertEquals(true, canReadFields);
	}
}