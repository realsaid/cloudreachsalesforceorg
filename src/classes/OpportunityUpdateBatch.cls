/************************************************************************

	Name:			OpportunityUpdateBatch.cls (Batchable)
	Created Date:	8/10/2015
	Created By:		Charlotte Gibson
	Purpose:		Batched class to select all unclosed opps and their OLIs
					Loop through and identify the primary product family of the OLIs 
					based on combined value
					
					Job is scheduled via the schedulable class
					OpportunityUpdateBatch_Cron


************************************************************************/



global class OpportunityUpdateBatch implements Database.Batchable<sObject> {
	
	String query;
	
	global OpportunityUpdateBatch() {
		
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		query = 'select id '
					+ ', name '
					+ ', derived_bu__c '
					+ ', (select id '
						+ ', name '
						+ ', Family__c '
						+ ', TotalPrice '
						+ 'from OpportunityLineItems) '
					+ 'from Opportunity '
					+ ' where isClosed != true';

		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		list<Opportunity> oppList = scope;
		list<Opportunity> oppUpdateList = new list<Opportunity>{};

		for(opportunity opp : oppList){
			integer loopCounter = 1;
			map<string, decimal> familyMap = new map<string, decimal>{};
			for(opportunityLineItem oli : opp.opportunitylineItems){
				system.debug('starting loop ' + loopcounter);
				//if family is not in familyMap, put it in the family map with total Price amount
				if(!familyMap.containsKey(oli.Family__c)){
					familyMap.put(oli.Family__c, oli.TotalPrice);
					system.debug('oli0: ' + oli.family__c);

				//otherwise, store amount value, remove from map, put back in map with additional oli amount
				} else {
					decimal amount = familyMap.get(oli.Family__c);
					familyMap.remove(oli.Family__c);
					system.debug('oli1: ' + oli.family__c);
					system.debug('FamilyMap1: ' + FamilyMap);

					system.debug('amount before: ' + amount);

					amount = amount + oli.TotalPrice;
					system.debug('amount after: ' + amount);

					familyMap.put(oli.Family__c, amount);
					system.debug('FamilyMap2: ' + FamilyMap);

				}
				system.debug('ending loop ' + loopcounter);
				loopcounter++;

			}

			system.debug('FamilyMap: ' + FamilyMap);
			string biggestFamily;
			decimal biggestValue = 0;

			//get highest integer value in familyMap
			for(string fam : familyMap.keySet()){
				//do stuff
				system.debug('Firefly  Fam: ' + fam + ' familyMap.get(fam): ' + familyMap.get(fam) + ' biggestValue: ' + biggestValue);
				if(familyMap.get(fam) > biggestValue){
					biggestValue = familyMap.get(fam);
					biggestFamily = fam;

				}

			}

			//write biggestFamily to opp.derived_bu__c
			system.debug('biggestFamily: ' + biggestFamily);
			if(opp.derived_bu__c != biggestFamily){
				opp.derived_bu__c = biggestFamily;
				oppUpdateList.add(opp);
			}

		}
		update oppUpdateList;

	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}