@isTest
public with sharing class TestRandom {
	static testMethod void shouldReturnRandomBoolean() {
		Random random = new Random();
		
		Boolean randomBoolean = random.getBoolean();
		
		System.assert(randomBoolean == true || randomBoolean == false);
	}
	
	static testMethod void shouldReturnRandomInteger() {
		Random random = new Random();
		
		Integer randomInteger1 = random.getInteger();
		Integer randomInteger2 = random.getInteger();
		
		System.assertNotEquals(randomInteger1, randomInteger2);
	}
	
	static testMethod void shouldReturnRandomDouble() {
		Random random = new Random();
		
		Double randomDouble1 = random.getDouble();
		Double randomDouble2 = random.getDouble();
		
		System.assertNotEquals(randomDouble1, randomDouble2);
	}
	
	static testMethod void shouldReturnRandomDecimal() {
		Random random = new Random();
		
		Decimal randomDecimal1 = random.getDecimal();
		Decimal randomDecimal2 = random.getDecimal();
		
		System.assertNotEquals(randomDecimal1, randomDecimal2);
	}
	
	static testMethod void shouldReturnRandomString() {
		Random random = new Random();
		
		String randomString1 = random.getString();
		String randomString2 = random.getString();
		
		System.assertNotEquals(randomString1, randomString2);
	}
	
	static testMethod void shouldReturnRandomStringFromList() {
		Random random = new Random();
		String[] strings = new String[]{'i', 'am', 'a', 'list', 'of', 'strings'};
		
		String randomString = random.getString(strings);
		
		Set<String> setOfStrings = new Set<String>();
		setOfStrings.addAll(strings);
		System.assert(setOfStrings.contains(randomString));
	}
	
	static testMethod void shouldReturnRandomDateGreaterThanMinDate() {
		Random random = new Random();
		Date minDate = Date.today();
		
		Date randomDate = random.getDate(minDate);
		
		System.assertEquals(true, minDate.daysBetween(randomDate) > 0);
	}
	
	static testMethod void shouldReturnRandomTime() {
		Random random = new Random();
		
		Time randomTime1 = random.getTime();
		Time randomTime2 = random.getTime();
		
		System.assertNotEquals(randomTime1, randomTime2);
	}
	
	static testMethod void shouldReturnRandomDateTime() {
		Random random = new Random();
		
		DateTime randomDateTime1 = random.getDateTime();
		DateTime randomDateTime2 = random.getDateTime();
		
		System.assertNotEquals(randomDateTime1, randomDateTime2);
	}
	
	static testMethod void shouldReturnRandomOpportunity() {
		Random random = new Random();
		
		Opportunity randomOpportunity1 = random.getOpportunity();
		Opportunity randomOpportunity2 = random.getOpportunity();
		
		System.assertNotEquals(randomOpportunity1, randomOpportunity2);
	}
	
	static testMethod void shouldReturnRandomAccount() {
		Random random = new Random();
		
		Account randomAccount1 = random.getAccount();
		Account randomAccount2 = random.getAccount();
		
		System.assertNotEquals(randomAccount1, randomAccount2);
	}
	
	static testMethod void shouldReturnRandomContact() {
		Random random = new Random();
		
		Contact randomContact1 = random.getContact();
		Contact randomContact2 = random.getContact();
		
		System.assertNotEquals(randomContact1, randomContact2);
	}
	
	static testMethod void shouldReturnRandomCampaign() {
		Random random = new Random();
		
		Campaign randomCampaign1 = random.getCampaign();
		Campaign randomCampaign2 = random.getCampaign();
		
		System.assertNotEquals(randomCampaign1, randomCampaign2);
	}
	
	static testMethod void shouldReturnRandomServiceContract() {
		Random random = new Random();
		
		ServiceContract randomServiceContract1 = random.getServiceContract();
		ServiceContract randomServiceContract2 = random.getServiceContract();
		
		System.assertNotEquals(randomServiceContract1, randomServiceContract2);
	}
	
	static testMethod void shouldReturnRandomOpportunityLineItem() {
		Random random = new Random();
		
		OpportunityLineItem randomOpportunityLineItem1 = random.getOpportunityLineItem();
		OpportunityLineItem randomOpportunityLineItem2 = random.getOpportunityLineItem();
		
		System.assertNotEquals(randomOpportunityLineItem1, randomOpportunityLineItem2);
	}
}