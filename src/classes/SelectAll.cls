public class SelectAll {
    private String soql;
    private String[] exceptions;
    
    public SelectAll(String objectType) {
    	exceptions = new String[]{};
        soql = buildQueryFor(objectType);
    }
    
    public SelectAll(String objectType, String[] exceptions) {
    	this.exceptions = exceptions;
    	soql = buildQueryFor(objectType);
    }
    
    private String buildQueryFor(String objectType) {
    	String query = '';
        for(String field : fieldNames(objectType)) {
        	if (shouldInclude(field, exceptions)) {
            	query += field + ', ';
        	}
        }
        query = query.Substring(0, query.length() - 2);
        soql  = 'SELECT ' + query + ' FROM ' + objectType;
        return soql;
    }
    
    private Set<String> fieldNames(String objectTypeName) {
    	Map<String, Schema.SObjectType> allSObjects = schema.getGlobalDescribe();
        Schema.SObjectType objectType = allSObjects.get(objectTypeName);
        Schema.DescribeSObjectResult describeResult = objectType.getDescribe();
        Map<String, Schema.SObjectfield> fieldMap = describeResult.fields.getMap();
        Set<String> fieldNames = fieldMap.Keyset();
        return fieldNames;
    }
    
    private Boolean shouldInclude(String field, String[] exceptions) {
    	Boolean include = true;
    	if (exceptions.size() > 0) {
	    	for (String except : exceptions) {
	    		if (field.toLowerCase().equals(except.toLowerCase())) {
	    			include = false;
	    		}
	    	}
    	}
    	return include;
    }
    
    public String getSOQL() {
    	return soql;
    }
}