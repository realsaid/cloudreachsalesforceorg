public with sharing class SMTime {
	public static final Integer DECIMAL_PLACES = 4;
	
	private final Time theTime;
	
	public SMTime(final Time theTime) {
		this.theTime = theTime;
	}
	
	public Time toSFObject() {
		return theTime;
	}
	
	public Decimal getHours() {
		Decimal hours = null;
		TimeConverter converter = new TimeConverter();
		hours = theTime.hour()
			  + converter.minutesToHours(theTime.minute())
			  + converter.secondsToHours(theTime.second())
			  + converter.millisecondsToHours(theTime.millisecond());
		return hours.setScale(DECIMAL_PLACES);
	}
	
	public SMTime addSeconds(Integer seconds) {
		return new SMTime(theTime.addSeconds(seconds));
	}
	
	public SMTime addHours(Integer hours) {
		return new SMTime(theTime.addHours(hours));
	}
	
	public Decimal hoursBetween(SMTime otherTime) {
		Decimal hours = null;
		if (otherTime != null) {
			hours = otherTime.getHours() - getHours();
		}
		return hours.setScale(DECIMAL_PLACES);
	}

	public static SMTime now() {
		return new SMTime(Datetime.now().time());
	}
	
	public Boolean equals(SMTime otherTime) {
		Boolean isEqual = false;
		if (this.toSFObject() == otherTime.toSFObject()) {
			isEqual = true;
		}
		return isEqual;
	}

	public override String toString() {
		String[] details = new String[]{'DECIMAL_PLACES: ' + DECIMAL_PLACES,
				   'this.theTime: ' + this.theTime,
				   'hours: ' + getHours()};

		return String.join(details, '\n');
	}
}