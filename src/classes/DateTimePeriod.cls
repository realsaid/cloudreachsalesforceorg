public with sharing class DateTimePeriod {
	private SMDateTime startDateTime;
	private SMDateTime endDateTime;
	
	public DateTimePeriod(SMDateTime startDateTime, SMDateTime endDateTime) {
		this.startDateTime = startDateTime;
		this.endDateTime = endDateTime;                                 
	}
	
	public SMDateTime getStart() {
		return startDateTime;
	}
	
	public SMDateTime getEnd() {
		return endDateTime;
	}
	
	public Integer getDays() {
		return startDateTime.daysBetween(endDateTime);
	}
}