@isTest
private class FFTriggerTesting{

    /*
    ExpenseReportBeforeInsertUpdate.trigger
    PayableInvoiceExpenseLineItemBeforeInsert.trigger
    PayableInvoiceLineItemBeforeInsert
    PurchaseOrderBeforeInsertUpdate.trigger
    SalesInvoiceLineItemBeforeInsert.trigger

    CG - 1/9/2016: these tests were written quickly to expediate fast deployment of Financial Force
    They are using SeeAllData as per FF's recommendation - so they require data to be in place before tests
    can pass. They will not work in config-only sandboxes where there is no FF data created.
    */


    @isTest (SeeAllData = true)
    static void expenseReportTriggerTesting(){
        
        
        //get assignment record, to derive project and user records for expense report

        pse__Assignment__c ass = [select id
                                    , pse__Project__r.id
                                    , pse__Resource__r.id
                                    , pse__Resource__r.pse__Salesforce_User__r.id
                                    from pse__Assignment__c
                                    where pse__Resource__r.pse__Salesforce_User__r.id = '0052000000601L0'


                                    limit 1];

        //get user to attach expense report to
        user u = [select id, FF_Company__c 
                    from user 
                    where id =: ass.pse__Resource__r.pse__Salesforce_User__r.id];


        //create expense matrix record. Currently no way to get global picklist values, so we need to hard
        //code this. :(
        Expense_matrix__c em = new Expense_matrix__c(
            expense_category__c = 'London/Edinburgh to Any UK City'
            , expense_limit__c = 12
            );
        insert em;

        //get an expense record to clone



        system.runas(u){
            pse__Expense_Report__c er = new pse__Expense_Report__c(
                  name = 'test er'
                , pse__Resource__c = ass.pse__Resource__r.id
                , pse__Project__c = ass.pse__Project__r.id
                , pse__Assignment__c = ass.id
                );
            insert er;


            //test updating an expense record. (quick and dirty. Inserting an expense record hit all sorts of issues with 3rd party apps.)

/*            pse__Expense__c exp = [select id 
                                    from pse__Expense__c 
                                    limit 1];

            exp.Category_Type__c = 'London/Edinburgh to Any UK City';
            update exp;
*/
            
        
        }



    }



  @isTest (SeeAllData = true)
    static void payableInvoiceTriggerTesting(){

        //for testing payable invoice expense line item 
        //and payable invoice line item

        Id accCustRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();


        account acc = [select id 
                        from account 
                        where RecordTypeId =:accCustRT 
                        and c2g__CODAAccountsPayableControl__c != null
                        limit 1];

        user u = [select id 
                    , FF_Company__c
                    from user 
                    where User.ID = '0052000000601L0'
                    and IsActive = true
                    and FF_Company__c != null
                    limit 1];
        
            //Get GLA
            c2g__codaGeneralLedgerAccount__c gla = [Select ID from c2g__codaGeneralLedgerAccount__c Limit 1];
            
        //get company
        c2g__codaCompany__c company = [select id 
                                    from c2g__codaCompany__c
                                        where name =: u.FF_Company__c
                                        limit 1];
        //get product

        product2 prod = [select id 
                            from product2 
                            where isActive = true 
                            limit 1];

        SCMC__Purchase_Order__c po = new SCMC__Purchase_Order__c(
            SCMFFA__Company__c = company.id
            , SCMC__Buyer_User__c = u.id
            );
        insert po;

/*        SCMC__AP_Voucher__c v = new SCMC__AP_Voucher__c(
            SCMC__Invoice_Number__c = '3i4uh'
            , SCMC__Invoice_Date__c = system.today()
            , SCMC__Purchase_Order__c = po.id
            , SCMC__Due_Date__c = system.today()

            );
        insert v;


        SCMC__AP_Voucher_Line__c vl = new SCMC__AP_Voucher_Line__c(
            SCMC__AP_Voucher__c = v.id
            );
        insert vl;*/

        system.runas(u){
            c2g__codaPurchaseInvoice__c pi = new c2g__codaPurchaseInvoice__c(
                c2g__AccountInvoiceNumber__c = 'xxrrtesttestrrxxrandom'
                , c2g__Account__c = acc.id
                //, Purchase_Order__c = po.id
                );
            insert pi;


            c2g__codaPurchaseInvoiceLineItem__c pili = new c2g__codaPurchaseInvoiceLineItem__c(
                c2g__PurchaseInvoice__c = pi.id
                , c2g__Quantity__c = 1
                , c2g__UnitPrice__c = 1
                , c2g__Product__c = prod.id
                //, SCMFFA__AP_Voucher_Line__c = vl.id
                );
            insert pili;


            //test c2g__codaPurchaseInvoiceExpenseLineItem__c
            c2g__codaPurchaseInvoiceExpenseLineItem__c pieli = new c2g__codaPurchaseInvoiceExpenseLineItem__c(
                c2g__NetValue__c = 1
                ,c2g__PurchaseInvoice__c = pi.id
                ,c2g__GeneralLedgerAccount__c = gla.Id
                );
            insert pieli;

        }


        


    }

    @isTest (SeeAllData=true)
    static void PurchaseorderTriggerTesting(){
        // Given
        //get user to run as
        user u = [select id 
                    , FF_Company__c
                    from user 
                    where User.ID = '0052000000601L0'
                    and IsActive = true
                    and FF_Company__c != null
                    limit 1];
        
        //get company
        c2g__codaCompany__c company = [select id 
                                        from c2g__codaCompany__c
                                        where name =: u.FF_Company__c
                                        limit 1];
        // When

        //create ICP & warehouse
        SCMC__ICP__c icp = new SCMC__ICP__c(
            name = 'test icp'
            );
        insert icp;

        SCMC__Warehouse__c wh = new SCMC__Warehouse__c(
            SCMC__ICP__c = icp.id, 
            name = 'text warehouse'
            );
        insert wh;

        // Then
        system.runAs(u){
            SCMC__Purchase_Order__c po = new SCMC__Purchase_Order__c(
                SCMFFA__Company__c = company.id
                , SCMC__Buyer_User__c = u.id
                );
            insert po;

            Id piliRT = Schema.SObjectType.SCMC__Purchase_Order_Line_Item__c.getRecordTypeInfosByName().get('Description').getRecordTypeId();

            SCMC__Purchase_Order_Line_Item__c pili = new SCMC__Purchase_Order_Line_Item__c(
                SCMC__Description__c = 'test desciption'
                , SCMC__Quantity__c = 1
                , SCMC__Unit_Cost__c = 1
                , SCMC__Purchase_Order__c = po.id
                , SCMC__Amount_Vouchered__c = 1
                , SCMC__Quantity_Vouchered__c = 1
                , SCMC__Warehouse__c = wh.id
                , RecordTypeId = piliRT
                );
            insert pili;
        }   
    }

    @isTest (SeeAllData = true)
    static void SalesInvoiceTriggerTesting(){
        // Given
        //insert salesInvoiceLineItem

        Id accCustRT = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();


        account acc = [select id 
                        from account 
                        where RecordTypeId =:accCustRT 
                        and c2g__CODAAccountsPayableControl__c != null
                        limit 1];

        opportunity opp = new opportunity(
            accountId = acc.id
            , closeDate = system.today()
            , stageName = 'Closed Won'
            , Contract_Type__c = 'FA'
            , leadsource = 'Referral'
            );

        product2 prod = [select id from product2 where isActive = true limit 1];
        //insert opp;

        c2g__codaInvoice__c invoice = new c2g__codaInvoice__c(
            c2g__InvoiceDate__c = system.today()
            , c2g__DueDate__c = system.today() + 7
            , c2g__Account__c = acc.id
            //, c2g__Opportunity__c = opp.id
            );
        insert invoice;

        // When
        c2g__codaInvoiceLineItem__c ili = new c2g__codaInvoiceLineItem__c(
            c2g__Invoice__c = invoice.id
            ,c2g__UnitPrice__c = 10
            ,c2g__Quantity__c = 10
            , c2g__Product__c = prod.id);
        insert ili;
        // Then

    } 

    
}