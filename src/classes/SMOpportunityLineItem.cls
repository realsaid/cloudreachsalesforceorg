public with sharing class SMOpportunityLineItem {
	private static final String ID = 'ID';
	private static final String OPPORTUNITY_ID = 'OpportunityID';
	private static final String LAST_MODIFIED_DATE = 'LastModifiedDate';
	private static final String SYSTEM_MOD_STAMP = 'SystemModStamp';
	private static final String CREATED_DATE = 'CreatedDate';
	private static final String TOTAL_PRICE = 'TotalPrice';
	
	private static final String[] MATCH_FIELD_EXCEPTIONS = new String[]{ID,
																		OPPORTUNITY_ID,
																  		LAST_MODIFIED_DATE,
																  		SYSTEM_MOD_STAMP,
																  		CREATED_DATE,
																  		TOTAL_PRICE};
	
	private OpportunityLineItem lineItem;
	
	public SMOpportunityLineItem(OpportunityLineItem lineItem) {
		this.lineItem = lineItem;
	}
	
	public Boolean equals(OpportunityLineItem otherLineItem) {
		Boolean isEqual = true;
        String[] matchFields = getMatchFields();
		for (Integer i = 0; i < matchFields.size() && isEqual; ++i) {
        	if (lineItem.get(matchFields[i]) != otherLineItem.get(matchFields[i])) {
        		isEqual = false;
        	}
		}
		return isEqual;
	}
	
	
	private String[] getMatchFields() {
		Map<String, Schema.SObjectfield> fieldMap = OpportunityLineItem.getSObjectType().getDescribe().fields.getMap();
		Set<String> allFields = fieldMap.keySet();
		String[] matchFields = new String[]{};
		for (String fieldException : MATCH_FIELD_EXCEPTIONS) {
        	allFields.remove(fieldException.toLowerCase());
        }
        matchFields.addAll(allFields);
        return matchFields;
	}
	
	public override String toString() {
		String toString = '';
		Map<String, Schema.SObjectfield> fieldMap = OpportunityLineItem.getSObjectType().getDescribe().fields.getMap(); 
		for (String field : fieldMap.keySet()) {
			toString += '' + field + ': ' + lineItem.get(field) + '\n';
		}
		return toString;
	}
	
	public String getProductCode() {
		return [SELECT ProductCode FROM PriceBookEntry WHERE Id = :this.lineItem.PriceBookEntryId].ProductCode; 
	}
}