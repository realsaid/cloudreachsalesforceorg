global class PonCaseEmailService implements Messaging.InboundEmailHandler{
    
    public Map<String, String> emailBodyInfoMap = new Map<String, String>();                                               
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, 
                                                           Messaging.InboundEnvelope envelope) {

        
        
        Case thisCase = new Case();
        Contact thisContact = new Contact();
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        // Populate the map with info extracted from the email body content
        getInfoFromBodyContent(email.htmlBody);
        System.debug('Originator Email Address: ' + emailBodyInfoMap.get('EmailAddress'));
        System.debug('Contact Name: ' + emailBodyInfoMap.get('ContactName'));
                                                               
        try {
            // Look for the email address in the existing contacts and create if necessary
            if ([SELECT Count() FROM Contact WHERE Email = :emailBodyInfoMap.get('EmailAddress')] == 0) {
                System.debug('New Contact detected');
                List<String> nameSegments = emailBodyInfoMap.get('ContactName').split(' ');
                System.debug('Contact split as ' + nameSegments);
                
                if (nameSegments.size() > 1) {
                    thisContact.FirstName = emailBodyInfoMap.get('ContactName').split(' ')[0];
                    thisContact.LastName = emailBodyInfoMap.get('ContactName').split(' ')[1];
                } else if (nameSegments.size() == 1) {
                    thisContact.FirstName = '';
                    thisContact.LastName = emailBodyInfoMap.get('ContactName').split(' ')[0];               
                }
                thisContact.Email = emailBodyInfoMap.get('EmailAddress');

                //thisContact.CurrencyIsoCode = 'GBP - British Pound';
                insert thisContact;
                System.debug('Contact successfully inserted');

            } else {
                thisContact = [SELECT Id FROM Contact WHERE Email=:emailBodyInfoMap.get('EmailAddress')];
            }
            
            // Match the case via the subject field entry (which should correspond to the email subject)
            // Otherwise create one
            if ([SELECT Count() FROM Case WHERE Subject =:email.subject]==0) {
                System.debug('Trying to create new case');
                thisCase = new Case(Type='Incident', ContactId=thisContact.Id, Priority='P4',
                                Origin='Email', Status='New', Subject =email.subject, 
                                Description=email.plainTextBody.replace('\n','\r\n\n'), Incident_Component__c='GA - Sites',
                                BusinessHoursId='01m20000000DKdR');
                insert thisCase; 
                System.debug('New case created');
            } else {
                System.debug('Found exisiting case');
                thisCase = [SELECT Id FROM Case WHERE Subject =:email.subject];
            }
            
            result.success = true;
            
            sendAutoReply(email, result, thisCase, thisContact);
             
        }  catch (Exception e) {
            result.success = false;
            result.message = e.getMessage();
        }  
        return result;                                                   
    }
    
    public void getInfoFromBodyContent(String emailBodyContent) {
        
        // Get Email Address
        emailBodyInfoMap.put('EmailAddress', regexFilter('EmailAddress', emailBodyContent));     
        emailBodyInfoMap.put('ContactName', regexFilter('ContactName', emailBodyContent));     
    } 
    
    private String regexFilter(String infoType, String emailBodyContent) {
        /*
            Method that performs regex expression on the email body content
        */
        String originatorEmailAddress;
        String regexExpression;
        
        if (infoType == 'EmailAddress') {
            regexExpression = '<th([^>]*)>Email<\\/th><td([^>]*)><a([^>]*)>([^<]*)<\\/a><\\/td>';    
        } else if (infoType == 'ContactName') {
            regexExpression = '<tr><th([^>]*)>User<\\/th>([^<]*)<td([^>]*)>([^<]*)<\\/td><\\/tr>';
        }
        
        Pattern emailPattern = Pattern.compile(regexExpression);
        Matcher matcher = emailPattern.matcher(emailBodyContent);
        if (matcher.find()) 
        {
           if (infoType == 'EmailAddress') {
              originatorEmailAddress = matcher.group(4);
           } else {
              originatorEmailAddress = matcher.group(4);
           }
        } 
        return originatorEmailAddress;
    }
    
    public void sendAutoReply(Messaging.InboundEmail email, Messaging.InboundEmailResult result, Case thisCase, Contact thisContact) {

        final String template = 'Case: PON - New Group';
        Id templateId;
        
        System.debug('Attempting to send email to ' + emailBodyInfoMap.get('EmailAddress'));
        try {
            templateId = [SELECT Id FROM EmailTemplate WHERE Name = :template].Id;
            System.debug('templateId: ' + templateId);
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setTemplateId(templateId);
            System.debug('templateId set');
            message.setTargetObjectId(thisContact.Id);
            System.debug('Target object set');
            message.setWhatId(thisCase.Id);
            System.debug('setWhatId set');
            message.setToAddresses(new String[] {emailBodyInfoMap.get('EmailAddress')});
            System.debug('setToAddress set');
            Messaging.sendEmail(new Messaging.Email[] {message});
            System.debug('Sent email: ' + message);
                
        } catch (Exception e){
            result.success = false;
            result.message = e.getMessage();
        }
    }
}