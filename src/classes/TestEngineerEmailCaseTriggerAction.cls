@isTest
public with sharing class TestEngineerEmailCaseTriggerAction {
	private static final Boolean IS_TRIGGER_TEST = false;
	
	private static void setup() {
		TestCaseEngineerEmail.IS_TRIGGER_TEST = IS_TRIGGER_TEST;
	}
	
	static testMethod void shouldNotEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsNotAnEmailStatus() {
		setup();
		TestCaseEngineerEmail.shouldNotEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsNotAnEmailStatus();
	}
	
	static testMethod void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsPendingVendorResponse() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsPendingVendorResponse();
	}
	
	static testMethod void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsPendingCloudreachResponse() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsPendingCloudreachResponse();
	}
	
	static testMethod void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsAssigning() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsAssigning();
	}
	
	static testMethod void shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsClosed() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnInsertGivenEngineerIsAssignedAndStatusIsClosed();
	}
	
	static testMethod void shouldNotEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsNotAnEmailStatus() {
		setup();
		TestCaseEngineerEmail.shouldNotEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsNotAnEmailStatus();
	}
	
	static testMethod void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsPendingVendorResponse() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsPendingVendorResponse();
	}
	
	static testMethod void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsPendingCloudreachResponse() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsPendingCloudreachResponse();
	}
	
	static testMethod void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsAssigning() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsAssigning();
	}
	
	static testMethod void shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsClosed() {
		setup();
		TestCaseEngineerEmail.shouldEmailEngineerOnUpdateGivenEngineerIsAssignedAndStatusIsClosed();
	}
}