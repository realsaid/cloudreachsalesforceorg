public with sharing class TestDataSet {
    private static final String URI = 'testing@sandbox.sfdc.lab.cloudreach.co.uk';

    public static Case getCase() {
        Case caseObj = new Case();
        return caseObj;
    }

    public static Case getCase(Entitlement entitlement) {
        Case caseObj = new Case();
        caseObj.entitlement = entitlement;
        return caseObj;
    }

    public static Case getCase(BusinessHours businessHours) {
        Case caseObj = new Case();
        caseObj.BusinessHours = businessHours;
        return caseObj;
    }

    public static BusinessHours getBusinessHours(SMTime startTime, SMTime endTime) {
        BusinessHours businessHours = new BusinessHours();
        businessHours.SundayStartTime = startTime.toSFObject();
        businessHours.SundayEndTime = endTime.toSFObject();
        businessHours.MondayStartTime = startTime.toSFObject();
        businessHours.MondayEndTime = endTime.toSFObject();
        businessHours.TuesdayStartTime = startTime.toSFObject();
        businessHours.TuesdayEndTime = endTime.toSFObject();
        businessHours.WednesdayStartTime = startTime.toSFObject();
        businessHours.WednesdayEndTime = endTime.toSFObject();
        businessHours.ThursdayStartTime = startTime.toSFObject();
        businessHours.ThursdayEndTime = endTime.toSFObject();
        businessHours.FridayStartTime = startTime.toSFObject();
        businessHours.FridayEndTime = endTime.toSFObject();
        businessHours.SaturdayStartTime = startTime.toSFObject();
        businessHours.SaturdayEndTime = endTime.toSFObject();
        return businessHours;
    }

    public static OpportunityLineItem getOpportunityLineItem(String productFamily,
            ID opportunityID) {
        Random random = new Random();

        PriceBookEntry entry = new PriceBookEntry();
        Product2 product = new Product2();
        product.Name = random.getString();
        product.Family = productFamily;
        insert product;
        entry.Product2ID = product.ID;
        entry.UnitPrice = random.getDecimal();
        entry.PriceBook2ID = [SELECT ID FROM PriceBook2 WHERE IsStandard = true limit 1].ID;
        entry.IsActive = true;
        insert entry;

        OpportunityLineItem lineItem = random.getOpportunityLineItem();
        lineItem.OpportunityID = opportunityID;
        lineItem.PriceBookEntryID = entry.ID;
        return lineItem;
    }

    public static OpportunityLineItem getOpportunityLineItem() {
        return getOpportunityLineItem(new Random().getString(), null);
    }

    public static Opportunity getOpportunity() {
        
        list<Territory> terrList = [select id from territory where name = 'UK & Ireland' limit 1];
        id terId = terrList[0].id;
        Account account;
        Opportunity opportunity;
        system.runas(getTestUser()){
            account = getAccount();
        	insert account;
            opportunity = new Opportunity();
            opportunity.AccountID = account.ID;
            opportunity.Name = URI;
            opportunity.StageName = 'Prospect';
            opportunity.CloseDate = Date.today();
            opportunity.Order_Form__c = 'https://docs.google.com/a/cloudreach.co.uk/';
            opportunity.Reasons_For_Loosing__c = 'blah';
            opportunity.territoryid = terId;
        }
        return opportunity;
    }

    public static ServiceContract getServiceContract() {
        ServiceContract contract = new Random().getServiceContract();
        contract.True_Up_Type__c = 'blah';
        contract.PriceBook2ID = [SELECT ID FROM PriceBook2 WHERE IsStandard = true limit 1].ID;
        return contract;
    }

    public static ContractLineItem getContractLineItem(ServiceContract con) {
        Random random = new Random();

        PriceBookEntry entry = new PriceBookEntry();
        Product2 product = new Product2();
        product.Name = random.getString();
        insert product;
        entry.Product2ID = product.ID;
        entry.UnitPrice = random.getDecimal();
        entry.PriceBook2ID = con.PriceBook2ID;
        entry.IsActive = true;
        entry.CurrencyIsoCode = con.CurrencyIsoCode;
        insert entry;

        ContractLineItem lineItem = random.getContractLineItem();
        lineItem.ServiceContractID = con.ID;
        lineItem.PriceBookEntryID = entry.ID;
        return lineItem;
    }

    public static Campaign getCampaign() {
        Campaign campaign = new Campaign();
        return campaign;
    }

    static testMethod void getActiveTerritroyUser() {
    	//List <UserTerritory> UserTerritories = [Select userId from UserTerritory where isActive= true];
        //List <Id> UserIds;
        //For (UserTerritory ut:UserTerritories){UserIds.add(ut.userId);}
        //List <User> users = [Select Id, Name, ProfileID from User where Id in :UserIds and isActive= true];
        List <User> users = [Select Id, Name, ProfileID from User where Name = 'Andre Azevedo'];
        User u0 = users[0];
        
        System.assertEquals(u0.id, users[0].id);
    }
    static testMethod void testAccountTerritory () {
        Account account;
        system.runas(getTestUser()){
         account = getAccount();
         insert account;}
        Account AssertAccount = [Select user_s_territory__c from Account where id = :account.Id];
        System.assertNotEquals('',AssertAccount.user_s_territory__c );
        
    }
    public static User getTestUser() {
        List <User> users = [Select Id, Name, ProfileID from User where Name = 'Andre Azevedo'];
        return users[0];
    }
    public static Account getAccount() {
        User u0 = getTestUser();
		Account account;
		system.runas(u0){
            account = new Account();
        }
        Random random = new Random();
        account.Name = URI+'/'+random.getString();
        account.sales_territory__c = 'UK & Ireland';
        return account;
    }

    public static Boolean insertOpportunityCloneSettings() {
        delete [SELECT ID FROM Opportunity_Clone_Fieldset__c];
        Opportunity_Clone_Fieldset__c[] settings = new Opportunity_Clone_Fieldset__c[]{};
        Schema.SObjectField[] fields = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().values();
        String[] fieldsToExclude = opportunityCloneSettingsExceptions();
        for (Schema.SObjectField field : fields) {
            Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
            if (!new SMString(fieldDescribe.getName()).isInList(fieldsToExclude) &&
                    fieldDescribe.isCreateable()) {
                String fieldName = fieldDescribe.getName();
                String fieldLabel;
                fieldLabel = fieldName.length() >= 39 ? fieldName.substring(0, 38) : fieldName;
                settings.add(new Opportunity_Clone_Fieldset__c(Field_Name__c=fieldName, Name=fieldLabel));
            }
        }
        insert settings;
        return true;
    }

    private static String[] opportunityCloneSettingsExceptions() {
        String[] fieldsToExclude = new String[]{};
        fieldsToExclude.add('Primary_Service_Contract__c');
        fieldsToExclude.add('ForecastCategoryName');
        return fieldsToExclude;
    }
    
    public static boolean insertOpportunityAttachment(ID opportunityID) {
        Attachment attachment = new Attachment(Name='Order Form',body=blob.valueof('b'),parentId=opportunityID);
        insert attachment;
        return true;
    }
    
    public static boolean insertOpportunitySignedAgreement(ID opportunityID) {
    	echosign_dev1__SIGN_Agreement__c Agreement = new echosign_dev1__SIGN_Agreement__c();
        Agreement.echosign_dev1__Status__c = 'Signed';
        Agreement.Name = 'Order Form';
        Agreement.echosign_dev1__Opportunity__c = opportunityID;
        
        insert Agreement;
        return true;
    }
}