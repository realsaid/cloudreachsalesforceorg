public with sharing class SMDateTime {
	private final DateTime theDateTime;
	
	public SMDateTime(final SMDate theDate, final SMTime theTime) {
		theDateTime = DateTime.newInstance(theDate.toSFObject(), theTime.toSFObject());
	}
	
	public SMDateTime(final DateTime theDateTime) {
		this.theDateTime = theDateTime;	
	}
	
	public Long getMilliseconds() {
		return theDateTime.getTime();
	}
	
	public Integer getDays() {
		return Date.newInstance(1, 1, 1970).daysBetween(theDateTime.date());
	}
	
	public SMTime getTime() {
		return new SMTime(theDateTime.time());
	}
	
	public DayOfWeek getDayOfWeek() {
		return new SMDate(theDateTime.date()).getDayOfWeek();
	}
	
	public Long millisecondsBetween(SMDateTime otherDateTime) {
		Long milliseconds = 0;
		if (otherDateTime != null) {
			milliseconds = otherDateTime.getMilliseconds() - getMilliseconds();
		}
		return milliseconds;
	}
	
	public Integer minutesBetween(SMDateTime otherDateTime) {
		Integer minutesBetween = 0;
		if (otherDateTime != null) {
			minutesBetween = (millisecondsBetween(otherDateTime) / 60000.0).intValue();
		}
		return minutesBetween;
	}
	
	public Decimal hoursBetween(SMDateTime otherDateTime)  {
		Decimal hoursBetween = 0.0;
		if (otherDateTime != null) {
			hoursBetween = millisecondsBetween(otherDateTime) / 3600000.0;
		}
		return hoursBetween;
	}
	
	public Integer daysBetween(SMDateTime otherDateTime) {
		return otherDateTime.getDays() - getDays();
	}	
}