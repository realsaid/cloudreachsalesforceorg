public with sharing class TriggerException extends Exception{
	public static final String METHOD_NOT_IMPLEMENTED = 'Method Not Implemented.  '; 
}