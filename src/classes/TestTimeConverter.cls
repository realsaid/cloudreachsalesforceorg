@isTest
public with sharing class TestTimeConverter {
	private static final Integer DECIMAL_PLACES = 4;
	private static final TimeConverter converter = new TimeConverter(DECIMAL_PLACES);
	
	private static Decimal scale(Decimal dec) {
		return dec.setScale(converter.getDecimalPlaces());
	}
	
	static testMethod void shouldConvertMinutesIntoDecimalHours() {
		Integer minutes = 1234;
		
		Decimal hours = new TimeConverter().minutesToHours(minutes);
		
		System.assertEquals(scale(1234 / 60.0), scale(hours));
	}
	
	static testMethod void shouldConvertSecondsIntoDecimalHours() {
		Integer seconds = 1234;
		
		Decimal hours = new TimeConverter().secondsToHours(seconds);
		
		System.assertEquals(scale(1234 / 3600.0), scale(hours));
	}
	
	static testMethod void shouldConvertMillisecondsIntoDecimalHours() {
		Integer milliseconds = 1234;
		
		Decimal hours = new TimeConverter().millisecondsToHours(milliseconds);
		
		System.assertEquals(scale(1234 / 3600000.0), scale(hours));
	}
}