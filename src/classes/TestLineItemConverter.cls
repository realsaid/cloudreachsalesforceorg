@isTest(SeeAllData=true)
public with sharing class TestLineItemConverter {
	static testMethod void shouldConvert1OpportunityLineItemTo1ContractLineItem() {
        system.runas(TestDataSet.getTestUser()){       
            Opportunity opp = TestDataSet.getOpportunity();
            insert opp;
            OpportunityLineItem oppLineItem = TestDataSet.getOpportunityLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES, opp.ID);
            OpportunityLineItem[] oppLineItems = new OpportunityLineItem[]{oppLineItem}; 
            insert oppLineItems;
            LineItemConverter converter = new LineItemConverter();
            
            ContractLineItem[] conLineItems = converter.convert(oppLineItems);
            
            System.assertEquals(1, conLineItems.size());
        }
	}
	
	static testMethod void shouldConvertOpportunityLineItemFieldsToContractLineItemFields() {
        system.runas(TestDataSet.getTestUser()){       
            Opportunity opp = TestDataSet.getOpportunity();
            insert opp;
            OpportunityLineItem oppLineItem = TestDataSet.getOpportunityLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES, opp.ID);
            OpportunityLineItem[] oppLineItems = new OpportunityLineItem[]{oppLineItem}; 
            insert oppLineItems;
            LineItemConverter converter = new LineItemConverter();
            
            ContractLineItem[] conLineItems = converter.convert(oppLineItems);
        
            Map<String, String> fieldMap = LineItemConverter.FIELD_MAP;
            for (Integer i = 0; i < oppLineItems.size(); ++i) {
                for (String oppLineItemField : fieldMap.keySet()) {
                   System.assertEquals(oppLineItems[i].get(oppLineItemField), conLineItems[i].get(fieldMap.get(oppLineItemField)));
                }
            }
        }
    }
}