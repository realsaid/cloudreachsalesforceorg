@isTest
public with sharing class TestTimePeriod {
	private static SMTime START_TIME = new SMTime(Time.newInstance(4, 3, 2, 1));
	private static SMTime END_TIME = new SMTime(Time.newInstance(10, 9, 8, 7));
	
	private static final Integer DECIMAL_PLACES = TimePeriod.DECIMAL_PLACES;
	
	static testMethod void shouldReturnStartTime() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		
		SMTime startTime = period.getStart();
		
		System.assertEquals(START_TIME, startTime);
	}
	
	static testMethod void shouldReturnEndTime() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		
		SMTime endTime = period.getEnd();
		
		System.assertEquals(END_TIME, endTime);
	}
	
	static testMethod void shouldReturnDecimalHoursInPeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		
		Decimal periodInHours = period.getHours();
		
		System.assertEquals(START_TIME.hoursBetween(END_TIME), periodInHours);
	}
	
	static testMethod void shouldIndicateThatPeriodIncludesGivenTime() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = END_TIME.addSeconds(-1);
		
		Boolean doesInclude = period.doesInclude(theTime);
		
		System.assertEquals(true, doesInclude);
	}
	
	static testMethod void shouldIndicateThatPeriodDoesNotIncludeGivenTime() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = END_TIME.addSeconds(1);
		
		Boolean doesInclude = period.doesInclude(theTime);
		
		System.assertEquals(false, doesInclude);
	}
	
	static testMethod void shouldReturnCorrectPositiveNumberOfHoursInPeriodBeforeTimeWhenTimeOccursWithinPeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = START_TIME.addHours(+1);
		
		Decimal hours = period.getHoursBefore(theTime);
		
		System.assertEquals(1.0.setScale(DECIMAL_PLACES), hours);
	}
	
	static testMethod void shouldReturnZeroHoursBeforeTimeWhenTimeOccursBeforePeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = START_TIME.addHours(-1);
		
		Decimal hours = period.getHoursBefore(theTime);
		
		System.assertEquals(0.0.setScale(DECIMAL_PLACES), hours);
	}
	
	static testMethod void shouldReturnAllHoursInPeriodBeforeTimeWhenTimeOccursAfterPeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = END_TIME.addHours(+1);
		
		Decimal hours = period.getHoursBefore(theTime);
		
		System.assertEquals(period.getHours(), hours);
	}
	
	static testMethod void shouldReturnCorrectPositiveNumberOfHoursInPeriodAfterTimeWhenTimeOccursWithinPeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = END_TIME.addHours(-1);
		
		Decimal hours = period.getHoursAfter(theTime);
		
		System.assertEquals(1.0.setScale(DECIMAL_PLACES), hours);
	}
	
	static testMethod void shouldReturnZeroHoursAfterTimeWhenTimeOccursAfterPeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = END_TIME.addHours(+1);
		
		Decimal hours = period.getHoursAfter(theTime);
		
		System.assertEquals(0.0.setScale(DECIMAL_PLACES), hours);
	}
	
	static testMethod void shouldReturnAllHoursInPeriodAfterTimeWhenTimeOccursBeforePeriod() {
		TimePeriod period = new TimePeriod(START_TIME, END_TIME);
		SMTime theTime = START_TIME.addHours(-1);
		
		Decimal hours = period.getHoursAfter(theTime);
		
		System.assertEquals(period.getHours(), hours);
	}
}