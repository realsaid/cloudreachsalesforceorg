@isTest(seeAllData=true)
public with sharing class TestOpportunityContractAssignment {
	private static final String STAGE_NAME_NOT_WON = 'Any Stage Name That Is Not Won';
	
	static testMethod void Trigger_shouldCreateContractForOpportunityGivenOpportunityHasManagedServicesProductsAndHasNoPrimaryContractAndIsWon() {
		system.runas(TestDataSet.getTestUser()){
            Opportunity opp = new OpportunityBuilder().withStageName(STAGE_NAME_NOT_WON).withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).build();
            TestDataSet.insertOpportunitySignedAgreement(opp.ID);      
            Test.startTest();
            
            opp.Apporved__c = true;
            opp.StageName = SMOpportunity.STAGE_NAME_CLOSED_WON;
            
            update opp;
            Test.stopTest();
            
            opp = [SELECT Primary_Service_Contract__c FROM Opportunity WHERE ID = :opp.ID];
            System.assertNotEquals(null, opp.Primary_Service_Contract__c);
        }
	}
	
	static testMethod void TriggerAction_shouldCreateContractForOpportunityGivenOpportunityHasManagedServicesProductsAndHasNoPrimaryContractAndIsWon() {
		system.runas(TestDataSet.getTestUser()){
            Opportunity oldOpportunityState = new OpportunityBuilder().withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).withStageName(STAGE_NAME_NOT_WON).build();
            Opportunity newOpportunityState = new OpportunityBuilder().withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).withStageName(SMOpportunity.STAGE_NAME_CLOSED_WON).build();
            newOpportunityState.Apporved__c = TRUE;
            TestDataSet.insertOpportunitySignedAgreement(newOpportunityState.Id);
            OpportunityCloseTriggerAction action = new OpportunityCloseTriggerAction(new Opportunity[]{oldOpportunityState}, new Opportunity[]{newOpportunityState});
            
            Test.startTest(); 
            action.onBeforeUpdate();
            Test.stopTest();
            
            System.assertNotEquals(null, newOpportunityState.Primary_Service_Contract__c);
        }
	}
	
	static testMethod void Trigger_shouldNotAssignContractToOpportunityGivenOpportunityHasManagedServicesProductsAndHasNoPrimaryContractAndIsNotWon() {
		system.runas(TestDataSet.getTestUser()){
            Opportunity opp = new OpportunityBuilder().withStageName(STAGE_NAME_NOT_WON).withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).build();
		 
            Test.startTest();
            update opp;
            Test.stopTest();
            
            opp = [SELECT Primary_Service_Contract__c FROM Opportunity WHERE ID = :opp.ID];
            System.assertEquals(null, opp.Primary_Service_Contract__c);
        }
	}
	
	static testMethod void TriggerAction_shouldNotAssignContractToOpportunityGivenOpportunityHasManagedServicesProductsAndHasNoPrimaryContractAndIsNotWon() {
		system.runas(TestDataSet.getTestUser()){
            Opportunity oldOpportunityState = new OpportunityBuilder().withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).withStageName(STAGE_NAME_NOT_WON).build();
            Opportunity newOpportunityState = new OpportunityBuilder().withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).withStageName(STAGE_NAME_NOT_WON).build();
            OpportunityCloseTriggerAction action = new OpportunityCloseTriggerAction(new Opportunity[]{oldOpportunityState}, new Opportunity[]{newOpportunityState});
             
            Test.startTest();
            action.onBeforeUpdate();
            Test.stopTest();
            
            System.assertEquals(null, newOpportunityState.Primary_Service_Contract__c);
        }
    }
	
	static testMethod void Trigger_shouldNotAssignContractToOpportunityGivenOpportunityDoesNotHaveManagedServicesProductsAndHasNoPrimaryContractAndIsWon() {
		system.runas(TestDataSet.getTestUser()){
            Opportunity opp = new OpportunityBuilder().withStageName('Suspect').build();
            insert opp;
            TestDataSet.insertOpportunitySignedAgreement(opp.Id);
            opp.Apporved__c = TRUE;
            opp.StageName = SMOpportunity.STAGE_NAME_CLOSED_WON;
            
            Test.startTest();     
			update opp;            
            Test.stopTest();
            
            opp = [SELECT Primary_Service_Contract__c FROM Opportunity WHERE ID = :opp.ID];
            System.assertEquals(null, opp.Primary_Service_Contract__c);
        }
	}
	
	static testMethod void TriggerAction_shouldNotAssignContractToOpportunityGivenOpportunityDoesNotHaveManagedServicesProductsAndHasNoPrimaryContractAndIsWon() {
		system.runas(TestDataSet.getTestUser()){
            Opportunity oldOpportunityState = new OpportunityBuilder().withStageName(STAGE_NAME_NOT_WON).build();
            //Opportunity newOpportunityState = new OpportunityBuilder().withStageName(SMOpportunity.STAGE_NAME_CLOSED_WON).build();
            Opportunity newOpportunityState = new OpportunityBuilder().withStageName(STAGE_NAME_NOT_WON).build();                      
            insert newOpportunityState;
            newOpportunityState.Apporved__c = true;
            TestDataSet.insertOpportunitySignedAgreement(newOpportunityState.Id);
            newOpportunityState.StageName = SMOpportunity.STAGE_NAME_CLOSED_WON;
            OpportunityCloseTriggerAction action = new OpportunityCloseTriggerAction(new Opportunity[]{oldOpportunityState}, new Opportunity[]{newOpportunityState});
             
            Test.startTest();
            action.onBeforeUpdate();
            Test.stopTest();
            
            System.assertEquals(null, newOpportunityState.Primary_Service_Contract__c);
        }
	}
	
	static testMethod void Trigger_shouldNotCreateAndAssignNewContractToOpportunityGivenOpportunityHasManagedServicesProductsAndHasPrimaryContractAndIsWon() {
        ServiceContract originalContract = TestDataSet.getServiceContract();
        insert originalContract;

        Opportunity opp = new OpportunityBuilder().withPrimaryServiceContract(originalContract).withStageName(STAGE_NAME_NOT_WON).withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).build();
        system.runas(TestDataSet.getTestUser()){            
            TestDataSet.insertOpportunitySignedAgreement(opp.Id);
            opp.StageName = SMOpportunity.STAGE_NAME_CLOSED_WON;
            opp.Apporved__c = true;
            
            Test.startTest();
            update opp;
            Test.stopTest();
            
            opp = [SELECT Primary_Service_Contract__c FROM Opportunity WHERE ID = :opp.ID];
            System.assertEquals(originalContract.ID, opp.Primary_Service_Contract__c);
        }
	}
	
	static testMethod void TriggerAction_shouldNotCreateAndAssignNewContractToOpportunityGivenOpportunityHasManagedServicesProductsAndHasPrimaryContractAndIsWon() {
        ServiceContract originalContract = TestDataSet.getServiceContract();
        insert originalContract;
		system.runas(TestDataSet.getTestUser()){
        	Opportunity oldOpportunityState = new OpportunityBuilder().withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).withPrimaryServiceContract(originalContract).withStageName(STAGE_NAME_NOT_WON).build();
            Opportunity newOpportunityState = new OpportunityBuilder().withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES).withPrimaryServiceContract(originalContract).withStageName(SMOpportunity.STAGE_NAME_CLOSED_WON).build();
            newOpportunityState.Apporved__c = true;
            OpportunityCloseTriggerAction action = new OpportunityCloseTriggerAction(new Opportunity[]{oldOpportunityState}, new Opportunity[]{newOpportunityState});
             
            Test.startTest();
            action.onBeforeUpdate();
            Test.stopTest();
            
            System.assertEquals(originalContract.ID, newOpportunityState.Primary_Service_Contract__c);
        }
	}
}