public abstract class MassDeleteSObjectController {
	
	protected ApexPages.StandardSetController controller;
	
	public MassDeleteSObjectController(ApexPages.StandardSetController controller) {
        this.controller = controller;
    }
    
	public PageReference deleteSelected() {
		delete controller.getSelected();
		
		return showListView();
	}
	
	protected abstract PageReference showListView();
}