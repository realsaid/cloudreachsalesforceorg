@isTest
public with sharing class TestDuplicateTaskCleaner {
	static testMethod void shouldClean1EmailsAndLeave1() {
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(2);
        Task[] tasks = new DuplicateTaskFetcher().fetch(1000, Date.today().addDays(-1), Date.today().addDays(1));
        
        new DuplicateTaskCleaner().clean(tasks);
        
        Task[] allTasks = [SELECT Id FROM Task];
        System.assertEquals(1, allTasks.size());
    }
    
	static testMethod void shouldClean9EmailsAndLeave1() {
		TestDuplicateTaskCleaner.insertAluminaEmailTasks(10);
		Task[] tasks = new DuplicateTaskFetcher().fetch(1000, Date.today().addDays(-1), Date.today().addDays(1));
        
        new DuplicateTaskCleaner().clean(tasks);
		
		Task[] allTasks = [SELECT Id FROM Task];
		System.assertEquals(1, allTasks.size());
	}
	
	static testMethod void shouldClean99EmailsAndLeave1() {
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(100);
        Task[] tasks = new DuplicateTaskFetcher().fetch(1000, Date.today().addDays(-1), Date.today().addDays(1));
        
        new DuplicateTaskCleaner().clean(tasks);
        
        Task[] allTasks = [SELECT Id FROM Task];
        System.assertEquals(1, allTasks.size());
    }
    
    static testMethod void shouldClean1EmailAndLeave2() {
    	Task unique = TestDuplicateTaskCleaner.getAluminaEmailTask(TestDuplicateTaskCleaner.getTestContact().Id);
    	unique.Subject += 'a';
    	insert unique;
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(2);
        Task[] tasks = new DuplicateTaskFetcher().fetch(1000, Date.today().addDays(-1), Date.today().addDays(1));
        
        new DuplicateTaskCleaner().clean(tasks);
        
        Task[] allTasks = [SELECT Id FROM Task];
        System.assertEquals(2, allTasks.size());
    }
    
    static testMethod void shouldClean8emailsEmailAndLeave2() {
        Task unique = TestDuplicateTaskCleaner.getAluminaEmailTask(TestDuplicateTaskCleaner.getTestContact().Id);
        unique.Subject += 'a';
        insert unique;
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(9);
        Task[] tasks = new DuplicateTaskFetcher().fetch(1000, Date.today().addDays(-1), Date.today().addDays(1));
        
        new DuplicateTaskCleaner().clean(tasks);
        
        Task[] allTasks = [SELECT Id FROM Task];
        System.assertEquals(2, allTasks.size());
    }
    
    static testMethod void shouldClean98emailsEmailAndLeave2() {
        Task unique = TestDuplicateTaskCleaner.getAluminaEmailTask(TestDuplicateTaskCleaner.getTestContact().Id);
        unique.Subject += 'a';
        insert unique;
        TestDuplicateTaskCleaner.insertAluminaEmailTasks(99);
        Task[] tasks = new DuplicateTaskFetcher().fetch(1000, Date.today().addDays(-1), Date.today().addDays(1));
        
        new DuplicateTaskCleaner().clean(tasks);
        
        Task[] allTasks = [SELECT Id FROM Task];
        System.assertEquals(2, allTasks.size());
    }
    
    public static Contact getTestContact() {
        Contact[] cons = [SELECT Id FROM Contact WHERE Email = 'robin.smith@cloudreach.co.uk'];
        Contact who = null;
        if (!(cons.size() >= 1)) {
            who = new Contact();
            who.FirstName = 'Robin';
            who.LastName = 'Smith';
            who.Email = 'robin.smith@cloudreach.co.uk';
        }  
        else {
            who = cons[0];
        }
        return who;
    }
        
    public static Task getAluminaEmailTask(String whoID) {
        String subject = 'Synced Alumina Email';
        String description = 'From: Robin Smith\nTo: Hello World!\nCC: Barry m8\nBCC: Sure-Shot Sean\n\nHello this is a synced Alumina Email.';
        
        Task task = new Task();
        task.Subject = subject;
        task.Description = description;
        task.WhoId = whoId;
        task.Type = 'Email';
        //task.Alumina__Is_Alumina_Email__c = true;
        task.Priority = 'Normal';
        task.Status = 'Urgent!';
        
        return task;
    }
    
    public static void insertAluminaEmailTasks(Integer n) {
        Task[] tasks = new Task[]{};  
        
        String whoId = getTestContact().Id;
        for (Integer i = 0; i < n; ++i) {
            tasks.add(getAluminaEmailTask(whoId));
        }
        
        insert tasks;
    }
}