@isTest
public with sharing class TestClockUpdateCaseTriggerAction {
	private static Boolean IS_TRIGGER_TEST = false;
	
	private static void setup() {
		TestCaseClockUpdate.IS_TRIGGER_TEST = IS_TRIGGER_TEST;
	}
	
	static testMethod void shouldStopCaseClockOnInsertGivenStatusIsPendingVendor() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockOnInsertGivenStatusIsPendingVendor();
	}
	
	static testMethod void shouldStopCaseClockOnInsertGivenStatusIsPendingCustomer() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockOnInsertGivenStatusIsPendingCustomer();
	}
	
	static testMethod void shouldStopCaseClockOnInsertGivenStatusIsSolutionOffered() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockOnInsertGivenStatusIsSolutionOffered();
	}
	
	static testMethod void shouldStopCaseClockOnInsertGivenStatusIsClosed() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockOnInsertGivenStatusIsClosed();
	}
	
	static testMethod void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsPendingVendor() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsPendingVendor();
	}
	
	static testMethod void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsPendingCustomer() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsPendingCustomer();
	}
	
	static testMethod void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsSolutionOffered() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsSolutionOffered();
	}
	
	static testMethod void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsClosed() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsClosed();
	}
	
	static testMethod void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenCaseIsNotEntitled() {
		setup();
		TestCaseClockUpdate.shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenCaseIsNotEntitled();
	}
	
	static testMethod void shouldStartCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsNotAStopStatus() {
		setup();
		TestCaseClockUpdate.shouldStartCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsNotAStopStatus();
	}
}