global class CloneOpportunityController {
	private static Boolean hasInsertedClone = false;
    private final String oppID;
    
    public CloneOpportunityController(ApexPages.StandardController stdController) {
        oppID = stdController.getRecord().ID;
    }
    
    public PageReference insertClone() {
    	Opportunity clone = new OpportunityCloner().insertClone(oppID);
        hasInsertedClone = true;
        return new PageReference('/' + clone.ID + '/e?retURL=' + clone.ID);
    }
}