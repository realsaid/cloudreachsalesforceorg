public with sharing class SMCase {
	public static String STATUS_ASSIGNING 			= 'Assigning';
	public static String STATUS_PENDING_VENDOR		= 'Pending Vendor Response';
	public static String STATUS_PENDING_CUSTOMER	= 'Pending Customer Response';
	public static String STATUS_PENDING_CLOUDREACH	= 'Pending Cloudreach Response';
	public static String STATUS_SOLUTION_OFFERED 	= 'Solution Offered';
	public static String STATUS_CLOSED	 			= 'Closed';
	
	public static String ENGINEER_NOT_ASSIGNED = 'Not Assigned';
	
	private Case record;
	
	public SMCase() {
		record = new Case();
	}
	
	public SMCase(Case record) {
		this.record = record;
	}
	
	public Case getRecord() {
		return record;
	}
	
	public Id getId() {
		return record.Id;
	}
	
	public Boolean isEntitled() {
		Boolean isEntitled = false;
		if (record != null && record.BusinessHours.SundayStartTime != null) {
			SMBusinessHours hours = new SMBusinessHours(record.businessHours);
			isEntitled = (hours.getHoursForToday().doesIncludeNow()
					     || hours.getHoursForToday().getHours() == 0.0000);
		}
		return isEntitled; 
	}
	
	public Boolean isClockStopped() {
		Boolean isStopped = false;
		if (record != null
			&& record.Clock_Start_Date__c == null) {
			isStopped = true;
		}
		return isStopped;
	}
	
	public void startClock() {
		updateClock();
		if (record != null) {
			if (record.Clock__c == null) {
				record.Clock__c = 0.0000;
			}
			record.Clock_Start_Date__c = DateTime.now();
			record.IsStopped = false;
		}
	}
	
	private void updateClock() {
		if (record != null) {
			if (record.Clock__c == null) {
				record.Clock__c = 0.0000;
			}
			if (record.Clock_Start_Date__c != null && record.BusinessHours.SundayStartTime != null) {
				SMBusinessHours businessHours = new SMBusinessHours(record.businessHours);
				DateTimePeriod periodSinceLastUpdate = new DateTimePeriod(new SMDateTime(record.Clock_Start_Date__c),
														   				  new SMDateTime(DateTime.now()));
				record.Clock__c += businessHours.getTotalBusinessHoursForPeriod(periodSinceLastUpdate);
			}
		}
	}
	
	public void stopClock() {
		if (record != null && record.BusinessHours.SundayStartTime != null) {
			updateClock();
			record.Clock_Start_Date__c = null;
			record.IsStopped = true;
		}
	}
	
	public Decimal getClock() {
		return record.Clock__c;
	}
	
	public void setClock(Decimal clock) {
		record.Clock__c = clock;
	}
	
	public DateTime getClockStartDate() {
		return record.Clock_Start_Date__c;
	}
	
	public void setClockStartDate(DateTime clockStartDate) {
		record.Clock_Start_Date__c = clockStartDate;
	}
	
	public String getStatus() {
		return record.Status;
	}
	
	public void setStatus(String status) {
		record.Status = status;
	}
}