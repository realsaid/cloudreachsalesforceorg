/************************************************************************

	Name:			OpportunityUpdateBatch_Cron.cls (SCHEDULABLE)
	Created Date:	8/12/2015
	Created By:		Charlotte Gibson
	Purpose:		Scheduleable class to call the Batched class OpportunityUpdateBatch
					for every 
					
					Run the following code to schedule this to run at 1am every morning. 

					OpportunityUpdateBatch_Cron job = new OpportunityUpdateBatch_Cron();
					String sch = '0 0 1 * * ?';
					system.schedule('Run_OpportunityUpdateBatch', sch, job);	

************************************************************************/

global class OpportunityUpdateBatch_Cron implements schedulable{

   global void execute(SchedulableContext SC) {
   		
		database.executebatch(new OpportunityUpdateBatch(),200);
		
	}

}