/*
	RUN THIS CODE TO SCHEDULE THIS JOB, ONE RUN FOR HOWEVER MANY COMPANIES HAVE ACCOUNTING CURRENCIES:


	updateExchangeRates_cron p1 = new updateExchangeRates_cron();
	String sch1 = '0 25 1 * * ?';
	system.schedule('Exchange Rate updates 1', sch1, p1);

	updateExchangeRates_cron p2 = new updateExchangeRates_cron();
	String sch2 = '0 30 1 * * ?';
	system.schedule('Exchange Rate updates 2', sch2, p2);

	updateExchangeRates_cron p3 = new updateExchangeRates_cron();
	String sch3 = '0 35 1 * * ?';
	system.schedule('Exchange Rate updates 3', sch3, p3);

	updateExchangeRates_cron p4 = new updateExchangeRates_cron();
	String sch4 = '0 40 1 * * ?';
	system.schedule('Exchange Rate updates 4', sch4, p4);

	updateExchangeRates_cron p5 = new updateExchangeRates_cron();
	String sch5 = '0 45 1 * * ?';
	system.schedule('Exchange Rate updates 5', sch5, p5);

	updateExchangeRates_cron p6 = new updateExchangeRates_cron();
	String sch6 = '0 50 1 * * ?';
	system.schedule('Exchange Rate updates 6', sch6, p6);

	updateExchangeRates_cron p7 = new updateExchangeRates_cron();
	String sch7 = '0 55 1 * * ?';
	system.schedule('Exchange Rate updates 7', sch7, p7);

	updateExchangeRates_cron p8 = new updateExchangeRates_cron();
	String sch8 = '0 00 2 * * ?';
	system.schedule('Exchange Rate updates 8', sch8, p8);

*/

global class updateExchangeRates_cron implements Schedulable {
	global void execute(SchedulableContext sc) {


		//depending on time, update each company's exchange rates:
		c2g__codaCompany__c thisCompany;
		integer currentTime = (system.now().hour() * 100) + system.now().minute();

		system.debug(currentTime);

		//get all companies, to check if they've got accounting currencies
		list<c2g__codaCompany__c> fullCompanyList = [select id
														, name
														, ownerId
														, (select id from c2g__AccountingCurrencies__r)
														 from c2g__codaCompany__c order by name];
		
		//strip down company list depending on whether the company has an accounting currency or not.
		list<c2g__codaCompany__c> companyList = new list<c2g__codaCompany__c>{};
		for(c2g__codaCompany__c c : fullCompanyList){
			system.debug('company: ' + c);
			if(c.c2g__AccountingCurrencies__r != null){
				companyList.add(c);
			}
		}
		//iterate through companies list, up to 20 companies. for each one
		if(test.isrunningTest()){
			thisCompany = companyList.get(0);
		}
		for(Integer i = 0; i < companyList.size(); i++){

			if(currentTime >= 0125 && currentTime < 0130 && i==0){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0130 && currentTime < 0135 && i==1){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0135 && currentTime < 0140 && i==2){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0140 && currentTime < 0145 && i==3){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0145 && currentTime < 0150 && i==4){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0150 && currentTime < 0155 && i==5){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0155 && currentTime < 0200 && i==6){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0200 && currentTime < 0205 && i==7){
				thisCompany = companyList.get(i);
			} /*else if(currentTime >= 0105 && currentTime < 0110 && i==8){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0110 && currentTime < 0115 && i==9){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0115 && currentTime < 0120 && i==10){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0950 && currentTime < 0955 && i=11){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0950 && currentTime < 0955 && i=12){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0950 && currentTime < 0955 && i=13){
				thisCompany = companyList.get(i);
			} else if(currentTime >= 0950 && currentTime < 0955 && i=14){
				thisCompany = companyList.get(i);
			} 
			}*/
		}
		updateExchangeRates_batch b = new updateExchangeRates_batch(thisCompany);
		database.executebatch(b);

	}
}