public class CaseEmailDeleteBatch implements Database.Batchable<SObject> {

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([
                select Id, (select Id from EmailMessages)
               from Case
               where status = 'Closed' and closeddate < 2015-02-28T00:00:00.000Z 
                ]);
    }

    public void execute(Database.BatchableContext bc, List<Case> scope) {
        EmailMessage[] messages = new EmailMessage[] {};
        for (Case c : scope) {
            messages.addAll(c.EmailMessages);
        }
        delete messages;
    }

    public void finish(Database.BatchableContext bc) {
    }
}