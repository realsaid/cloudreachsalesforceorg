public with sharing class UpdateCasesController {
	public static final String CASE_UPDATE_NAME = 'Case Update';
	public static final String CASE_UPDATE_CRON = new CRON(DateTime.now().addMinutes(2)).getCRON();
	
	public void scheduleUpdate() {
		System.schedule(CASE_UPDATE_NAME, CASE_UPDATE_CRON, new CaseUpdateBatch());
	}
}