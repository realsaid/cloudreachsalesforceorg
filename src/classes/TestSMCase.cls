@isTest
public with sharing class TestSMCase {
    private static SMTime now = SMTime.now();

    private static final Integer DECIMAL_PLACES = 4;
    private static final Decimal TOLERANCE = 0.001;
    
    private static SMCase getSMCase(SMTime startTime, SMTime endTime) {
        return new SMCase(TestDataSet.getCase(TestDataSet.getBusinessHours(startTime, endTime)));
    }
    
    static testMethod void shouldIndicateThatCaseIsEntitledWhenBusinessHoursCoverNow() {
        // This test fails if the start of the time period used in the test falls before midnight
        // and the end falls after midnight.
        if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
            SMCase smCase = getSMCase(now.addHours(-1), now.addHours(+1));
            
            Boolean isEntitled = smCase.isEntitled();
            
            System.assertEquals(true, isEntitled);
        }
    }
    
    static testMethod void shouldIndicateThatCaseIsNotEntitledWhenBusinessHoursDoNotCoverTime() {
        // This test fails if the current time is within an hour after the time used in the test,
        // because then the Case is actually entitled
        Decimal diff = new SMTime(Time.newInstance(16, 43, 01, 00)).hoursBetween(SMTime.now());
        if ((diff > 0.0) && (diff < 1.0)) {
            SMTime tm = new SMTime(Time.newInstance(16, 43, 01, 00));
            SMCase smCase = getSMCase(tm.addHours(+1), tm.addHours(+2));
        
            Boolean isEntitled = smCase.isEntitled();
        
            System.assertEquals(false, isEntitled);
        }
    }
    
    static testMethod void shouldIndicateThatCaseIsNotEntitledWhenCaseIsNull() {
        SMCase smCase = new SMCase(null);
            
        Boolean isEntitled = smCase.isEntitled();
        
        System.assertEquals(false, isEntitled);
    }
    
    static testMethod void shouldIndicateThatCaseIsNotEntitledWhenCaseBusinessHoursIsNull() {   
        SMCase smCase = new SMCase();
        
        Boolean isEntitled = smCase.isEntitled();
        
        System.assertEquals(false, isEntitled);
    }
    
    static testMethod void shouldIndicateThatCaseClockIsStoppedWhenClockStartDateIsNull() {
        SMCase smCase = new SMCase();
        smCase.setClockStartDate(null);
        
        Boolean isStopped = smCase.isClockStopped();
        
        System.assertEquals(true, isStopped);
    }
    
    static testMethod void shouldIndicateThatCaseClockIsNotStoppedWhenClockStartDateIsNotNull() {
        SMCase smCase = new SMCase();
        smCase.setClockStartDate(DateTime.now());
        
        Boolean isStopped = smCase.isClockStopped();
        
        System.assertEquals(false, isStopped);
    }
    
    static testMethod void shouldStopAndUpdateClock() {
        if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
            SMCase smCase = getSMCase(now.addHours(-1), now.addHours(+1));
            smCase.setClockStartDate(DateTime.now().addHours(-1));
            smCase.setClock(0.0000);
            
            smCase.stopClock();
            
            System.assertEquals(true, smCase.isClockStopped());
            System.assertEquals(null, smCase.getClockStartDate());
            assertRoughlyEquals(1.0000, smCase.getClock());
        }
    }
    
    static testMethod void shouldStartClock() {
        if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
            SMCase smCase = getSMCase(now.addHours(-1), now.addHours(+1));
            smCase.setClockStartDate(null);
            Decimal currentClock = 1.2345;
            smCase.setClock(currentClock);
            
            smCase.startClock();
            
            System.assertEquals(false, smCase.isClockStopped());
            System.assertNotEquals(null, smCase.getClockStartDate());
            assertRoughlyEquals(currentClock, smCase.getClock());
        }
    }
    
    private static void assertRoughlyEquals(Decimal expected, Decimal actual) {
        System.assert(scale(actual) >= scale(expected - TOLERANCE) && scale(actual) <= scale(expected + TOLERANCE));
    }
    
    private static Decimal scale(Decimal dec) {
        return dec.setScale(DECIMAL_PLACES);
    }
}