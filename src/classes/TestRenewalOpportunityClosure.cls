@isTest(seeAllData=true)
public with sharing class TestRenewalOpportunityClosure {
	private static Date originalEndDate;
	private static final Boolean settingsInserted = TestDataSet.insertOpportunityCloneSettings();
	
	static testMethod void TriggerAction_shouldSetPrimaryContractStartDateToOriginalEndDateWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected() {
		ServiceContract contract = createServiceContract();
		
		contract = TriggerAction_shouldSetPrimaryContactDetailsWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected(contract);
		
		System.assertEquals(originalEndDate, contract.StartDate);
	}
	
	static testMethod void Trigger_shouldSetPrimaryContractStartDateToOriginalEndDateWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected() {
		ServiceContract contract = createServiceContract();
		 
		contract = Trigger_shouldSetPrimaryContractDetailsWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected(contract);
		
		System.assertEquals(originalEndDate, contract.StartDate);
	}
	
	static testMethod void TriggerAction_shouldSetPrimaryContractEndDateToNullWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected() {
		ServiceContract contract = createServiceContract();
		
		contract = TriggerAction_shouldSetPrimaryContactDetailsWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected(contract);
		
		System.assertEquals(null, contract.EndDate);
	}
	
	static testMethod void Trigger_shouldSetPrimaryContractEndDateToNullWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected() {
		ServiceContract contract = createServiceContract();
		 
		contract = Trigger_shouldSetPrimaryContractDetailsWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected(contract);
		
		System.assertEquals(null, contract.EndDate);
	}
	
	private static ServiceContract createServiceContract() {
		Random random = new Random();
		Date startDate = random.getDate();
		ServiceContract contract = new ServiceContractBuilder()
		.withStartDate(startDate)
		.withEndDate(originalEndDate = random.getDate(startDate))
		.withRenewalType(SMServiceContract.RENEWAL_TYPE_RENEWAL_EXPECTED)
		.withTrueUpType(SMServiceContract.TRUE_UP_TYPE_NONE)
		.build();
		insert contract;
		return contract;
	}
	
	private static ServiceContract Trigger_shouldSetPrimaryContractDetailsWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected(ServiceContract contract) {
		Opportunity opp = new OpportunityBuilder()
		.withType(SMOpportunity.TYPE_RENEWAL)
		.withStageName(SMOpportunity.STAGE_NAME_PROPOSAL)
		.withPrimaryServiceContract(contract)
		.withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES)
		.build();
		
        opp.Apporved__c = true;
        TestDataSet.insertOpportunitySignedAgreement(opp.Id); 
        opp.Order_Form_Echosigned__c = true;
		
        Test.startTest();
		update opp;
        opp.StageName = SMOpportunity.STAGE_NAME_CLOSED_WON;
		update opp;
		Test.stopTest();
		
		return new SMOpportunity(opp).getPrimaryServiceContract();
	}
	
	private static ServiceContract TriggerAction_shouldSetPrimaryContactDetailsWhenOpportunityIsClosedGivenContractHasTypeRenewalExpected(ServiceContract contract) {
		Opportunity oldOpportunityState = new OpportunityBuilder()
		.withType(SMOpportunity.TYPE_RENEWAL)
		.withStageName(SMOpportunity.STAGE_NAME_PROPOSAL)
		.withPrimaryServiceContract(contract)
		.withLineItem(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES)
		.build();
		Opportunity newOpportunityState = new SMOpportunity(oldOpportunityState).insertClone();
        
		newOpportunityState.Primary_Service_Contract__c = contract.ID;
        TestDataSet.insertOpportunitySignedAgreement(newOpportunityState.Id);
        newOpportunityState.StageName = SMOpportunity.STAGE_NAME_CLOSED_WON;
		RenewalOpportunityCloseTriggerAction action = new RenewalOpportunityCloseTriggerAction(new Opportunity[]{oldOpportunityState}, new Opportunity[]{newOpportunityState});
		 
		Test.startTest();
		action.onBeforeUpdate();
		Test.stopTest();
		
		return new SMOpportunity(newOpportunityState).getPrimaryServiceContract();
	}
}