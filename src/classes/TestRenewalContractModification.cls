@isTest
public with sharing class TestRenewalContractModification {
	private static RenewalContractTriggerAction action;
	
	private static final String[] REQUIRED_FIELDS = new String[]{'Name', 'CloseDate', 'StageName'};
    
    private static void insertRequiredCloneFieldset() {
        TestDataSet.insertOpportunityCloneSettings();
    }
	
	static testMethod void Trigger_shouldCreateRenewalOpportunityOnServiceContractUpdateGivenServiceContractMeetsRenewalCriteriaOfCRSMI_85() {
		system.runas(TestDataSet.getTestUser()){
            insertRequiredCloneFieldset();
            ServiceContract contract = new ServiceContractBuilder()
            .withRenewalType('')
            .withStartDate(Date.today())
            .withEndDate(null).build();
            insert contract;
            Opportunity originalOpportunity = new OpportunityBuilder()
            .withType('Upsell')
            .withPrimaryServiceContract(contract)
            //.withStageName('Closed Won')
            .withOrderForm('https://docs.google.com/a/cloudreach.co.uk/blahblahblah')
            .build();
            insert originalOpportunity;
            
            originalOpportunity.Apporved__c = true;
            TestDataSet.insertOpportunitySignedAgreement(originalOpportunity.Id);
            originalOpportunity.StageName = 'Closed Won';
            update originalOpportunity;
    
            
            Test.startTest();
            contract.Renewal_Type__c = SMServiceContract.RENEWAL_TYPE_RENEWAL_EXPECTED;
            contract.EndDate = new Random().getDate(Date.today());
            // Done to satisfy validation rule in production.
            contract.True_Up_Type__c = SMServiceContract.TRUE_UP_TYPE_NONE;
            update contract;
            Test.stopTest();
            
            Opportunity[] renewalOpps = [SELECT ID
                                         FROM Opportunity 
                                         WHERE Type = :SMOpportunity.TYPE_RENEWAL
                                         AND Primary_Service_Contract__c = :contract.ID];
            System.assertEquals(1, renewalOpps.size());
        }
	}
	
	static testMethod void TriggerAction_shouldCreateRenewalOpportunityOnServiceContractUpdateGivenServiceContractMeetsRenewalCriteriaOfCRSMI_85() {
		system.runas(TestDataSet.getTestUser()){
            insertRequiredCloneFieldset();
            ServiceContract originalServiceContractState = new ServiceContractBuilder()
            .withRenewalType('')
            .withStartDate(Date.today())
            .withEndDate(null).build();
            insert originalServiceContractState;
            ServiceContract newServiceContractState = originalServiceContractState.clone();
            newServiceContractState.Renewal_Type__c = SMServiceContract.RENEWAL_TYPE_RENEWAL_EXPECTED;
            newServiceContractState.EndDate = new Random().getDate();
            // Done to satisfy validation rule in production.
            newServiceContractState.True_Up_Type__c = SMServiceContract.TRUE_UP_TYPE_NONE;
            insert newServiceContractState;
            Opportunity originalOpportunity = new OpportunityBuilder()
            .withType('Upsell')
            .withPrimaryServiceContract(newServiceContractState)
            .withStageName('Suspect')
            .withOrderForm('https://docs.google.com/a/cloudreach.co.uk/blahblahblah')
            .build();
    
            insert originalOpportunity;
            
            TestDataSet.insertOpportunitySignedAgreement(originalOpportunity.Id);		
            originalOpportunity.Apporved__c = true;
            originalOpportunity.StageName = 'Closed Won';
            update originalOpportunity;
            
            action = new RenewalContractTriggerAction(new ServiceContract[]{originalServiceContractState}, new ServiceContract[]{newServiceContractState});
            
            Test.startTest();
            action.onBeforeUpdate();
            Test.stopTest();
            
            Opportunity[] renewalOpps = [SELECT ID
                                         FROM Opportunity 
                                         WHERE Type = :SMOpportunity.TYPE_RENEWAL
                                         AND Primary_Service_Contract__c = :newServiceContractState.ID];
            System.assertEquals(1, renewalOpps.size());
        }
	}
}