@isTest
public without sharing class TestCaseClockUpdate {
	private static final SMTime START_TIME = new SMTime(Time.newInstance(9, 0, 0, 0));
	private static final SMTime END_TIME = new SMTime(Time.newInstance(17, 0, 0, 0));
	private static final Integer CLOCK_DECIMAL_PLACES = 4;
	
	public static Boolean IS_TRIGGER_TEST;
	
	private static ClockUpdateCaseTriggerAction action;
	private static SMCase theCase;
	
	private static void setup(String status, SMTime startTime, SMTime endTime) {
		theCase = getCase(startTime, endTime);
		theCase.setStatus(status);
		action = new ClockUpdateCaseTriggerAction(new Case[]{theCase.getRecord()});
	}
	
	private static SMCase getCase(SMTime startTime, SMTime endTime) {
		return new SMCase(TestDataSet.getCase(TestDataSet.getBusinessHours(startTime, endTime)));
	}
	
	private static void performUpdateTest() {
		Test.startTest();
		if (IS_TRIGGER_TEST) {
			insert theCase.getRecord();
			update theCase.getRecord();
		}
		else {
			action.onBeforeUpdate();
		}
		Test.stopTest();
	}
	
	private static void performInsertTest() {
		Test.startTest();
		if (IS_TRIGGER_TEST) {
			insert theCase.getRecord();
		}
		else {
			action.onBeforeInsert();
		}
		Test.stopTest();
	}
	
	public static void shouldStopCaseClockOnInsertGivenStatusIsPendingVendor() {
		shouldStopCaseClockOnInsertGivenStatusIs(SMCase.STATUS_PENDING_VENDOR);
	}
	
	public static void shouldStopCaseClockOnInsertGivenStatusIsPendingCustomer() {
		shouldStopCaseClockOnInsertGivenStatusIs(SMCase.STATUS_PENDING_CUSTOMER);
	}
	
	public static void shouldStopCaseClockOnInsertGivenStatusIsSolutionOffered() {
		shouldStopCaseClockOnInsertGivenStatusIs(SMCase.STATUS_SOLUTION_OFFERED);
	}
	
	public static void shouldStopCaseClockOnInsertGivenStatusIsClosed() {
		shouldStopCaseClockOnInsertGivenStatusIs(SMCase.STATUS_CLOSED);
	}
	
	public static void shouldStartCaseClockOnInsertGivenStatusIsNotAStopStatus() {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup(SMCase.STATUS_ASSIGNING, now.addHours(-1), now.addHours(+1));
			
			performInsertTest();
			
			System.assertEquals(false, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) == scale(0.000));
		}
		else {
			System.assert(true);
		}
	}
	
	private static void shouldStopCaseClockOnInsertGivenStatusIs(String status) {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup(status, now.addHours(-1), now.addHours(+1));
			
			performInsertTest();
			
			System.assertEquals(true, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) == scale(0.000));
		}
		else {
			System.assert(true);
		}
	}
	
	private static void shouldStopCaseClockOnInsertGivenCaseIsNotEntitled(String status) {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup(status, now.addHours(+1), now.addHours(+2));
			
			performInsertTest();
			
			System.assertEquals(true, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) == scale(0.000));
		}
		else {
			System.assert(true);
		}
	}
	
	private static void shouldStartCaseClockOnInsertGivenStatusIsNotAStopStatusAndCaseIsEntitled() {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup('Not A Stop Status', now.addHours(-1), now.addHours(+1));
			
			performInsertTest();
			
			System.assertEquals(false, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) == scale(0.000));
		}
		else {
			System.assert(true);
		}
	}
	
	public static void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsPendingVendor() {
		shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIs(SMCase.STATUS_PENDING_VENDOR);
	}
	
	public static void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsPendingCustomer() {
		shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIs(SMCase.STATUS_PENDING_CUSTOMER);
	}
	
	public static void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsSolutionOffered() {
		shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIs(SMCase.STATUS_SOLUTION_OFFERED);
	}
	
	public static void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsClosed() {
		shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIs(SMCase.STATUS_CLOSED);
	}
	
	private static void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenStatusIs(String status) {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup(status, now.addHours(-1), now.addHours(+1));
			Datetime dtNow = Datetime.now();
			Datetime clockStartDate = dtNow.addHours(-2);
			Decimal currentClock = 123.4;
			theCase.setClock(currentClock);
			theCase.setClockStartDate(clockStartDate);
			
			performUpdateTest();
			
			System.assertEquals(true, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) >= (scale(currentClock + 1.000)));
			System.assertEquals(true, scale(theCase.getClock()) <= (scale(currentClock + 1.100)));
		}
		else {
			System.assert(true);
		}
	}
	
	public static void shouldStopCaseClockAndAddEntitledTimeOnUpdateGivenCaseIsNotEntitled() {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup('Not A Stop Status', now.addHours(-2), now.addHours(-1));
			Datetime dtNow = Datetime.now();
			Datetime clockStartDate = dtNow.addHours(-2);
			Decimal currentClock = 123.4;
			theCase.setClock(currentClock);
			theCase.setClockStartDate(clockStartDate);
			
			performUpdateTest();
			
			System.assertEquals(true, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) >= (scale(currentClock + 1.000)));
			System.assertEquals(true, scale(theCase.getClock()) <= (scale(currentClock + 1.100)));
		}
		else {
			System.assert(true);
		}
	}
	
	public static void shouldStartCaseClockAndAddEntitledTimeOnUpdateGivenStatusIsNotAStopStatus() {
		if (new SMTime(Time.newInstance(23, 59, 59, 59)).hoursBetween(SMTime.now()) > 2.0) {
			SMTime now = SMTime.now();
			setup('Not A Stop Status', now.addHours(-1), now.addHours(+1));
			Datetime dtNow = Datetime.now();
			Datetime clockStartDate = dtNow.addHours(-2);
			Decimal currentClock = 123.4;
			theCase.setClock(currentClock);
			theCase.setClockStartDate(clockStartDate);
			
			performUpdateTest();
			
			System.assertEquals(false, theCase.isClockStopped());
			System.assertEquals(true, scale(theCase.getClock()) >= (scale(currentClock + 1.000)));
			System.assertEquals(true, scale(theCase.getClock()) <= (scale(currentClock + 1.100)));
		}
		else {
			System.assert(true);
		}
	}
	
	private static Decimal scale(Decimal dec) {
		return dec.setScale(CLOCK_DECIMAL_PLACES);
	}
}