public with sharing class Log {

	private static final Integer MAX_COMPONENT_LENGTH = 255;

	private static final Integer MAX_MSG_LENGTH = 32000;

	private static final Integer MAX_STACKTRACE_LENGTH = 32000;

	private final Log__c record;

	public Log() {
		this.record = new Log__c();
	}

	public Log withComponent(String comp) {
		record.Component__c = cropField(comp, MAX_COMPONENT_LENGTH);
		return this;
	}

	public Log withMessage(String msg) {
		record.Message__c = cropField(msg, MAX_MSG_LENGTH);
		return this;
	}

	public Log withStackTrace(String trace) {
		record.Trace__c = cropField(trace, MAX_STACKTRACE_LENGTH);
		return this;
	}

	public void save() {
		record.Created_Datetime__c = DateTime.now();

		insert record;
	}

	private String cropField(String fieldText, Integer maxLength) {
		if (fieldText.length() > maxLength) {
			return fieldText.substring(0, maxLength);
		}

		return fieldText;
	}

}