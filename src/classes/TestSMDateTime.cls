public with sharing class TestSMDateTime {
	private static final DateTime NOW = DateTime.now();
	
	static testMethod void shouldReturnCorrectMillisecondsSince01011970() {
		DateTime originalDateTime = NOW;
		SMDateTime smDateTime = new SMDateTime(originalDateTime);
		
		Long milliseconds = smDateTime.getMilliseconds();
		
		System.assertEquals(originalDateTime.getTime(), milliseconds);
	}
	
	static testMethod void shouldReturnCorrectNumberOfDaysSince01011970() {
		DateTime originalDateTime = NOW;
		SMDateTime smDateTime = new SMDateTime(originalDateTime);
		
		Integer days = smDateTime.getDays();
		
		System.assertEquals(Date.newInstance(1, 1, 1970).daysBetween(originalDateTime.date()), days);
	}
	
	static testMethod void shouldReturnCorrectTime() {
		SMTime originalTime = new SMTime(Time.newInstance(0, 0, 0, 0));
		SMDateTime smDateTime = new SMDateTime(new SMDate(Date.today()), originalTime);
		
		SMTime smTime = smDateTime.getTime();
		
		System.assertEquals(true, originalTime.equals(smTime));
	}
	
	static testMethod void shouldReturnCorrectDayOfWeek() {
		SMDateTime smDateTime = new SMDateTime(new SMDate(Date.newInstance(1987, 6, 24)),
											   new SMTime(Time.newInstance(12, 30, 30, 50)));
											   
		DayOfWeek day = smDateTime.getDayOfWeek();
		
		System.assertEquals(DayOfWeek.WEDNESDAY, day);
	}
	
	static testMethod void shouldReturnCorrectPositiveMillisecondsBetween() {
		shouldReturnMillisecondsBetweenForOffset(60);
	}

	static testMethod void shouldReturnCorrectNegativeMillisecondsBetween() {
        shouldReturnMillisecondsBetweenForOffset(-60);
    }
    
    private static void shouldReturnMillisecondsBetweenForOffset(Integer offsetInSeconds) {
    	DateTime dateTime1 = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
    	SMDateTime smDateTime1 = new SMDateTIme(dateTime1);
		DateTime dateTime2 = dateTime1.addSeconds(offsetInSeconds);
		SMDateTime smDateTime2 = new SMDateTIme(dateTime2);

		Long timeBetween = smDateTime1.millisecondsBetween(smDateTime2);

		System.assertEquals(offsetInSeconds * 1000, timeBetween);
    }
	
	static testMethod void shouldReturnCorrectPositiveMinutesBetween() {
		shouldReturnMinutesBetweenForOffset(60);
	}

	static testMethod void shouldReturnCorrectNegativeMinutesBetween() {
        shouldReturnMinutesBetweenForOffset(-60);
    }
    
    private static void shouldReturnMinutesBetweenForOffset(Integer offsetInMinutes) {
    	DateTime dateTime1 = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
    	SMDateTime smDateTime1 = new SMDateTIme(dateTime1);
		DateTime dateTime2 = dateTime1.addMinutes(offsetInMinutes);
		SMDateTime smDateTime2 = new SMDateTIme(dateTime2);

		Integer timeBetween = smDateTime1.minutesBetween(smDateTime2);

		System.assertEquals(offsetInMinutes, timeBetween);
    }
    
    static testMethod void shouldReturnCorrectPositiveHoursBetween() {
		shouldReturnHoursBetweenForOffset(90);
	}

	static testMethod void shouldReturnCorrectNegativeHoursBetween() {
        shouldReturnHoursBetweenForOffset(-90);
    }
    
    private static void shouldReturnHoursBetweenForOffset(Integer offsetInMinutes) {
        DateTime dateTime1 = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        SMDateTime smDateTime1 = new SMDateTIme(dateTime1);
		DateTime dateTime2 = dateTime1.addMinutes(offsetInMinutes);
		SMDateTime smDateTime2 = new SMDateTIme(dateTime2);

		Decimal hoursBetween = smDateTime1.hoursBetween(smDateTime2);

		System.assertEquals(offsetInMinutes / 60.0, hoursBetween);
    }
    
    static testMethod void shouldReturnCorrectPositiveDaysBetween() {
		shouldReturnDaysBetweenForOffset(90);
	}

	static testMethod void shouldReturnCorrectNegativeDaysBetween() {
        shouldReturnDaysBetweenForOffset(-90);
    }
    
    private static void shouldReturnDaysBetweenForOffset(Integer offsetInDays) {
        DateTime dateTime1 = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        SMDateTime smDateTime1 = new SMDateTIme(dateTime1);
		DateTime dateTime2 = DateTime.newInstance(dateTime1.date().addDays(offsetInDays),
												  dateTime1.time());
		SMDateTime smDateTime2 = new SMDateTIme(dateTime2);

		Decimal hoursBetween = smDateTime1.daysBetween(smDateTime2);

		System.assertEquals(offsetInDays, hoursBetween);
    }
}