public with sharing class OpportunityCloseTriggerAction extends TriggerAction {
	private final Opportunity[] newOpportunities;
	private final Opportunity[] oldOpportunities;
	
	private static final String[] WIN_STAGE_NAMES = new String[] {'Closed Won'};
	
	public OpportunityCloseTriggerAction(final Opportunity[] oldOpportunities, final Opportunity[] newOpportunities) {
		this.oldOpportunities = oldOpportunities;
		this.newOpportunities = newOpportunities;
	}
	
	public override void onBeforeUpdate() {
		if (shouldPerformAction()) {
			performAction(newOpportunities[0]);
		}
	}
	
	private Boolean shouldPerformAction() {
		Boolean shouldPerformAction = false;
		if (newOpportunities.size() == 1 &&
			oldOpportunities.size() == 1 &&
			isNotRenewalOpportunity(newOpportunities[0]) &&
			hasNoPrimaryContract(newOpportunities[0]) &&
			isNewlyWon(oldOpportunities[0], newOpportunities[0]) &&
			isManagedServicesOpportunity(newOpportunities[0])) {
			shouldPerformAction = true;
		}
		return shouldPerformAction;
	}
	
	private Boolean isNotRenewalOpportunity(Opportunity opp) {
		return opp.Type != SMOpportunity.TYPE_RENEWAL;
	}
	
	private Boolean hasNoPrimaryContract(Opportunity opp) {
		return opp.Primary_Service_Contract__c == null;
	}
	
	private Boolean isNewlyWon(Opportunity oldOpportunity, Opportunity newOpportunity) {
		return !(new SMString(oldOpportunity.StageName).isInList(WIN_STAGE_NAMES))
			   && (new SMString(newOpportunity.StageName).isInList(WIN_STAGE_NAMES));
	}
	
	private Boolean isManagedServicesOpportunity(Opportunity opportunity) {
		return new SMOpportunity(opportunity).hasProductsInFamily(SMOpportunity.PRODUCT_FAMILY_MANAGED_SERVICES);
	}
	
	private void performAction(Opportunity opportunity) {
		ServiceContract contract = new ServiceContract();
		contract.Name = opportunity.Name;
		contract.Description = getProductCodesString(opportunity);
		contract.AccountId = opportunity.AccountId;
		contract.ApprovalStatus = SMServiceContract.APPROVAL_STATUS_DRAFT;
		contract.True_up_Type__c = SMServiceContract.TRUE_UP_TYPE_NONE;
		insert contract;
		opportunity.Primary_Service_Contract__c = contract.Id;
		
		new LineItemSyncer().syncLineItemsFromOppToServiceContract(opportunity.Id,
		                                                           opportunity.Primary_Service_Contract__c);
	}
	
	private String getProductCodesString(Opportunity opp) {
		String str = '';
		OpportunityLineItem[] items = new SMOpportunity(opp).getLineItems();
		
		for (OpportunityLineItem item : items) {
			str += new SMOpportunityLineItem(item).getProductCode() + '\n';
		}
		
		return str;
	}
}