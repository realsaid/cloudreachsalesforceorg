public with sharing class SMServiceContract {
    public static final String RENEWAL_TYPE_RENEWAL_EXPECTED = 'Renewal Expected';
    public static final String APPROVAL_STATUS_DRAFT = 'Draft';
    public static final String TRUE_UP_TYPE_NONE = 'No True-Up';
    
    private ServiceContract contract;
    
    public SMServiceContract(ServiceContract contract) {
        this.contract = contract;   
    }
    
    public Opportunity[] getRelatedOpportunities() {
        return getRelatedOpportunities('WHERE Primary_Service_Contract__c = \'' + contract.ID + '\'');
    }
    
    private Opportunity[] getRelatedOpportunities(String whereClause) {
        String query = new SelectAll('Opportunity').getSOQL() + ' ' + whereClause;
        Opportunity[] opps = new Opportunity[]{};
        try {
            System.Debug('query: ' + query);
            opps = Database.query(query);
        }
        catch (Exception e) {
            System.Debug('Exception caught in SMServiceContract.getRelatedOpportunities: ' + e.getMessage());
        }
        return opps;
    }
    
    public Opportunity getMostRecentlyClosedRelatedOpportunity() {
        Opportunity opp = null;
        try {
            opp = getRelatedOpportunities('WHERE Primary_Service_Contract__c = \'' + contract.ID + '\' ' + 
                                           'AND StageName = \'' + SMOpportunity.STAGE_NAME_CLOSED_WON + '\' ' + 
                                           'ORDER BY LastModifiedDate DESC')[0];
        }
        catch (Exception e) {
            System.Debug('Exception caught in SMServiceContract.getMostRecentlyClosedOpportunity: ' + e.getMessage());
        }
        return opp;
    }
    
    public void addLineItems(ContractLineItem[] items) {
        ID priceBookID = [SELECT ID FROM PriceBook2 WHERE IsStandard = true limit 1].ID;
        contract.Pricebook2Id = priceBookID;
        update contract;
         
        for (ContractLineItem item : items) {
            item.ServiceContractId = this.contract.Id;
        }
        insert items;
    }
    
    public ContractLineItem[] getLineItems() {
        return getLineItems(' WHERE ServiceContractID = ' + stringQuotes(this.contract.Id)); 
    }
    
    public ContractLineItem[] getLineItems(String whereClause) {
        String query = new SelectAll('ContractLineItem').getSOQL() + whereClause; 
        return Database.query(query);
    }
    
    private String stringQuotes(String theString) {
        return '\'' + theString + '\''; 
    }
}