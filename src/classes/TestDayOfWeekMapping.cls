@isTest
public with sharing class TestDayOfWeekMapping {
	static testMethod void shouldMapOrdinalsToCorrectDayOfWeekValues() {
		shouldMapOrdinalToDayOfWeek(0, DayOfWeek.SUNDAY);
		shouldMapOrdinalToDayOfWeek(1, DayOfWeek.MONDAY);
		shouldMapOrdinalToDayOfWeek(2, DayOfWeek.TUESDAY);
		shouldMapOrdinalToDayOfWeek(3, DayOfWeek.WEDNESDAY);
		shouldMapOrdinalToDayOfWeek(4, DayOfWeek.THURSDAY);
		shouldMapOrdinalToDayOfWeek(5, DayOfWeek.FRIDAY);
		shouldMapOrdinalToDayOfWeek(6, DayOfWeek.SATURDAY);
	}
	
	private static void shouldMapOrdinalToDayOfWeek(Integer ordinal, DayOfWeek dayOfWeek) {
		System.assertEquals(dayOfWeek, DayOfWeekMapping.get(ordinal));
	}
}