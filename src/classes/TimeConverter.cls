public with sharing class TimeConverter {
	private Integer decimalPlaces;
	
	public TimeConverter() {
		decimalPlaces = 4;
	}
	
	public TimeConverter(final Integer decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}
	
	public Decimal minutesToHours(Integer minutes) {
		Decimal hours = null;
		if (minutes != null) {
			hours = minutes / 60.0.setScale(decimalPlaces);
		}
		return scale(hours);
	}
	
	public Decimal secondsToHours(Integer seconds) {
		Decimal hours = null;
		if (seconds != null) {
			hours = seconds / 3600.0.setScale(decimalPlaces);
		}
		return scale(hours);
	}
	
	public Decimal millisecondsToHours(Integer milliseconds) {
		Decimal hours = null;
		if (milliseconds != null) {
			hours = milliseconds / 3600000.0.setScale(decimalPlaces);
		}
		return scale(hours);
	}
	
	private Decimal scale(Decimal theDecimal) {
		Decimal scaledDec = null;
		if (theDecimal != null) {
			scaledDec = theDecimal.setScale(decimalPlaces);
		}
		return scaledDec;
	}
	
	public Integer getDecimalPlaces() {
		return decimalPlaces;
	}
}