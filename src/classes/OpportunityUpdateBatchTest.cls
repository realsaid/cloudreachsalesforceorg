@isTest
private class OpportunityUpdateBatchTest {
	


	@isTest static void test_method_one() {
		//insert products

		Product2 prod1 = new Product2(Name = 'Consulting', Family = 'Salesforce');
		insert prod1;

		Product2 prod2 = new Product2(Name = 'Training', Family = 'Salesforce');
		insert prod2;

		Product2 prod3 = new Product2(Name = 'Consulting', Family = 'AWS');
		insert prod3;

		//get standard pricebook id
		Id pricebookId = Test.getStandardPricebookId();

		//insert standard prices

		PricebookEntry pbe1 = new PricebookEntry(
			Pricebook2Id = pricebookId
			, Product2Id = prod1.Id
			, UnitPrice = 1000
			, IsActive = true);
		insert pbe1;

		PricebookEntry pbe2 = new PricebookEntry(
			Pricebook2Id = pricebookId
			, Product2Id = prod2.Id
			, UnitPrice = 1000
			, IsActive = true);
		insert pbe2;

		PricebookEntry pbe3 = new PricebookEntry(
			Pricebook2Id = pricebookId
			, Product2Id = prod3.Id
			, UnitPrice = 100
			, IsActive = true);
		insert pbe3;		

		//get territory
		list<Territory> terrList = [select id from territory where name = 'UK & Ireland' limit 1];
		id terId = terrList[0].id;

		//create Account

		Account acc = new account(
			name='testAcc'
			, sales_territory__c = 'UK & Ireland'
			);
		Insert acc;

		//create opportunity
		Opportunity opp = new Opportunity(
			accountid = acc.id
			, name='TestOpp'
			, StageName = 'Prospect'
            , Type = 'Customer'
			, CloseDate = system.today() + 14
			, territoryId = terId);		
		insert Opp;
		id oppid = opp.id;

		//assign products to Opp

		OpportunityLineItem oli1 = new OpportunityLineItem(
			Opportunityid = opp.id
			, priceBookEntryId = pbe1.id
			);
		insert oli1;

		OpportunityLineItem oli2 = new OpportunityLineItem(
			Opportunityid = opp.id
			, priceBookEntryId = pbe2.id

			);
		insert oli2;

		OpportunityLineItem oli3 = new OpportunityLineItem(
			Opportunityid = opp.id
			, priceBookEntryId = pbe3.id

			);
		insert oli3;
		test.startTest();
		database.executeBatch(new OpportunityUpdateBatch(), 200);
		test.stopTest();

		opp = [select id, derived_bu__c from opportunity where id =: oppid];
		system.debug('Firefly: '+ opp.derived_bu__c);
		system.assertEquals(opp.derived_bu__c, 'Salesforce');				

	}

	
}