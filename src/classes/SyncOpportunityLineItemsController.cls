public with sharing class SyncOpportunityLineItemsController {
	private Opportunity opp;
	
	private ApexPages.StandardController stdController;
	
    public String redirectUrl {public get; private set;}
    
    public Boolean shouldRedirect {public get; private set;}
    
    public Boolean hasSynced {public get; private set;}
    
    public SyncOpportunityLineItemsController(ApexPages.StandardController stdController) {
    	this.stdController = stdController;
    	
        opp = (Opportunity) stdController.getRecord();
        opp = [SELECT Id, Primary_Service_Contract__c FROM Opportunity WHERE Id = :opp.Id];
        
        shouldRedirect = false;
        hasSynced = false;
    }
    
    public void syncOpportunityLineItemsToPrimaryServiceContract() {
    	shouldRedirect = true;
    	if (opp.Primary_Service_Contract__c != null) {
    		new LineItemSyncer().syncLineItemsFromOppToServiceContract(opp.ID, opp.Primary_Service_Contract__c);
    		hasSynced = true;
	        redirectUrl = '/' + opp.Primary_Service_Contract__c;
    	}
    }
}