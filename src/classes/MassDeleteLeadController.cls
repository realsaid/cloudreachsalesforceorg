public class MassDeleteLeadController extends MassDeleteSObjectController {
	public MassDeleteLeadController(ApexPages.StandardSetController controller) {
        super(controller);
    }
    
	protected override PageReference showListView() {
		return new PageReference('/00Q');
	}
}