@isTest
private class ContractTestFactory{
  private static TestMethod void ContractsonAccount()
    {    
    test.startTest(); 
    Account myAccount = TestDataSet.getAccount();        
    insert myAccount;  
    
    //List<RecordType> RecTypes = [Select Id From RecordType Where SobjectType = 'Contract' and Name in ('Framework Agreement','Online Ts & Cs')];
    
    
    List<Contract> Contract= new List<Contract>();
    //Contract myContract = new Contract(recordtypeid=RecTypes[0].Id, accountId=myAccount.Id,StartDate=date.today());
    Contract myContract = new Contract(accountId=myAccount.Id,StartDate=date.today(),Confidentiality_liability_cap__c='No Cap',CS_FA_version__c='2.0',preapproval_required_for_travel_expenses__c='Yes',Right_to_adjust_timetable__c='Yes');
    
    insert(myContract);
    myContract.Status = 'Signed';
    update(myContract);
    
    Contract validateContract = [select Status from Contract where Id = :myContract.Id];
    System.assertEquals('Signed',validateContract.Status);
              
    test.stopTest();  
    }
    // Valdiate that it's not possible to create more than 1 FA contract
    private static TestMethod void PreventDuplicateFA()
    {    
    test.startTest(); 
    Account myAccount = TestDataSet.getAccount();        
    insert myAccount;  
    
    List<RecordType> RecTypes = [Select Id From RecordType Where SobjectType = 'Contract' and Name = 'Framework Agreement'];
        
    List<Contract> Contract= new List<Contract>();
    
    Contract myFirstContract = new Contract(recordtypeid=RecTypes[0].Id,
            accountId=myAccount.Id,StartDate=date.today(),Confidentiality_liability_cap__c='No Cap',CS_FA_version__c='2.0',preapproval_required_for_travel_expenses__c='Yes',Right_to_adjust_timetable__c='Yes');
    
    
    insert(myFirstContract);
    myFirstContract.Status = 'Signed';
    update(myFirstContract);
    
    Contract mySecondContract = new Contract(recordtypeid=RecTypes[0].Id,
            accountId=myAccount.Id,StartDate=date.today(),Liability_cap_includes_Confidentiality__c=true, Amount_limitation_of_liability_cap__c='No Cap');
    insert(mySecondContract);
    
    mySecondContract.Status = 'Signed';
    
    Contract validateContract = [select Status from Contract where Id = :mySecondContract.Id];
    System.assertNotEquals('Signed',validateContract.Status);
    
    Exception e;
        try{
            update(mySecondContract);
        } catch(Exception ex){
            e = ex;
        }

        system.assertNotEquals(null, e);
        system.assert(e instanceOf system.Dmlexception);
        system.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        system.assert(e.getMessage().contains('Can\'t have more then one signed Framework Agreement Contract for the account. Please ensure only one exists before progressing.'));
              
    test.stopTest();  
    }
}