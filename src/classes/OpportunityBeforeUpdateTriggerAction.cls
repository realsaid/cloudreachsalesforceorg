public with sharing class OpportunityBeforeUpdateTriggerAction extends TriggerAction {
  private final Opportunity[] newOpportunities;
  private final Opportunity[] oldOpportunities;
  private final String PROJECT_DEFINED_STAGENAME = 'Project Defined';
    
  public OpportunityBeforeUpdateTriggerAction(final Opportunity[] oldOpportunities, final Opportunity[] newOpportunities) {
    this.oldOpportunities = oldOpportunities;
    this.newOpportunities = newOpportunities;
  }
  
  public override void onBeforeUpdate() {
    if (shouldPerformAction()) {
      performAction(newOpportunities[0]);
    }
  }
  
  private Boolean shouldPerformAction() {
    Boolean shouldPerformAction = false;
    if (newOpportunities.size() == 1 &&
      oldOpportunities.size() == 1 &&
      isNotRenewalOpportunity(newOpportunities[0])) {
      shouldPerformAction = true;
    }
    return shouldPerformAction;
  }
  
  private Boolean isNotRenewalOpportunity(Opportunity opp) {
    return opp.Type != SMOpportunity.TYPE_RENEWAL;
  }
  
  private void performAction(Opportunity opportunity) {
    // create local vars
    Boolean Signed_Order_Form_Exists = false;  
    Opportunity oldOpp = oldOpportunities[0]; //Note: this only works as we perform action when there is only one opp..
    List<RecordType> RecTypes = [Select Id From RecordType  Where SobjectType = 'Contract' and Name in ('Framework Agreement','Online Ts & Cs')];
    RecordType FA_type = [Select Id From RecordType  Where SobjectType = 'Contract' and Name = 'Framework Agreement'];
    
    List<Contract> Contracts;
    Contracts= [SELECT Amount_limitation_of_liability_cap__c,are_travel_expenses_billable__c,Can_the_Customer_terminate_an_Order_Form__c,
    clause_12_5_applies__c,Confidentiality_liability_cap__c,CS_FA_version__c,Customer_obliged_to_cooperate__c,DP_language_cap__c,
    FA_Order_Form_one_separate_agreement__c,Id,Implied_terms_excluded__c,Interest_rate_at_least_2_over__c,IPR_Indemity_clause_cap__c,
    Is_CR_liability_excluded__c,Is_it_a_Cloudreach_framework_agreement__c,Is_there_a_non_solicitation_clause__c,
    Is_there_a_specific_IPR_indemnity_clause__c,Is_there_data_protection_language__c,Liability_capped_context__c,
    Liability_cap_includes_Confidentiality__c,Liability_is_excluded_for__c,Order_Form_prevails__c,overtime_provisions__c,
    preapproval_required_for_travel_expenses__c,Prior_notice_required_for_changes__c,Right_to_adjust_timetable__c,
    standard_of_endeavours_required__c,standard_of_skill_and_care_required__c,TUPE_Clause_added__c,URL__c,What_is_the_payment_term__c,
    Who_owns_the_IPR_in_the_deliverables__c,X3rd_party__c, RecordTypeId FROM Contract
    Where AccountId = :opportunity.AccountId AND RecordTypeId in :RecTypes 
    AND Status = 'Signed' and isDeleted = false AND (EndDate = null OR EndDate > :date.today())]; 
     
    system.debug('Number of FA contracts is: ' + Contracts.size());
    //-----------------extract singed OF agreements------------  
    List <echosign_dev1__SIGN_Agreement__c> Agreements; 
    Agreements    = [SELECT Id, echosign_dev1__Status__c, Name 
                                                    from echosign_dev1__SIGN_Agreement__c 
                                                    where echosign_dev1__Opportunity__c = :opportunity.Id
                                                    AND echosign_dev1__Status__c = 'Signed'];
    //------raise signed OF agreement boolean & checkbox
    if  (Agreements.size() > 0){Signed_Order_Form_Exists = true;}
    opportunity.Order_Form_Echosigned__c =   Signed_Order_Form_Exists;
    
    /////////////////// start validations/////////////////////////////////////////
    //////////////////////////////////////////////////////////////
    // Do not allow close-winning an Opp, with an 'FA' contract type but no FA
    /////////////////////////////////////////////////////////////////
    if (( opportunity.IsClosed ) && ( opportunity.IsWon ) && (Contracts.size() ==0) && (opportunity.Contract_Type__c == 'FA')
         && (!(oldOpp.IsClosed) && !(oldOpp.IsWon)))
        { 
            opportunity.addError('Can\'t Close-win Opportunity, as found no signed Framework Agreement Contract for the account. Please ensure one exists before progressing.');
        }
    ////////////////////////////////////////////////////////////////////////////////////////////
    // If probability is larger then 20%, populate FA contract lookup, which triggers an email containing HTCs
    //////////////////////////////////////////////////////////////
      if (( opportunity.Probability > 0.2 ) && (Contracts.size() >0)){
        Contract FA;
        if (Contracts.size() ==1)
            FA = Contracts[0];
        else if (Contracts.size() > 1) //Populate using latest contract - this clause shuld never be reached...
            FA = Contracts[Contracts.size()-1];
            
        system.debug('Setting FA_Contract__c to ' + FA.id);
        opportunity.FA_Contract__c = FA.id;
        //This triggers a 'Send HTC Email' workflow.             
     }// else - If the lookup is NOT popualted, a workflow is triggered to notify the AM to create an FA 
    //////////////////////////////////////////////////////////////
    // Do not allow close-winning an Opp without a signed order form
    /////////////////////////////////////////////////////////////////
     if (( opportunity.IsClosed ) && ( opportunity.IsWon )&& (!(oldOpp.IsClosed) && !(oldOpp.IsWon)) )
     { 
        if (!Signed_Order_Form_Exists ) {
            opportunity.addError('Can\'t close win Opportunity , as found no signed Order Form!. Please ensure one exists before progressing.');
        } else system.debug('Signed_Order_Form_Exists = ' +Signed_Order_Form_Exists); 
     }else system.debug('Agreements size is ' + Agreements.size());
  } 
}