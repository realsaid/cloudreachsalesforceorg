public with sharing class SMBusinessHours {
	private BusinessHours record;
	private Map<Integer, TimePeriod> hoursMap;
	
	public SMBusinessHours(BusinessHours record) {
		this.record = record;
		populateHoursMap();	
	}
	
	public Id getId() {
		return record.Id;
	}
	
	private void populateHoursMap() {
		hoursMap = new Map<Integer, TimePeriod>();
		hoursMap.put(DayOfWeek.SUNDAY.ordinal(), new TimePeriod(new SMTime(record.SundayStartTime), new SMTime(record.SundayEndTime)));
		hoursMap.put(DayOfWeek.MONDAY.ordinal(), new TimePeriod(new SMTime(record.MondayStartTime), new SMTime(record.MondayEndTime)));
		hoursMap.put(DayOfWeek.TUESDAY.ordinal(), new TimePeriod(new SMTime(record.TuesdayStartTime), new SMTime(record.TuesdayEndTime)));
		hoursMap.put(DayOfWeek.WEDNESDAY.ordinal(), new TimePeriod(new SMTime(record.WednesdayStartTime), new SMTime(record.WednesdayEndTime)));
		hoursMap.put(DayOfWeek.THURSDAY.ordinal(), new TimePeriod(new SMTime(record.ThursdayStartTime), new SMTime(record.ThursdayEndTime)));
		hoursMap.put(DayOfWeek.FRIDAY.ordinal(), new TimePeriod(new SMTime(record.FridayStartTime), new SMTime(record.FridayEndTime)));
		hoursMap.put(DayOfWeek.SATURDAY.ordinal(), new TimePeriod(new SMTime(record.SaturdayStartTime), new SMTime(record.SaturdayEndTime)));
	}
	
	public TimePeriod getHoursForToday() {
		return getHoursForDay(new SMDate(Date.today()).getDayOfWeek());
	}
	
	public TimePeriod getHoursForDay(DayOfWeek dayOfWeek) {
		return hoursMap.get(dayOfWeek.ordinal());
	}
	
	private TimePeriod getHoursForDay(Integer dayOfWeekNumber) {
		return hoursMap.get(dayOfWeekNumber);
	}
	
	public Decimal getTotalBusinessHoursForPeriod(DateTimePeriod period) {
		Decimal hours = 0.000;
		if (period != null) {
			DayOfWeek startDay = period.getStart().getDayOfWeek();
			DayOfWeek endDay = period.getEnd().getDayOfWeek();
			Integer nDays = period.getDays();
			
			if (nDays > 0) {
				hours += getHoursForDay(startDay).getHoursAfter(period.getStart().getTime());
				for (Integer dayNumber = startDay.ordinal() + 1; dayNumber < (nDays + startDay.ordinal()); ++dayNumber) {
					hours += hoursMap.get(Math.mod(dayNumber, 7)).getHours();
				}
				hours += getHoursForDay(endDay).getHoursBefore(period.getEnd().getTime());
			}
			else {
				TimePeriod timePeriod = new TimePeriod(period.getStart().getTime(),
													   period.getEnd().getTime());
				hours += getHoursForDay(startDay).getOverlappingHours(timePeriod);
			}
		}
		return hours.setScale(4);
	}
	
	private Boolean is24HourEntitlement(TimePeriod businessHours) {
		return businessHours.getHours() == 0.0000 ? true : false;
	}
	
	private Decimal scale(Decimal dec) {
		return dec.setScale(4);
	}
	
	public BusinessHours getRecord() {
		return record;
	}
}