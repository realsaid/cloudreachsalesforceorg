public with sharing class OpportunityCloner {
    Map<String, Schema.SObjectField> fieldMap;
    
    public OpportunityCloner() {
    	fieldMap = Schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap();
    }
    
    public Opportunity insertClone(ID oppID) {
    	Opportunity opp = Database.query(new SelectAll('Opportunity').getSOQL() + ' WHERE ID = ' + new SMString(oppID).inQuotes());
    	Opportunity clone = new Opportunity();
        for (Opportunity_Clone_Fieldset__c field : fieldsToClone()) {
        	clone.put(field.Field_Name__c, opp.get(field.Field_Name__c));
        }
        clone.StageName = 'Suspect';
        insert clone;
        
        OpportunityLineItem[] cloneItems = new SMOpportunity(opp).getLineItems().deepClone(false);
        for (OpportunityLineItem item : cloneItems) {
            item.OpportunityID = clone.ID;
            item.TotalPrice = null;
        }
        insert cloneItems;
        return clone;
    }
        
    private Opportunity_Clone_Fieldset__c[] fieldsToClone() {
    	return [SELECT Field_Name__c FROM Opportunity_Clone_Fieldset__c];
    }
}