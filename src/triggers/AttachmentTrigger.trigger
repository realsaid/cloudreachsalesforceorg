trigger AttachmentTrigger on Attachment (After insert) {
    for (Attachment att:Trigger.new)
    {
        String parentObjId = att.ParentId;
        //006 is the starting sting in ID for all opportunities
        if(parentObjId.startsWith('006') && att.Name.toLowerCase().contains('order') && att.Name.toLowerCase().contains('form'))
        {
            Opportunity opp = [SELECT Id,Order_Form__c FROM Opportunity WHERE Id = :parentObjId];
            String URL =   System.URL.getSalesforceBaseURL().getHost();
            opp.Proposed_Order_Form__c = URL+'/servlet/servlet.FileDownload?file='+att.Id;
            update opp;
        }
    }
}