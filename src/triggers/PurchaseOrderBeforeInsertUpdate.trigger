/*

	Created by: 	Charlotte Gibson
	Date: 			16/08/2016

	ChangeLog:		15/09/2016 CG - removed before update from trigger, as requested by Pardha 14/9/2016	

	Purpose:
		1) 	On create/update of SCMC__Purchase_Order__c, match buyer.ff-company to 
			SCMC__Purchase_Order__c.company__c, 

*/

trigger PurchaseOrderBeforeInsertUpdate on SCMC__Purchase_Order__c (before insert) {

	//get companies
	map<string,id> companyMap = new map<string,id>{};
	for(c2g__codaCompany__c c: [select name, id from c2g__codaCompany__c]){
		companyMap.put(c.name, c.id);
	}

	set<id> userIds = new set<id>{};
	for(SCMC__Purchase_Order__c po : trigger.new){
		if(!userIds.contains(po.SCMC__Buyer_User__c)) userIds.add(po.SCMC__Buyer_User__c);
	}

	map<id,User> userMap = new map<id,User>{};
	for(user u: [select id
						, ff_company__c
						from User
						where id in: userIds]){
		userMap.put(u.id, u);
	}
	system.debug('running trigger' );

	for(Integer i = 0 ;i < Trigger.new.size(); i++){

		id buyerId =  trigger.new[i].SCMC__Buyer_User__c;
		system.debug('usermap: '+ userMap);
		
		//get company name from user record via buyer
		string company ;
		if(userMap.get(buyerId).ff_company__c != null){
			company = userMap.get(buyerId).ff_company__c;
		}
		system.debug('Company: ' + company);

		//match company name to company Id via companyMap
		string companyId;	
		if(company != null){
			companyId = companyMap.get(company);

		}
		system.debug('companyId: ' + companyId);
		//write company id to po record

		if(companyId != null){
			trigger.new[i].SCMFFA__Company__c = companyId;
		}
		system.debug('trigger.new[i].SCMFFA__Company__c: ' + trigger.new[i].SCMFFA__Company__c);
	}

}