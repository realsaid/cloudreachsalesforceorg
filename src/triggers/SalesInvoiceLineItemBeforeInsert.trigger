/*

Created by: Charlotte Gibson
Date 16/08/2016

Purpose:
		1) 	On create of c2g__codaInvoiceLineItem__c, populate dimensions 1-4, according to the following mapping:
			Dim1 = invoice.opp.owner.ff-company
			Dim2 = ili.product.dim2
			Dim3 = invoice.opp.owner.addressCity
			Dim4 = ili.product.dim4

			if no opportuniy on sales invoice, Dim 1 & 3 from sales invoice creator.
*/

trigger SalesInvoiceLineItemBeforeInsert on c2g__codaInvoiceLineItem__c (before insert) {

	//get related records from triggered set

	//get set of ids
	set<id> productIds = new set<id>{};
	set<id> invoiceIds = new set<id>{};
	for(c2g__codaInvoiceLineItem__c sli : trigger.new){
		if(!productIds.contains(sli.c2g__Product__c)) productIds.add(sli.c2g__Product__c);
		if(!invoiceIds.contains(sli.c2g__Invoice__c)) invoiceIds.add(sli.c2g__Invoice__c);
	}

	map<id, product2> productMap= new map<id, product2>{};
	for(product2 p : [select id
								, Dimension2__c
								, Dimension4__c
								from product2
								where id in: productIds]){
		productMap.put(p.id, p);
	}
	
	map<id, c2g__codaInvoice__c> invoiceMap= new map<id, c2g__codaInvoice__c>{};
	for(c2g__codaInvoice__c i : [select id
								, c2g__Opportunity__r.owner.ff_company__c
								, c2g__Opportunity__r.owner.city
								, CreatedBy.ff_company__c
								, CreatedBy.city
								from c2g__codaInvoice__c
								where id in: invoiceIds]){
		invoiceMap.put(i.id, i);
	}

	//get company ids
	map<string,id> dim1Map = new map<string,id>{};
	for(c2g__codaDimension1__c c: [select name, id from c2g__codaDimension1__c]){
		dim1Map.put(c.name, c.id);
	}

	//get City ids
	map<string,id> dim3Map = new map<string,id>{};
	for(c2g__codaDimension3__c c: [select name, id from c2g__codaDimension3__c]){
		dim3Map.put(c.name, c.id);
	}

	for(Integer i = 0 ;i < Trigger.new.size(); i++){

		c2g__codaInvoiceLineItem__c sli = trigger.new[i];

		//get invoice from invoice map
		c2g__codaInvoice__c inv = invoiceMap.get(sli.c2g__Invoice__c);


		//Set Dimension 1 - invoice opp.owner or invoice creator ff-company
		//check invoice.opp owner
		string company;

		if(inv.c2g__Opportunity__r.owner.ff_company__c != null){
			company = inv.c2g__Opportunity__r.owner.ff_company__c;
		} else {
			company = inv.createdBy.ff_company__c;
		}

		//get company id from map

		sli.c2g__Dimension1__c = dim1Map.get(company);
		
		//Set Dimension 2 = product dimension 2
		sli.c2g__Dimension2__c = productMap.get(sli.c2g__Product__c).Dimension2__c;


		//Set Dimension 3 - invoice opp.owner or invoice creator city
		//get city id from map
		
		string city;

		if(inv.c2g__Opportunity__r.owner.city != null){
			city = inv.c2g__Opportunity__r.owner.city;
		} else {
			city = inv.createdBy.city;
		}

		sli.c2g__Dimension3__c = dim3Map.get(city);


		//Set Dimension 4 product dimension 4
		sli.c2g__Dimension4__c = productMap.get(sli.c2g__Product__c).Dimension4__c;
	}

}