trigger ContractTrigger on Contract (Before Insert, Before Update) {
     if (Trigger.isInsert) {
         //initialise dependandt picklist values if they do not exist - according to Pontus' definitions
         for(Contract c : trigger.new) {
             if ((c.are_travel_expenses_billable__c) && (c.preapproval_required_for_travel_expenses__c == Null)) c.preapproval_required_for_travel_expenses__c='No';
             if ((c.Customer_obliged_to_cooperate__c) && (c.Right_to_adjust_timetable__c==null)) c.Right_to_adjust_timetable__c='Yes';
             if ((c.Is_it_a_Cloudreach_framework_agreement__c) && (c.CS_FA_version__c==null)) c.CS_FA_version__c='2.0'; 
         }
    }
    
    if (Trigger.isUpdate){
        // Get id of Framework Agreement record types
        List<RecordType> RecTypes = [Select Id From RecordType Where SobjectType = 'Contract' and Name in ('Framework Agreement','Online Ts & Cs')];
        
        Set<String> RecTypeSet = new set<String>(); 
        For(RecordType rc :RecTypes ){ 
            RecTypeSet.add(rc.Id); 
        } 
        // Get a list of currently active Framework Agreement contracts (should be zero or one)
        // NOTE: the assumption is that this trigger is only called online hence using AccountId = :Trigger.new[0].AccountId 
        // This assumption would be worng if contracts are loaded en-masse for multiple accounts!
        
        List<Contract> Contracts;
        date d_today = system.today();
        // get all currently effective framework agreement contracts (including online Ts & Cs)
        Contracts= [SELECT Amount_limitation_of_liability_cap__c,are_travel_expenses_billable__c,Can_the_Customer_terminate_an_Order_Form__c,
        clause_12_5_applies__c,Confidentiality_liability_cap__c,CS_FA_version__c,Customer_obliged_to_cooperate__c,DP_language_cap__c,
        FA_Order_Form_one_separate_agreement__c,Id,Implied_terms_excluded__c,Interest_rate_at_least_2_over__c,IPR_Indemity_clause_cap__c,
        Is_CR_liability_excluded__c,Is_it_a_Cloudreach_framework_agreement__c,Is_there_a_non_solicitation_clause__c,
        Is_there_a_specific_IPR_indemnity_clause__c,Is_there_data_protection_language__c,Liability_capped_context__c,
        Liability_cap_includes_Confidentiality__c,Liability_is_excluded_for__c,Order_Form_prevails__c,overtime_provisions__c,
        preapproval_required_for_travel_expenses__c,Prior_notice_required_for_changes__c,Right_to_adjust_timetable__c,
        standard_of_endeavours_required__c,standard_of_skill_and_care_required__c,TUPE_Clause_added__c,URL__c,What_is_the_payment_term__c,
        Who_owns_the_IPR_in_the_deliverables__c,X3rd_party__c FROM Contract
        Where RecordTypeId in :RecTypes AND Status = 'Signed' and isDeleted = false 
        and AccountId = :Trigger.new[0].AccountId and (EndDate > :d_today or EndDate = null)
        and Id not in :Trigger.new]; 
         
        system.debug('Number of current FA contracts is: ' + Contracts.size());
        
        List <Contract> ActiveContracts = new List<Contract>();
        
        // get all contracts ABOUT to be activated
        for (Contract c: Trigger.new) {
            Contract old_c = System.Trigger.oldMap.get(c.Id);
            if ((RecTypeSet.contains(c.RecordTypeId)) && (c.Status == 'Signed') && (old_c.Status <> 'Signed') && (c.isDeleted == false) && ((c.EndDate > d_today) || (c.EndDate == null)) )
                ActiveContracts.add(c);
           
            if (( Contracts.size()+ ActiveContracts.size() > 1))
                c.addError('Can\'t have more then one signed Framework Agreement Contract for the account. Please ensure only one exists before progressing.');
      }
   }
}