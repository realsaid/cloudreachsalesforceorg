trigger PayableInvoiceLineItemBeforeInsert on c2g__codaPurchaseInvoiceLineItem__c (before insert) {

	//get related records from triggered set

	//get set of ids
	set<id> productIds = new set<id>{};
	set<id> piIds = new set<id>{};


	for(c2g__codaPurchaseInvoiceLineItem__c pil : trigger.new){
		if(!productIds.contains(pil.c2g__Product__c)) productIds.add(pil.c2g__Product__c);
		if(!piIds.contains(pil.c2g__PurchaseInvoice__c)) piIds.add(pil.c2g__PurchaseInvoice__c);


	}
	system.debug('piIds: ' + piIds);
	map<id, product2> productMap= new map<id, product2>{};
	for(product2 p : [select id
								, Dimension2__c
								, Dimension4__c
								from product2
								where id in: productIds]){
		productMap.put(p.id, p);
	}
	
	map<id, c2g__codaPurchaseInvoice__c> piMap= new map<id, c2g__codaPurchaseInvoice__c>{};
	for(c2g__codaPurchaseInvoice__c i : [select id
				, purchase_order__r.SCMC__Buyer_User__r.ff_company__c
				, purchase_order__r.SCMC__Buyer_User__r.city
				from c2g__codaPurchaseInvoice__c
				where id =: piIds]){
		piMap.put(i.id, i);
	}
	system.debug('piMap: ' + piMap);
	//get company ids
	map<string,id> dim1Map = new map<string,id>{};
	for(c2g__codaDimension1__c c: [select name, id from c2g__codaDimension1__c]){
		dim1Map.put(c.name, c.id);
	}

	//get City ids
	map<string,id> dim3Map = new map<string,id>{};
	for(c2g__codaDimension3__c c: [select name, id from c2g__codaDimension3__c]){
		dim3Map.put(c.name, c.id);
	}

	for(Integer i = 0 ;i < Trigger.new.size(); i++){
		c2g__codaPurchaseInvoiceLineItem__c pil = trigger.new[i];

		//get invoice from invoice map
		c2g__codaPurchaseInvoice__c pi = piMap.get(pil.c2g__PurchaseInvoice__c);

		//Set Dimension 1 - invoice opp.owner or invoice creator ff-company
		//check invoice.opp owner
		string company;
		company = pi.purchase_order__r.SCMC__Buyer_User__r.ff_company__c;
		system.debug(piMap);


		//get company id from map

		if(dim1Map != null)pil.c2g__Dimension1__c = dim1Map.get(company);
		
		//Set Dimension 2 = product dimension 2
		if(productMap != null && pil.c2g__Product__c != null) pil.c2g__Dimension2__c = productMap.get(pil.c2g__Product__c).Dimension2__c;
		system.debug(dim1Map.get(company));

		//Set Dimension 3 - invoice opp.owner or invoice creator city
		//get city id from map
		
		string city;
		city = pi.purchase_order__r.SCMC__Buyer_User__r.city;
		
		if(dim3Map != null) pil.c2g__Dimension3__c = dim3Map.get(city);


		//Set Dimension 4 product dimension 4
		if(productMap != null && pil.c2g__Product__c != null) pil.c2g__Dimension4__c = productMap.get(pil.c2g__Product__c).Dimension4__c;
	}


}