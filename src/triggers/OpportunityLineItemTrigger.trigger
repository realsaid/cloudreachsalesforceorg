trigger OpportunityLineItemTrigger on OpportunityLineItem (Before Insert, Before Update) {
    	for (OpportunityLineItem lineItem : Trigger.new) {
    		if (lineItem.Gross_price__c != null)
    			lineItem.Booking_Amount__c = lineItem.Gross_price__c;
    		else
    			lineItem.Booking_Amount__c = lineItem.ListPrice;
    		
    		if (lineItem.Third_Party_Cost__c != null && lineItem.Third_Party_Cost__c>0) {
	    		lineItem.UnitPrice = lineItem.Booking_Amount__c  - lineItem.Third_Party_Cost__c;
	    	} else lineItem.UnitPrice = lineItem.Booking_Amount__c;
    	} 
}