trigger AccountDaysOffsetUpdate on Opportunity (after Insert,after Update) {
    List<ID> ids = new List<ID>();
    List<Account> accUpdate = new List<Account>();
    
    for(Opportunity opp:trigger.new)    {
        ids.add(opp.accountId);
    }
    for(Account acc:[Select Id,c2g__CODADaysOffset1__c from Account Where Id IN:ids])   {
        for(Opportunity opps:trigger.new)   {
            if(opps.Payment_Terms__c != Null)   {
                acc.c2g__CODADaysOffset1__c = opps.Payment_Terms__c;
                accUpdate.add(acc);
            }
        }
    }
    Update accUpdate;
}