trigger CaseTrigger on Case (before update) {
	try {
		Disabled_Org_Triggers__c settings = Disabled_Org_Triggers__c.getOrgDefaults();
		if (!settings.Case_Trigger__c) {
			TriggerAction[] actions = new TriggerAction[]{};
			
			if (Trigger.isUpdate && Trigger.isBefore) {
				actions.add(new ClockUpdateCaseTriggerAction(trigger.new));
				TriggerHandler.onBeforeUpdate(actions);
			}
		}
	}
	catch (Exception e) {
		Logger.logException('CaseTrigger', 'Exception caught in CaseTrigger', e);
	}
}