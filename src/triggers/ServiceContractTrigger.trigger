trigger ServiceContractTrigger on ServiceContract (before update) {
	private static Boolean hasExecuted = false;
	
	try {
		TriggerAction[] actions = new TriggerAction[]{};
		
		if (Trigger.isUpdate && Trigger.isBefore && !hasExecuted) {
			actions.add(new RenewalContractTriggerAction(trigger.old, trigger.new));
			TriggerHandler.onBeforeUpdate(actions);
		}
	}
	catch (Exception e) {
		System.Debug('Exception caught in ServiceContractTrigger: ' + e.getMessage());
	}
	finally {
		hasExecuted = true;
	}
}