/*

	Created by: 	Charlotte Gibson
	Date: 			23/8/2016
		dim 1 --> expense.resource.name.company match with dimension1 object
		dim 2 --> *not set*
		dim 3 --> expense.resource.name.city match with dimension3 object
		dim 4 --> *not set*

*/

trigger PayableInvoiceExpenseLineItemBeforeInsert on c2g__codaPurchaseInvoiceExpenseLineItem__c (before insert) {

	//get purchase invoice ids to extract resource user data
	set<id> piIds = new set<id>{};
	for(c2g__codaPurchaseInvoiceExpenseLineItem__c eli : trigger.new){

		if(!piIds.contains(eli.c2g__PurchaseInvoice__c)) piIds.add(eli.c2g__PurchaseInvoice__c);
	}

	//query purchase invoice resource data
	map<id, c2g__codaPurchaseInvoice__c> piMap= new map<id,c2g__codaPurchaseInvoice__c>{};
	for(c2g__codaPurchaseInvoice__c pi : [select id
											, ffpsai__ExpenseReportResource__r.pse__Salesforce_User__r.ff_company__c
											, ffpsai__ExpenseReportResource__r.pse__Salesforce_User__r.city
											from c2g__codaPurchaseInvoice__c
											where id in: piIds]){
		piMap.put(pi.id, pi);
	}

	//get company ids
	map<string,id> dim1Map = new map<string,id>{};
	for(c2g__codaDimension1__c c: [select name, id from c2g__codaDimension1__c]){
		dim1Map.put(c.name, c.id);
	}

	//get City ids
	map<string,id> dim3Map = new map<string,id>{};
	for(c2g__codaDimension3__c c: [select name, id from c2g__codaDimension3__c]){
		dim3Map.put(c.name, c.id);
	}


	//cycle through triggerset and match up data
	for(c2g__codaPurchaseInvoiceExpenseLineItem__c eli : trigger.new){

		//get purchase invoice map data
		c2g__codaPurchaseInvoice__c pInvoice= piMap.get(eli.c2g__PurchaseInvoice__c);

		string company = pInvoice.ffpsai__ExpenseReportResource__r.pse__Salesforce_User__r.ff_company__c;
		string city = pInvoice.ffpsai__ExpenseReportResource__r.pse__Salesforce_User__r.city;

		//set dim1 data
		eli.c2g__Dimension1__c = dim1Map.get(company);

		//set dim3 data
		eli.c2g__Dimension3__c = dim3Map.get(city);


	}
}