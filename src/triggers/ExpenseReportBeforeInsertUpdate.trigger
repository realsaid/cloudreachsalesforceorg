/*

Created by: Charlotte Gibson
Date 16/08/2016

Purpose:
		1) 	On create/update of pse__Expense_Report__c, match name of resource to name of user, 
			then match user.ff_company__c to company__c object, and write that object to expense report

*/


trigger ExpenseReportBeforeInsertUpdate on pse__Expense_Report__c (before insert, before update) {



	//get map of <c2g__codaCompany__c, id>
	map<string,id> companyMap = new map<string,id>{};
	for(c2g__codaCompany__c c: [select name, id from c2g__codaCompany__c]){
		companyMap.put(c.name, c.id);
	}

	//get set of contact ids from expense report
	set<Id> contactIds = new set<Id>{};
	for(pse__Expense_Report__c er : trigger.new){
		if(!contactIds.contains(er.pse__Resource__c)) contactIds.add(er.pse__Resource__c);
	}

	//get map of expense report with related fields
	map<id,contact> erMap = new map<id,contact>{};
	for(contact er: [select name
						, pse__Salesforce_User__r.ff_company__c
						from contact
						where id in: contactIds]){
		erMap.put(er.id, er);
	}



	for(Integer i = 0 ;i < Trigger.new.size(); i++){

		 Contact resource = erMap.get(trigger.new[i].pse__Resource__c);

		 //get contact from map


		// match resource SF user's ff company to the expense report company by company name.
		if(resource.pse__Salesforce_User__r.ff_company__c != null){
			trigger.new[i].ffpsai__OwnerCompany__c = companyMap.get(resource.pse__Salesforce_User__r.ff_company__c);
		}

	}

}