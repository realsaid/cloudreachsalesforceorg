trigger OpportunityTrigger on Opportunity (before update) {
    private static Boolean hasExecuted = false;
    
    TriggerAction[] actions = null;
    if (Trigger.isBefore && Trigger.isUpdate && !hasExecuted) {
        try {
            actions = new TriggerAction[]{};
            actions.add(new RenewalOpportunityCloseTriggerAction(trigger.old, trigger.new));
            TriggerHandler.onBeforeUpdate(actions);
            
            actions = new TriggerAction[]{};
            actions.add(new OpportunityCloseTriggerAction(trigger.old, trigger.new));
            TriggerHandler.onBeforeUpdate(actions); 
            
            actions = new TriggerAction[]{};
            actions.add(new OpportunityBeforeUpdateTriggerAction(trigger.old, trigger.new));
            TriggerHandler.onBeforeUpdate(actions); 
           
        }
        catch (Exception e) {
            System.Debug('Exception caught in OpportunityTrigger: ' + e.getMessage());
        }
        finally {
            hasExecuted = true;
        }
    }
}