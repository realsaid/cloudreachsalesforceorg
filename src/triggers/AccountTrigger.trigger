trigger AccountTrigger on Account( before insert ) {
    
    /* This section needs to be replaced - the territories should be evaluated by the
    e acocunt owner, not the running user */    
    UserTerritories myUserTerritories = new UserTerritories();
    Map<Id, Territory> idToTerritoryMap = myUserTerritories.getUserTerritories();
    
    //Loop over trigger accounts and set the user territory field      
    for(Account acc : trigger.new){
       Boolean first = true;
       // If user is assigned to one or more territory, update the user_s_territory__c field accordingly
       if (idToTerritoryMap.size() > 0){ 
           String userTerritories = '';
           for (Territory t:idToTerritoryMap.values()) {
               if (first) {
                   userTerritories = userTerritories + t.Name;
                   first = false;
               } else { userTerritories = userTerritories + ', ' + t.Name;}
           }    
           acc.user_s_territory__c = userTerritories;
       }
    }
}