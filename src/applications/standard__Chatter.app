<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-UserProfile</tab>
    <tab>standard-OtherUserProfile</tab>
    <tab>standard-CollaborationGroup</tab>
    <tab>standard-File</tab>
    <tab>SupportProfile__c</tab>
    <tab>SupportContact__c</tab>
    <tab>SCMC__Warehouse__c</tab>
    <tab>SCMC__ICP__c</tab>
    <tab>SCMC__Address__c</tab>
    <tab>ffc__Event__c</tab>
</CustomApplication>
