<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>SCM Spend Management</label>
    <tab>standard-Account</tab>
    <tab>SCMC__Supplier_Site__c</tab>
    <tab>standard-Product2</tab>
    <tab>SCMC__Product_Group__c</tab>
    <tab>c2g__codaDimension1__c</tab>
    <tab>SCMC__Purchase_Order__c</tab>
    <tab>SCMC__AP_Voucher__c</tab>
    <tab>SCMC__Receipt__c</tab>
    <tab>c2g__codaPurchaseInvoice__c</tab>
    <tab>SCMC__Warehouse__c</tab>
    <tab>SCMC__ICP__c</tab>
    <tab>SCMC__Address__c</tab>
    <tab>ffc__Event__c</tab>
</CustomApplication>
