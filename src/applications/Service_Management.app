<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Case</defaultLandingTab>
    <label>Service Management</label>
    <tab>standard-File</tab>
    <tab>standard-Case</tab>
    <tab>SupportContact__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Account</tab>
    <tab>standard-ServiceContract</tab>
    <tab>standard-Entitlement</tab>
    <tab>SCMC__Warehouse__c</tab>
    <tab>SCMC__ICP__c</tab>
    <tab>SCMC__Address__c</tab>
    <tab>ffc__Event__c</tab>
</CustomApplication>
